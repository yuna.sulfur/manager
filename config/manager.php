<?php return [
	'default' => [
		// The main manager class
		'manager' => 'Sulfur\Manager\Manager',
		// Form class
		'form' => null,
		// Entity class
		'entity' => null,
		// Load entities with. Dot notation: ['item.user.avatar', 'item.comments.image']
		'with' => [],
		// List is draggable by user.
		'sortable' => false,
		// Index type: 'table', 'tree' or 'images'
		'index' => 'table',
		// List is sortable by:
		'sort' => ['created' => 'DESC', 'title' => 'ASC'],
		// Lock items for editing for multiple users
		'lock' => false,
		// Supply a route-name to make items previewable
		'preview' => false,
		// Items can be published and depublished or timed: false, true, 'start', 'end', ['start', 'end']
		'publish' => false,
		// track changes in history
		'history' => true,
		// main route to use for linking to an item
		'route' => false,
		// list filters
		'filters' => [
			'status' => ['live', 'edit'],
		],
		// what fields to search
		'search' => ['title'],
		// item per page, false for no pagination
		'pagination' => 50,
		// columns in list. When a view is supplied, it will be taken from index/column
		'columns' => [
			'title',
			'created' => [
				'width' => 200,
				'view' => 'created'
			],
		],
		// properties that can be toggled. name of a view in index/toggle
		'toggle' => ['status'],
		// available batch actions
		'batch' => ['delete'],
		// available extra actions for an item in the list view
		'actions' => [],
		// maximum filesize for an upload
		'size' => 25000000,
		// resize to dimensions when adding image
		'dimensions' => [
			'max' => ['width' => 5000, 'height' => 5000],
			'downsize' => ['width' => 2000, 'height' => 2000]
		],
		// storage path for an upload
		'path' => '',
		// link-module: these modules can be linked to
		'modules' => [],
		// link-module: preset routes with optional 'params' key with additional params
		// menu-module: preset string, to be parsed in frontend view
		'presets' => [],
		// link-module: let user add nofollow attr to the links
		'nofollow' => false,
		// form-module: avaliable shortcodes in e-mails
		'shortcodes' => [],
		// form-module: avaliable presets in hooks
		'hooks' => []
	],

	'auth' => [
		'form' => 'Sulfur\Manager\Auth\Form'
	],

	'form' => [
		'form' => 'Sulfur\Manager\Form\Form',
		'entity' => 'Sulfur\Manager\Form\Entity',
		'filters' => [],
		'toggle' => [],
	],


	'file' => [
		'manager' => 'Sulfur\Manager\File\Manager',
		'form' => 'Sulfur\Manager\File\Form',
		'entity' => 'Sulfur\Manager\File\Entity',
		'history' => false,
		'filters' => [],
		'toggle' => [],
		'actions' => ['serve'],
		'size' => 25000000,
		'path' => '',
	],


	'home' => [
		'entity' => 'Sulfur\Manager\Home\Entity',
		'manager' => 'Sulfur\Manager\Single\Manager',
		'form' => 'Sulfur\Manager\Home\Form',
		'route' => 'home'
	],

	'image' => [
		'manager' => 'Sulfur\Manager\Image\Manager',
		'form' => 'Sulfur\Manager\Image\Form',
		'entity' => 'Sulfur\Manager\Image\Entity',
		'route' => 'image',
		'embed' => 'normal',
		'index' => 'images',
		'history' => false,
		'filters' => [],
		'toggle' => [],
		'size' => 25000000,
		'dimensions' => [
			'max' => ['width' => 5000, 'height' => 5000],
			'downsize' => ['width' => 2000, 'height' => 2000]
		],
		'path' => '',
	],


	'link' => [
		'manager' => 'Sulfur\Manager\Link\Manager',
		'form' => 'Sulfur\Manager\Link\Form',
		'modules' => [],
		'presets' => [],
		'nofollow' => false
	],


	'menu' => [
		'manager' => 'Sulfur\Manager\Menu\Manager',
		'form' => 'Sulfur\Manager\Menu\Form',
		'entity' => 'Sulfur\Manager\Menu\Entity',
		'sortable' => true,
		'batch' => [],
		'index' => 'tree',
		'tree' => true,
		'preview' => false,
		'history' => false,
		'pagination' => false,
		'columns' => [
			'menu' => ['view' => 'menu']
		],
		'toggle' => [],
		'presets' => []
	],


	'preferences' => [
		'manager' => 'Sulfur\Manager\Preferences\Manager',
		'entity' => 'Sulfur\Manager\User\Entity',
		'form' => 'Sulfur\Manager\Preferences\Form',
		'history' => false,
	],


	'settings' => [
		'entity' => 'Sulfur\Manager\Settings\Entity',
		'manager' => 'Sulfur\Manager\Single\Manager',
		'form' => 'Sulfur\Manager\Settings\Form',
		'history' => false,
	],


	'user' => [
		'manager' => 'Sulfur\Manager\User\Manager',
		'entity' => 'Sulfur\Manager\User\Entity',
		'form' => 'Sulfur\Manager\User\Form',
		'history' => false,
		'sort' => ['username' => 'ASC'],
		'filters' => [],
		'search' => ['email', 'username', 'screenname'],
		'columns' => ['username', 'email'],
		'toggle' => [],
	],

	'tag' => [
		'manager' => 'Sulfur\Manager\Tag\Manager',
		'entity' => 'Sulfur\Manager\Tag\Entity',
		'form' => 'Sulfur\Manager\Tag\Form',
		'history' => false,
		'filters' => [],
		'toggle' => [],
	],
];