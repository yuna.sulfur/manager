<?php return [
	'frontend' => 'home',
	'menu' => [
		'structure' => [
			'icon' => 'dashboard',
			'items' => [
				'home' => [
					'icon' => 'home',
				],
				'menu' => [
					'icon' => 'menu',
					'button' => [
						'icon' => 'add_circle',
						'action' => 'create'
					]
				]
			],
		],
		'content' => [
			'icon' => 'list',
			'items' => [
				'page' => [
					'icon' => 'file_copy',
					'button' => [
						'icon' => 'note_add',
						'action' => 'create'
					]
				],
				'form' => [
					'icon' => 'check_box',
					'button' => [
						'icon' => 'note_add',
						'action' => 'create'
					]
				],
			]
		],
		'files' => [
			'icon' => 'folder',
			'items' => [
				'file' => [
					'icon' => 'cloud',
				],
				'image' => [
					'icon' => 'image',
				],
			]
		],
		'settings' => [
			'icon' => 'settings',
			'items'=> [
				'settings' => [
					'icon' => 'settings'
				],
				'tag' => [
					'icon' => 'style',
					'button' => [
						'icon' => 'add_to_photos',
						'action' => 'create'
					]
				],
				'user' => [
					'icon' => 'person',
					'button' => [
						'icon' => 'person_add',
						'action' => 'create'
					]
				],
			]
		]
	],
];