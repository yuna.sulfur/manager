<?php
use Sulfur\Identity;
use Sulfur\Upload;

return [
	'section' => [
		'start' => 'Start',
		'structure' => 'Structure',
		'content' => 'Content',
		'files' => 'Files',
		'users' => 'Users',
		'settings' => 'Settings'
	],
	'title' => [
		'index' => ':plural',
		'list' => ':plural',
		'create' => ':new :single',
		'update' => 'Edit :single',
		'roles' => 'Edit roles',
		'drop' => 'Click here or drop files to upload'
	],
	'field' => [
		'slug' => 'Part of url',
		'created' => 'created',
		'timestamp' => 'updated',
		'title' => 'title',
		'description' => 'description',
		'alias' => 'url alias',
		'status' => 'status',
		'intro' => 'intro',
		'body' => 'text',
		'preview' => 'preview',
		'blocks' => 'blocks',
		'owner_id' => 'owner',
		'editor_id' => 'updated by',
		'meta_title' => 'meta title',
		'meta_keywords' => 'meta keywords',
		'meta_description' => 'meta description',
		'time_start' => 'from',
		'time_end' => 'until',
		'use_time_start' => 'show from',
		'use_time_end' => 'show until',
		'username' => 'username',
		'password' => 'password',
		'email' => 'e-mail address',
		'screenname' => 'screenname',
		'submit' => 'Submit',
		'roles' => 'roles',
		'lang' => 'language',
		'file' => 'upload',
		'url' => 'Link',
		'blank' => 'open link in a new window',
		'nofollow' => 'add rel="nofollow" attribute',
		'parent_id' => 'Place in',
		'link' => 'Link',
		'html' => 'Html',
		'login' => 'Login',
		'timed' => ' ',
		'form' => 'form',
		'caption' => 'caption',
		'credits' => 'Source / credits',
		'image' => 'image',
		'images' => 'images',
		'file' => 'file',
		'files' => 'files',
		'redirect' => 'Redirects',
		'seo' => 'SEO and readability',
		'tags' => 'tags',
		'owner' => 'owner',
		'by' => 'by',
		'embed' => 'embed',
		'timed' => 'publication time',
	],
	'error' => [
		'title' => ['required' => 'provide a title'],
		'alias' => ['required' => 'provide a url'],
		'slug' => ['required' => 'provide a url'],
		Identity::ERROR_CREDENTIALS => 'Unknown credentials',
		Identity::ERROR_THROTTLE => 'Too many sign-in attempts. Try again in a few minutes.',
		Identity::ERROR_UNKNOWN => 'Unknown user',
		Sulfur\Upload::ERROR_MAX_UPLOAD_SIZE => 'File :file upload is too large',
		Sulfur\Upload::ERROR_MAX_FILE_SIZE => 'File :file is too large',
		Sulfur\Upload::ERROR_PARTIAL_UPLOAD => 'File :file is incomplete',
		Sulfur\Upload::ERROR_MISSING_UPLOAD => 'File :file is not found',
		Sulfur\Upload::ERROR_MISSING_TEMP => 'Uploaded file :file is not found',
		Sulfur\Upload::ERROR_WRITING => 'File :file could not be written',
		Sulfur\Upload::ERROR_PHP_EXTENSION => 'File :file not uploaded because of a server-error',
		Sulfur\Upload::ERROR_DISABLED => 'Uploads aren\'t allowed',
		Sulfur\Upload::ERROR_KEY_MISSING => 'No uploads found',
		Sulfur\Upload::ERROR_FILTER_SIZE => 'File :file is larger than allowed',
		Sulfur\Upload::ERROR_FILTER => 'File :file is not allowed',
		Sulfur\Upload::ERROR_OTHER => 'Upload of :file has failed',
		'username' => [
			'required' => 'Provide a username',
			'valid' => 'Username can only contain letters and digits',
			'unique' => 'Username exists',
		],
		'email' => [
			'required' => 'Provide an e-mail address',
			'email' => 'Provide a valid e-mail address',
			'unique' => 'E-mail address is in use',
		],
		'password' => [
			'required' => 'Provide a password',
			'password' => 'Provide a password of at least six characters',
		],
	],

	'label' => [
		'frontend' => 'To frontend',
		'start' => 'start',
		'logout' => 'Log out',
		'login' => 'Log in',
		'ok' => 'ok',
		'cancel' => 'cancel',
		'back' => 'back',
		'confirm' => 'ok',
		'save' => 'save',
		'saveclose' => 'save and close',
		'submit' => 'save',
		'submit_back' => 'save and back',
		'update' => 'edit',
		'delete' => 'remove',
		'show' => 'show',
		'hide' => 'hide',
		'preview' => 'preview',
		'select' => 'select',
		'use' => 'use',
		'apply' => 'apply',
		'filter' => 'filter',
		'sort' => 'sort',
		'open' => 'open',
		'close' => 'close',
		'previous' => 'previous',
		'next' => 'next',
		'view' => 'view',
		'create' => 'create',
		'add' => 'add',
		'add_image' => 'add image',
		'add_file' => 'add file',
		'add_video' => 'add video',
		'add_image' => 'add image',
		'link_create' => 'add link',
		'link_update' => 'edit link',
		'link_delete' => 'remove link',
		'delete_link' => 'remove link',
		'choose' => 'Choose',
		'browse' => 'browse',
		'content' => 'content',
		'media' => 'media',
		'seo' => 'search engines',
		'settings' => 'settings',
		'collection' => 'collections',
		'text' => 'text',
		'by' => 'by',
		'revision' => 'versions',
		'group' => [
			'create' => 'create group',
			'all' => 'all :plural',
			'ungrouped' => ':plural without group',
			'manage' => 'manage groups',
		],
		'serve' => 'Download file',
		'crop' => 'crop',
		'no_ratio' => 'no ratio',
		'with_selected' => 'With selected...',
		'delete_instance' => 'Only remove link',
		'delete_original' => 'Remove original',
		'existing_images' => 'existing images',
		'redirect_from' => 'from url',
		'redirect_to' => 'to url',
		'popular' => 'frequently used',
		'readability' => 'readability',
		'findability' => 'searchability',
		'improve' => 'improve',
		'seo_title' => 'title',
		'seo_description' => 'description',
		'seo_keyword' => 'keyword',
	],
	'option' => [
		'parent_id' => [
			0 => 'place in main group',
			'root' => 'as root element'
		],
		'status' => [
			'live' => 'live',
			'review' => 'reviewing',
			'edit' => 'in process',
			'new' => 'new',
			'active' => 'active',
			'blocked' => 'blocked'
		],
		'roles' => [
			'admin' => 'Administrator',
			'manager' => 'manager',
			'manager_main' => 'manager main website',
			'member' => 'only access rights',
			'user' => 'no access',
		],
		'lang' => [
			'nl' => 'Dutch',
			'en' => 'English'
		],
		'type' => [
			'default' => 'Default',
			'html' => 'Custom html',
		]
	],
	'dialog' => [
		'delete' => [
			'title' =>  'Confirm removal',
			'message' => 'this change can\'t be undone. Do you want to remove :article :single ?'
		],
		'claim' => [
			'title' =>  'Confirm edit',
			'message' => ':article :single is in use by {{username}}. Do you want to claim :article :single? Unsaved changes can be recoverd by {{username}}.'
		],
		'claimed' => [
			'message' => ':article :single is claimed by {{username}}. Your changes have been saved and can be recovered the next time you edit :article :single'
		],
		'restore' => [
			'title' =>  'Found changes',
			'message' => 'There are unsaved changes for :article :single . Would you like to recover these changes? These can be saved afterwards.'
		],
		'delete_file' => [
			'title' =>  'What would you like to remove',
			'message' => 'Would you like to remove the file (this can not be undone), or just this instance?'
		],
		'delete_image' => [
			'title' =>  'What would you like to remove',
			'message' => 'Would you like to remove the iamge-file (this can not be undone), or just this instance?'
		],
		'delete_batch' => [
			'title' =>  'Confirm delete',
			'message' => 'This change can not be undone. Do you want to continue removing the :plural ?.'
		],
		'abandon' => [
			'title' =>  'Unsaved changes',
			'message' => 'Unsaved changes found. These changes will be lost when proceeding.<br /><br />Continue anyway?'
		]
	],
	'message' => [
		'created' => ':article :single created',
		'updated' => ':article :single updated',
		'deleted' => ':article :single removed',
		'copied' => ':article :single copied',
		'invalid' => ':article :single contains errors and was not saved',
		'uploaded' => 'Upload successful',
		'not_uploaded' => 'Upload failed',
	],

	'time' => [
		'days' => ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'],
		'days_short' => ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'],
		'months' => ['Januari', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		'months_short' => ['jan', 'feb', 'march', 'april', 'may', 'june', 'july', 'aug', 'sep', 'oct', 'nov', 'dec'],
		'now' => 'now',
		'today' => 'today',
		'hour' => 'hours',
		'minute' => 'minutes',
		'date' => 'date',
		'time' => 'time',
		'format' =>[
			'date' => 'DD d MM yy',
			'time' => 'hh:mm',
			'separator' => ' at ',
		]
	],


	'settings' => [
		'name' => 'Settings',
		'article' => 'the',
		'single' => 'Setting',
		'plural' => 'Settings',
		'new' => 'new',
		'description' => 'Settings',
		'field' => [
			'head' => 'Html added to the head-element of all pages',
			'body_before' => 'Html added to the start of the  body-element of all pages',
			'body_after' => 'Html added to the end of the  body-element of all pages',
		]
	],


	'user' => [
		'name' => 'Users',
		'article' => 'the',
		'single' => 'User',
		'plural' => 'Users',
		'new' => 'new',
		'description' => 'user management',
	],


	'preferences' => [
		'name' => 'Preferences',
		'article' => 'the',
		'single' => 'Preference',
		'plural' => 'Preferences',
		'new' => 'new',
		'description' => 'Account preferences',
	],


	'block' => [
		'type' => [
			'images' => 'Gallery',
			'image' => 'Image',
			'link' => 'Link',
			'html' => 'Text',
			'quote' => 'Quote',
			'aside' => 'Aside',
			'embed' => 'Embed',
			'header' => 'Header',
		]
	],


	'file' => [
		'name' => 'Files',
		'article' => 'the',
		'single' => 'File',
		'plural' => 'Files',
		'new' => 'new',
		'description' => 'Uploaded files',
	],


	'image' =>  [
		'name' => 'Images',
		'article' => 'the',
		'single' => 'Image',
		'plural' => 'Images',
		'new' => 'new',
		'description' => 'Uploaded images',
	],


	'link' => [
		'field' => [
			'title' => 'Link-text',
		],
		'preset' => []
	],


	'menu' => [
		'name' => 'Menu',
		'article' => 'the',
		'single' => 'Menu-item',
		'plural' => 'Menu-items',
		'new' => 'new',
		'description' => 'Website menu\'s',
		'field' => [
			'title' => 'Text on the menu-item',
			'type' => 'What kind of menu-item',
		],
	],


	'home' => [
		'name' => 'Homepage',
		'article' => 'the',
		'single' => 'Homepage',
		'plural' => 'Homepages',
		'new' => 'new',
		'description' => 'homepage management',
	],


	'tag' => [
		'name' => 'Tags',
		'article' => 'the',
		'single' => 'tag',
		'plural' => 'tags',
		'new' => 'new',
		'description' => 'Conent-tags',
	],


	'form' => [
		'name' => 'Form',
		'article' => 'the',
		'single' => 'form',
		'plural' => 'forms',
		'new' => 'new',
		'description' => 'Form manager',
	]
];