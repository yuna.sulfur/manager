<?php
use Sulfur\Identity;
use Sulfur\Upload;

return [
	'section' => [
		'start' => 'Start',
		'structure' => 'Indeling',
		'content' => 'Inhoud',
		'files' => 'Bestanden',
		'users' => 'Gebruikers',
		'settings' => 'Instellingen'
	],
	'title' => [
		'index' => ':name overzicht',
		'list' => ':name overzicht',
		'create' => ':new :single',
		'update' => ':single bewerken',
		'roles' => 'Rollen bewerken',
		'drop' => 'Klik of sleep je bestanden om te uploaden'
	],
	'field' => [
		'slug' => 'Deel van de url',
		'created' => 'aangemaakt',
		'timestamp' => 'gewijzigd',
		'title' => 'titel',
		'subtitle' => 'subtitel',
		'description' => 'omschrijving',
		'alias' => 'url alias',
		'status' => 'status',
		'intro' => 'intro',
		'body' => 'tekst',
		'preview' => 'preview',
		'blocks' => 'blokken',
		'owner_id' => 'eigenaar',
		'editor_id' => 'gewijzigd door',
		'meta_title' => 'meta titel',
		'meta_keywords' => 'meta keywords',
		'meta_description' => 'meta description',
		'time_start' => 'vanaf',
		'time_end' => 'tot',
		'use_time_start' => 'tonen vanaf',
		'use_time_end' => 'tonen tot',
		'username' => 'gebruikersnaam',
		'password' => 'wachtwoord',
		'email' => 'e-mailadres',
		'screenname' => 'schermnaam',
		'submit' => 'Verstuur',
		'roles' => 'rollen',
		'lang' => 'Taal',
		'file' => 'upload',
		'url' => 'Link',
		'blank' => 'Link in een nieuw venster openen',
		'nofollow' => 'rel="nofollow" attribuut toevoegen',
		'parent_id' => 'Plaatsen in',
		'link' => 'Link',
		'html' => 'Html',
		'login' => 'Inloggen',
		'timed' => ' ',
		'form' => 'formulier',
		'caption' => 'bijschrift',
		'credits' => 'Bron / credits',
		'alt' => 'Alt-attribuut',
		'image' => 'Afbeelding',
		'images' => 'Afbeeldingen',
		'file' => 'Bestand',
		'files' => 'Bestanden',
		'redirect' => 'Redirects',
		'seo' => 'Vindbaarheid en leesbaarheid',
		'tags' => 'tags',
		'owner' => 'eigenaar',
		'by' => 'door',
		'embed' => 'embed',
		'timed' => 'publicatietijd',
	],
	'error' => [
		'title' => ['required' => 'vul een titel in'],
		'alias' => ['required' => 'vul een url in'],
		Identity::ERROR_CREDENTIALS => 'Onjuiste inloggegevens',
		Identity::ERROR_THROTTLE => 'Te veel inlogpogingen gedaan. Probeer het over een aantal minuten nog een keer',
		Identity::ERROR_UNKNOWN => 'Onbekende gebruiker',
		Sulfur\Upload::ERROR_MAX_UPLOAD_SIZE => 'Bestand :file is een te grote upload',
		Sulfur\Upload::ERROR_MAX_FILE_SIZE => 'Bestand :file is te groot',
		Sulfur\Upload::ERROR_PARTIAL_UPLOAD => 'Bestand :file is niet compleet',
		Sulfur\Upload::ERROR_MISSING_UPLOAD => 'Bestand :file is niet gevonden',
		Sulfur\Upload::ERROR_MISSING_TEMP => 'Geuploade bestand :file is niet gevonden',
		Sulfur\Upload::ERROR_WRITING => 'Bestand :file kon niet weggeschreven worden',
		Sulfur\Upload::ERROR_PHP_EXTENSION => 'Bestand :file niet geupload door een serverfout',
		Sulfur\Upload::ERROR_DISABLED => 'Uploads zijn niet toegestaan',
		Sulfur\Upload::ERROR_KEY_MISSING => 'Geen uploads gevonden',
		Sulfur\Upload::ERROR_FILTER_SIZE => 'Bestand :file is groter dan toegestaan',
		Sulfur\Upload::ERROR_FILTER => 'Bestand :file is niet toegestaan',
		Sulfur\Upload::ERROR_OTHER => 'Upload van :file is mislukt',
		'username' => [
			'required' => 'Vul een gebruikersnaam in',
			'valid' => 'Gebruikersnaam mag alleen letters en cijfers bevatten',
			'unique' => 'Gebruikersnaam bestaat al',
		],
		'email' => [
			'required' => 'Vul een e-mailadres in',
			'email' => 'Vul een geldig e-mailadres in',
			'unique' => 'E-mailadres is al in gebruik',
		],
		'password' => [
			'required' => 'Vul een wachtwoord in',
			'password' => 'Vul een wachtwoord van minimaal zes karakters in',
		],
	],

	'label' => [
		'frontend' => 'Naar frontend',
		'start' => 'start',
		'logout' => 'uitloggen',
		'login' => 'inloggen',
		'ok' => 'ok',
		'cancel' => 'annuleer',
		'back' => 'terug',
		'confirm' => 'ok',
		'save' => 'opslaan',
		'saveclose' => 'opslaan en sluiten',
		'submit' => 'opslaan',
		'submit_back' => 'opslaan en terug',
		'update' => 'wijzig',
		'delete' => 'verwijder',
		'show' => 'toon',
		'hide' => 'verberg',
		'preview' => 'preview',
		'select' => 'selecteren',
		'use' => 'gebruiken',
		'apply' => 'toepassen',
		'filter' => 'zoeken',
		'sort' => 'sorteer',
		'open' => 'open',
		'close' => 'sluit',
		'previous' => 'vorige',
		'next' => 'volgende',
		'view' => 'bekijk',
		'create' => 'nieuw',
		'add' => 'toevoegen',
		'add_image' => 'afbeelding toevoegen',
		'add_file' => 'document toevoegen',
		'add_video' => 'filmpje toevoegen',
		'add_image' => 'afbeelding toevoegen',
		'link_create' => 'link toevoegen',
		'link_update' => 'link wijzigen',
		'link_delete' => 'link verwijderen',
		'delete_link' => 'verwijder',
		'choose' => 'maak een keuze',
		'browse' => 'selecteer',
		'content' => 'inhoud',
		'media' => 'media',
		'seo' => 'zoekmachines',
		'settings' => 'instellingen',
		'collection' => 'koppelingen',
		'text' => 'tekst',
		'by' => 'door',
		'revision' => 'versies',
		'group' => [
			'create' => 'nieuwe groep',
			'all' => 'alle :plural',
			'ungrouped' => ':plural zonder groep',
			'manage' => 'groepen beheren',
		],
		'serve' => 'Bestand downloaden',
		'crop' => 'Bijsnijden',
		'no_ratio' => 'vrije verhouding',
		'with_selected' => 'Met geselecteerd...',
		'delete_instance' => 'alleen koppeling verwijderen',
		'delete_original' => 'origineel verwijderen',
		'existing_images' => 'bestaande afbeeldingen',
		'redirect_from' => 'van url',
		'redirect_to' => 'naar url',
		'popular' => 'veel gebruikt',
		'readability' => 'leesbaarheid',
		'findability' => 'vindbaarheid',
		'improve' => 'verbeteren',
		'seo_title' => 'titel',
		'seo_description' => 'omschrijving',
		'seo_keyword' => 'steekwoord',
	],
	'option' => [
		'parent_id' => [
			0 => 'in hoofdgroep plaatsen',
			'root' => 'Als root element plaatsen'
		],
		'status' => [
			'live' => 'gepubliceerd',
			'review' => 'in behandeling',
			'edit' => 'in bewerking',
			'new' => 'nieuw',
			'active' => 'actief',
			'blocked' => 'geblokkeerd'
		],
		'roles' => [
			'admin' => 'hoofdbeheerder',
			'manager' => 'beheerder',
			'manager_main' => 'beheerder hoofdwebsite',
			'member' => 'alleen toegang tot beheer',
			'user' => 'geen toegang tot beheer',
		],
		'lang' => [
			'nl' => 'Nederlands',
			'en' => 'Engels'
		],
		'type' => [
			'default' => 'Standaard',
			'html' => 'Eigen html',
		]
	],
	'dialog' => [
		'delete' => [
			'title' =>  'Verwijderen bevestigen',
			'message' => 'Deze wijziging kan niet ongedaan gemaakt worden. Wilt u :article :single verwijderen?.'
		],
		'claim' => [
			'title' =>  'Aanpassen bevestigen',
			'message' => ':article :single is op dit moment in bewerking door {{username}}. Wilt u het bewerken overnemen? De wijzigingen die nog niet zijn opgeslagen blijven bewaard.'
		],
		'claimed' => [
			'message' => ':article :single is door {{username}} overgenomen. Uw wijzigingen zijn automatisch opgeslagen en kunnen worden opgehaald als het artikel opnieuw wordt bewerkt.'
		],
		'restore' => [
			'title' =>  'Wijzigingen gevonden',
			'message' => 'Voor :article :single zijn wijzigingen gevonden die niet opgeslagen zijn. Wilt u de wijzigingen ophalen? U kunt ze daarna opslaan.'
		],
		'delete_file' => [
			'title' =>  'Wat wilt u verwijderen',
			'message' => 'Wilt u het bestand verwijderen (dit kan niet ongedaan gemaakt worden), of alleen de hier gebruikte koppeling?'
		],
		'delete_image' => [
			'title' =>  'Wat wilt u verwijderen',
			'message' => 'Wilt u het afbeeldingsbestand verwijderen (dit kan niet ongedaan gemaakt worden), of alleen de hier gebruikte koppeling?'
		],
		'delete_batch' => [
			'title' =>  'Verwijderen bevestigen',
			'message' => 'Deze wijziging kan niet ongedaan gemaakt worden. Wilt u de :plural verwijderen?.'
		],
		'abandon' => [
			'title' =>  'Niet opgeslagen wijzigingen',
			'message' => 'Er zijn niet opgeslagen wijzigingen. Als u doorgaat gaan deze wijzigingen verloren.<br /><br />Wilt u doorgaan?'
		]
	],
	'message' => [
		'created' => ':article :single is aangemaakt',
		'updated' => ':article :single is aangepast',
		'deleted' => ':article :single is verwijderd',
		'copied' => ':article :single is gekopieerd',
		'invalid' => ':article :single bevat nog fouten en is niet opgeslagen',
		'uploaded' => 'Uploaden is gelukt',
		'not_uploaded' => 'Uploaden is niet gelukt',
	],

	'time' => [
		'days' => ['zondag', 'maandag', 'dinsdag', 'woensdag', 'donderdag', 'vrijdag', 'zaterdag'],
		'days_short' => ['zo', 'ma', 'di', 'wo', 'do', 'vr', 'za'],
		'months' => ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'],
		'months_short' => ['jan', 'feb', 'mrt', 'apr', 'mei', 'jun', 'jul', 'aug', 'sep', 'okt', 'nov', 'dec'],
		'now' => 'nu',
		'today' => 'vandaag',
		'hour' => 'uur',
		'minute' => 'minuut',
		'date' => 'datum',
		'time' => 'tijd',
		'format' =>[
			'date' => 'DD d MM yy',
			'time' => 'hh:mm',
			'separator' => ' om ',
		]
	],


	'settings' => [
		'name' => 'Instellingen',
		'article' => 'de',
		'single' => 'Instelling',
		'plural' => 'Instellingen',
		'new' => 'nieuwe',
		'description' => 'Instellingen',
		'field' => [
			'head' => 'Html in de <head> van alle pagina\'s',
			'body_before' => 'Html aan het begin van de <body> van alle pagina\'s',
			'body_after' => 'Html aan het eind van de <body> van alle pagina\'s',
		]
	],


	'user' => [
		'name' => 'Gebruikers',
		'article' => 'de',
		'single' => 'Gebruiker',
		'plural' => 'Gebruiker',
		'new' => 'nieuwe',
		'description' => 'beheer gebruikers',
	],


	'preferences' => [
		'name' => 'Voorkeuren',
		'article' => 'de',
		'single' => 'Instelling',
		'plural' => 'Instellingen',
		'new' => 'nieuwe',
		'description' => 'Account instellingen',
	],


	'block' => [
		'name' => 'Blok',
		'article' => 'de',
		'single' => 'Blok',
		'plural' => 'blokken',
		'new' => 'nieuw',
		'description' => 'Beheer van blokken',
		'type' => [
			'images' => 'Gallerij',
			'image' => 'Afbeelding',
			'link' => 'Link',
			'html' => 'Tekst',
			'quote' => 'Quote',
			'aside' => 'Kadertekst',
			'embed' => 'Embed',
			'header' => 'Koptekst',
			'download' => 'Download',
			'form' => 'Formulier',
			'card' => 'Preview',
		],
		'label' => [
			'add_content' => 'Klik hier om content toe te voegen',
			'add_image' => 'Klik hier om een afbeelding toe te voegen',
			'add_images' => 'Klik hier om afbeeldingen toe te voegen',
		],
		'option' =>[
			'position' => [
				'column' => 'Breedte van de tekstkolom',
				'full' => 'Breder dan de tekstkolom',
				'outside' => 'In sidebar',
				'left' => 'In de tekst, links uitgelijnd',
				'left_small' => 'In de tekst, links uitgelijnd klein ',
				'right' => 'In de tekst, rechts uitgelijnd',
				'right_small' => 'In de tekst, rechts uitgelijnd klein',
			]
		]
	],


	'file' => [
		'name' => 'Bestanden',
		'article' => 'het',
		'single' => 'Bestand',
		'plural' => 'Bestanden',
		'new' => 'nieuw',
		'description' => 'Beheer van bestanden',
	],


	'image' =>  [
		'name' => 'Afbeeldingen',
		'article' => 'de',
		'single' => 'Afbeelding',
		'plural' => 'Afbeeldingen',
		'new' => 'nieuwe',
		'description' => 'Beheer van afbeeldingen',
		'preset' => [
			'manager' => 'beheer',
			'thumb' => 'thumbnail',
			'normal' => 'normaal'
		]
	],


	'link' => [
		'field' => [
			'title' => 'Linktekst',
		],
		'preset' => [
			'home' => 'Homepage'
		]
	],


	'menu' => [
		'name' => 'Menu',
		'article' => 'het',
		'single' => 'Menu-item',
		'plural' => 'Menu-items',
		'new' => 'nieuw',
		'description' => 'Beheer van menu\'s',
		'field' => [
			'title' => 'Tekst op het menu-item',
			'type' => 'Wat doet het menu-item',
		],
	],


	'home' => [
		'name' => 'Homepage',
		'article' => 'de',
		'single' => 'homepage',
		'plural' => 'homepages',
		'new' => 'nieuwe',
		'description' => 'Beheer homepage',
	],


	'tag' => [
		'name' => 'Tags',
		'article' => 'de',
		'single' => 'tag',
		'plural' => 'tags',
		'new' => 'nieuwe',
		'description' => 'Conent-tags',
	],


	'form' => [
		'name' => 'Formulieren',
		'article' => 'het',
		'single' => 'formulier',
		'plural' => 'formulieren',
		'new' => 'nieuw',
		'description' => 'Formulierenmanager',
		'field' => [
			'label' => 'Tekst boven het invulveld',
			'info' => 'Extra informatie bij het invulveld',
			'type' => 'Type invulveld',
			'type' => 'Type invulveld',
			'error' => 'Foutmelding',
			'subject' => 'Onderwerp',
			'submit' => 'Tekst op verstuur knop',
			'success' => 'Tekst na succesvol versturen',
			'from' => 'Van e-mailadres',
			'sender' => 'Van naam',
			'webhook' => 'Webhook url',
			'javascript' => 'Javascript uitvoeren',
			'preset' => 'Actie',
			'to' => 'Naar e-mailadres (gebruik eventueel email-velden). Meerdere adressen mogelijk',
			'message' => 'Tekst bericht (gebruik eventueel formulier-velden)',
		],
		'label' => [
			'no_label' => '<i>&lt;Geen tekst ingevuld&gt;</i>',
			'no_error' => '<i>&lt;Geen foutmelding ingevuld&gt;</i>',
			'required' => 'Invuldveld is verplicht',
			'hook' => 'Na succesvol vesturen',
			'email' => 'E-mail',
			'javascript' => 'Javascript uitvoeren',
			'webhook' => 'Webhook',
			'preset' => 'Andere actie',
		],
		'option' => [
			'type' => [
				'text' => 'Tekstveld',
				'textarea' => 'Groot tekstveld',
				'radio' => 'Keuze: een keuze mogelijk',
				'checkbox' => 'Keuze: meerdere keuzes mogelijk',
				'email' => 'E-mailadres',
				'toggle' => 'Aanvinkveld',
			]
		]
	]
];