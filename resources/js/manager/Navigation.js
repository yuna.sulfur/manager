define('manager.Navigation')
.as(function(y) {
	this.start = function(scope) 
	{
		var index = sessionStorage.getItem('expanded');
		
		
		if(index || index === 0) {
			var item = scope.fetch('expandable').eq(index);
			item.addClass('expanded');
			item.fetch('more').hide();
			item.fetch('less').show();
			item.fetch('expanded').show();
		}
								
		scope.fetch('button').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			
			var target = y(this).attr('target');
			if(target) {
				window.open(y(this).attr('href'), target);
			} else {
				document.location.href = y(this).attr('href');
			}
		});
		
		
		scope.fetch('expand').click(function(e){
			scope.addClass('expanded');
		});
		
		scope.fetch('overlay').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			scope.removeClass('expanded');
		});
		
		scope.fetch('expandable').click(function(e) {
			scope.fetch('more').show();
			scope.fetch('less').hide();
			
			scope.fetch('expanded').slideUp(100);
			
			
			var self = y(this);
			var expanded = self.hasClass('expanded');
			
			scope.fetch('expandable').removeClass('expanded');
			
			if(! expanded) {
				self.addClass('expanded');
				self.fetch('more').hide();
				self.fetch('less').show();
				self.fetch('expanded').slideDown(100);
				
				// store the expanded state
				sessionStorage.setItem('expanded', scope.fetch('expandable').index(self));
			}
		});
	}
	
});
