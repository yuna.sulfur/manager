define('manager.Post')
.use('yellow.View')
.template('form', '<form action="{{ action }}" method="POST"{% if target %} target="{{ target }}"{% endif %}></form>')
.template('hidden', '<input type="hidden" />')
.as(function(y, View, template) {
	
	var _url;
	
	var _maxDepth = 10;
	
	this.start = function(url) 
	{
		_url = url;
	}
	
	
	this.submit = function(data, target)
	{
		// create a temp form
		var form = View.make(template('form')).element({
			action: _url,
			target: target
		});
		// populate the form with serialized post values
		var values = serialize(data);
		for(var i = 0; i < values.length; i++) {
			var hidden = View.make(template('hidden')).element();
			hidden.attr('name', values[i].name);
			hidden.val(values[i].value);
			form.append(hidden);
		}
		// submit and remove
		y('body').append(form);
		form.submit().remove();
	}
	
	
	this.ajax = function(data, done, fail, always)
	{
		y.ajax(_url, {
			data: serialize(data),
			type: 'POST',
			dataType: 'JSON'
		}).done(function(data) {
			if(y.isFunction(done)) {
				done(data)
			}
		}).fail(function(data) {
			if(y.isFunction(fail)) {
				fail(data)
			}
		}).always(function(data) {
			if(y.isFunction(always)) {
				always(data)
			}
		})
	}
	
	
	/**
	 * serialize a js object into post format
	 */
	var serialize = function(data, serialized, name, depth)
	{
		if(! y.isSet(serialized)) {
			// first run
			serialized = [];
			name = ''
		}
		
		if(! y.isSet(depth)) {
			// first run
			depth = 0;
		}
		
		// dont exceed max depth, as it will create an enormous post array.
		// stop parsing: just return
		depth++;
		if(depth > _maxDepth) {
			return serialized;
		}
	
		if(y.isObject(data)) {
			// serialize object, append [key] to name
			for(var key in data) {
				if(name === '') {
					serialized = serialize(data[key], serialized, key, depth);	
				} else {
					serialized = serialize(data[key], serialized, name + '[' + key + ']', depth);	
				}
			}
		} else if (y.isArray(data)) {
			// serialize array, append [i] to name
			for(var i = 0; i < data.length; i++) {
				serialized = serialize(data[i], serialized, name + '[' + i + ']', depth);	
			}
		} else {
			// a scalar: we can push it to the post array
			serialized.push({ name: name, value: data})
		}
		return serialized;
	}
	
	
	/**
	 * unserialize post format to object
	 */
	var unserialize = function(values)
	{
		var data = {};
		for(var i = 0; i < values.length; i++) {
			var name = values[i].name;
			var value = values[i].value;

			var base = name.split('[')[0];
			if(! data[base]) {
				data[base] = '__empty__';
			}
			var put = data;
			var current = base;
			var regex = /\[([^\]]*)\]/g;
			var match;
			while (match = regex.exec(name)) {
				if(/[0-9]+/.test(match[1])) {
					var key = Number(match[1]);
					if(put[current] === '__empty__') {
						put[current] = [];
					}
					if(put[current] instanceof Array) {
						put[current][key] = '__empty__';
						put = put[current];
						current = key;
					} else {
						break;
					}
				} else if(match[1] === '') {
					if(put[current] === '__empty__') {
						put[current] = [];
					}
					if(put[current] instanceof Array) {
						put[current].push('__empty__');
						put = put[current];
						current = put.length - 1;
					} else {
						break;
					}
				} else {
					var key = match[1];
					if(put[current] === '__empty__') {
						put[current] = {};
					}
					if(put[current] instanceof Object && ! (put[current] instanceof Array)) {
						put[current][key] = '__empty__';
						put = put[current];
						current = key;
					} else {
						break;
					}
				}
			}
			put[current] = value;
		}
		return data;
	}
});



