define('manager.Message')
		
.use('yellow.View')

.template('message', '\
<div class="flashmessage">\
	<div class="alert alert-{{ type }}" role="alert">\
		{{ message }}\
		<button type="button" class="close" data-dismiss="alert">\
		  <span aria-hidden="true">&times;</span>\
		  <span class="sr-only">Close</span>\
		</button>\
	</div>\
</div>\
')
.as(function(y, View, template)
{
	this.start = function(message, type)
	{
		var types = {
			success: 'success',
			error: 'danger'
		};
		
		var message = View.make(template('message')).element({
			message: message,
			type: types[type]
		}).appendTo('body');
		
		
		setTimeout(function(){
			message.fadeOut(function(){
				this.remove();
			});
		}, 4000);
	}
});