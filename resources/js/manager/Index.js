define('manager.Index')
.use('yellow.View')
.use('manager.Loading')
.as(function(y, View, Loading){
	
	var _scope;
	var _url = '';
	var _url_move = '';
	var _filter = [];
	var _filters = [];
	var _sort = [];
	var _search = [];
	var _skip = 0;
	var _amount = 0;
	var _total = false;
	var _tree = false;
	var _sortable = false;
	var _lock = false;
	var _url_locked = false;
	
	this.start = function(scope)
	{
		// globalize vars
		_scope = scope;
		_url = scope.data('url');
		_url_move = scope.data('url_move');
		_filter = scope.data('filter');
		_filters = scope.data('filters');
		_sort = scope.data('sort');
		_search = scope.data('search');
		_skip = scope.data('skip');
		_amount = scope.data('amount');
		_tree = scope.data('tree');
		_sortable = scope.data('sortable');
		_lock = scope.data('lock');
		_url_locked = scope.data('url_locked');
		
		// create elements
		index();
		
		// add search and filters
		if(! _tree && ! _sortable) {
			search();
			filters();
		}
	
		// lock
		if(_lock) {
			lock();
		}
	
		// start load
		load();
	}
	
	
	
	/**
	 * Start up search field
	 */
	var search = function()
	{
		var search = View.make(_scope.template('search')).element({
			value: _search
		})
		.appendTo(_scope.fetch('search'));

		var remove = search.fetch('remove');
		var input = search.fetch('input');
		var submit = search.fetch('submit');
		
		input.on('keyup', function (e) {
			if (e.keyCode == 13) {
				submit.click();
			}
		});
		
		submit.click(function(){
			var val = input.val();
			if(val !== _search){
				// set searchval
				_search = val;
				// recalculate total
				_total = false;
				// start a beginning
				_skip = 0;
				// load new data
				load();
			}

			remove.detach();
			if(val){
				remove.prependTo(search);
			}
		});
		
		remove.click(function(){
			input.val('');
			submit.click();
		});
		
		
		input.keyup(function (e) {
            if (e.keyCode === '13') {
				e.preventDefault();
				e.stopPropagation();
                submit.click();
            }
        });
		
		if(!_search){
			remove.detach();
		}
	};
	
	
	/**
	 * Start up filters
	 */
	var filters = function()
	{
		for(var name in _filters){
			// create a filterbox
			var filter = View.make(_scope.template('filter'))
			.element({
				name : name,
				options : _filters[name]
			})
			.appendTo(_scope.fetch('filters'));
	
			// set change handler
			filter.fetch('select')
			.change('dit is data', function(e){
				var el = y(this);
				var filter = el.fetch('filter', 'parent');
				var remove = filter.fetch('remove');
				var name = el.attr('name');
				var val = el.val();
				if(val === '-1') {
					// no value selected
					remove.hide();
					filter.removeClass('input-group');
					delete(_filter[name]);
				} else {
					// value selected
					remove.show();
					filter.addClass('input-group');
					_filter[name] = val;
				}
				// only load if allowed
				if(filter.data('load')){
					// recalculate total
					_total = false;
					// start a beginning
					_skip = 0;
					// load new data
					load();
				}
			});
			
			// remove button
			filter.fetch('remove')
			.click(function(e){
				var filter = y(this).fetch('filter', 'parent');
				filter.fetch('select')
				.val('-1')
				.change();
			});
	
			// init value
			// dont load on the initial change
			filter.data('load', false);
			if(y.isSet(_filter[name])){
				filter.fetch('select')
				.val(_filter[name])
				.change();
			} else {
				filter.fetch('select')
				.val('-1')
				.change();
			}
			// from now on load data on change
			filter.data('load', true);
		}
	};
	
	
	/**
	 * Create sortable selectable container for items
	 */
	var index = function()
	{
		// create list
		var element = View.make(_scope.template('list')).element();
		
		// append it
		element.appendTo(_scope.fetch('list'));
		
		// make sorters work
		element.fetch('sort').each(function(){
			var element = y(this);
			var column = element.data('column');
			var direction = element.data('direction');
			
			if(y.isSet(_sort[column])){
				direction = _sort[column];
				element.fetch(direction).show();
			} 
			element.click(function(e){
				e.preventDefault();
				
				// turn off all arrows
				element.fetch('asc').hide();
				element.fetch('desc').hide();
				
				// change direction
				direction = direction == 'asc' ? 'desc' : 'asc';
				
				// turn on this one
				element.fetch(direction).show();
				
				// set new state
				_sort = {};
				_sort[column] = direction;
				_skip = 0;
				
				//load again
				load();
				
				// paginate
				if(! _tree && ! _sortable) {
					pagination();
				}
			})
		})
	};
	
	
	/**
	 * periodically get locks
	 * @returns
	 */
	var lock = function()
	{
		setInterval(function() {
			var ids = []
			 _scope.fetch('item').each(function(){
				 ids.push(y(this).data('id'))
			});
			y.ajax(_url_locked, {
				dataType: 'json',
				type: 'POST',
				data: {id: ids.join(',')}
			})
			.done(function(data){
				_scope.fetch('item').each(function(){
					if(data[y(this).data('id')]) {
						y(this).fetch('locked').text(data[y(this).data('id')]).show();
					} else {
						y(this).fetch('locked').hide();
					}
			   });
			});
		}, 5 * 1000)
	}
	
	
	/**
	 * Load the data
	 */
	var load = function()
	{
		var container = _scope.fetch('container');
		// create url
		var urlSort = [];
		for(var column in _sort){
			urlSort.push(column + '=' + _sort[column]);
		}
		urlSort = urlSort.join(';');
		
		
		var urlFilter = [];
		for(var column in _filter){
			urlFilter.push(column + '=' + _filter[column]);
		}
		urlFilter = urlFilter.join(';');
		
		
		var url = _url
		.replace('{{count}}', _total === false ? '1' : '0')
		.replace('{{amount}}', _amount ? _amount : -1)
		.replace('{{skip}}', _skip)
		.replace('{{filter}}', urlFilter)
		.replace('{{sort}}', urlSort)
		.replace('{{search}}', _search);

		// show loading
		Loading.show();
		
		// start loading
		y.ajax(url, {
			dataType: 'json'
		})
		.done(function(data){
			
			// empty list
			container.empty();
			
			// add new items
			add(data.items);
			
			// hide loading
			Loading.hide();
			
			// add drag handlers
			if(_sortable) {
				sortable(container);
			}
		
			// build the pagination if total wasn't known yet
			// this happens only the first time
			if(_total === false){
				// set the total
				_total = data.total;
				if(! _tree && ! _sortable) {
					// build the pagination
					pagination();
				}
			}
		});
	};
	
	
	var add = function(items, before)
	{
		// get the container
		var container = _scope.fetch('container');
		
		// create the items
		var view = View.make(_scope.template('item'));

		var map = {};
		for(var i = 0; i < items.length; i++){
			// render element
			var element = view.element({
				item: items[i]
			});
			// add item data for use by other parts
			element.data('item', items[i].data);
			
			// mark locked
			if(_lock && items[i].data.editor) {
				element.fetch('update').invoke('lock', items[i].data.editor.screenname)
			}
			
			if(_tree) {
				// build a map for a tree
				map['id_' + items[i].data.id] = element;
				element.data('parentId', items[i].data.parent_id)
			} else {
				// just add it for a regular table
				if(before) {
					element.prependTo(container);
				} else {
					element.appendTo(container);
				}
			}
		}	

		if(_tree) {
			// build tree
			for(var id in map) {
				var element = map[id];
				var parentId = element.data('parentId');
				if(parentId == 0) {
					// root element
					if(before) {
						element.prependTo(container);
					} else {
						element.appendTo(container);
					}
				} else if(y.isSet(map['id_' + parentId])) {
					// child element
					element.appendTo(map['id_' + parentId].fetch('children').first());
				}
			}
		}
	}
	
	
	/**
	 * public prepend
	 * @param array items
	 * @returns void
	 */
	this.add = function(items, before)
	{
		add(items, before);
	}
	
	
	var sortable = function(container)
	{
		container.nestedSortable({
			handle: '[y-name=move]',
			items: '[y-name^=item]',
			helper: 'clone',
			opacity: .6,
			revert: 250,
			placeholder: 'placeholder',
			//toleranceElement: '> div',
			listType: container.prop('tagName').toLowerCase(),
			isTree: _tree
		}).on('sortstop', function(event,ui) {
			// get the moved id
			var id = ui.item.data('id');

			// get the item after which it moved
			var after = 0;
			var prev = ui.item.prev();
			if(prev.length > 0){
				after = prev.data('id');
			}
			
			// get the parent in which it moved
			var parent = '';
			if(_tree) {
				var closest = ui.item.fetch('item', 'parents').first();
				if(closest.length > 0){
					parent = closest.data('id');
				} else {
					parent = 0;
				}
			}


			// send out ajax req
			y.ajax(
				_url_move.replace('{{id}}', id)
				.replace('{{after}}', after)
				.replace('{{parent}}', parent), 
				{
					dataType: 'json',
					type : 'POST',
					data: {
						csrf: _scope.data('csrf')
					}
				}
			);
		});
	}


	/**
	 * Build pagination
	 */
	var pagination = function()
	{
		if(_amount) {
			// renderdata
			var data = {
				first: _skip + 1,
				last: _total > _amount ? _skip + _amount : _total,
				total: _total,
				previous: false,
				next: false
			};
			
			if(_total > _amount){
				// get steps
				var steps = Math.ceil(_total/_amount);

				// add  previous
				if(_skip > 0){
					data.previous = (_skip - _amount < 0) ? 0 : (_skip - _amount)
				}
				
				// add next
				if(_skip < (steps - 1) * _amount){
					data.next = _skip + _amount;
				} 
			}
			// render pagination
			var element = View.make(_scope.template('pagination')).element(data);
			
			// add actions
			element.fetch('button').click(function(e){
				// no real click
				e.preventDefault();
				// set offset
				_skip = y(this).data('skip');
				// load new content
				load();
				// build it again
				pagination();
			})
			
			
			// add it to the html
			_scope.fetch('pagination')
			.empty()
			.append(element);

		}
	}
});