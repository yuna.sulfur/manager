define('manager.Date')
//.use('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment-with-locales.min.js', 'moment')
//.use('moment.Moment', 'moment')
.as(function(y) {
	
	this.start = function(scope) 
	{
		if(scope.data('date')) {
			var offset = scope.data('offset') || 0;
			moment.locale(scope.data('lang'));
			scope.text(
				moment(scope.data('date'))
				.add(offset, 'm')
				.format(scope.data('format'))
			);
		}
	}
});