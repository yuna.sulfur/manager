define('manager.Main')
.use('manager.Dialog')
.as(function(y, Dialog){
	
	var _scope;
	
	this.start = function(scope)
	{
		_scope = scope;
		
		// set dialog language
		Dialog.lang = scope.data('lang');
		
		// fixed header
		var header = _scope.fetch('header-fixed');
		header.css({
			position: 'relative',
			width: '100%'
		});
		var width = header.outerWidth();
		header.css({
			position: 'fixed',
			top: 0,
			width:  width + 'px'
		});
		
		
		// adjust header ehight
		var adjustHeight = function(height){
			header.parent().css('padding-top', height + 'px');
		}
		
		// First time
		var height = header.outerHeight();
		adjustHeight(height)
		
		// Keep checking
		setInterval(function(){
			var newHeight = header.outerHeight();
			if(newHeight != height) {
				adjustHeight(newHeight);
				height = newHeight;
			}
		}, 500);
		
		
		// keepalive session
		setInterval(function(){
			y.ajax(scope.data('keepalive'))
		}, 1000* 60 * 5)
	}
});
