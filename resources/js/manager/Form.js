define('manager.Form')
.use('manager.Post')
.as(function(y, Post) {
	
	/**
	 * Form element
	 */
	var _scope;


	/**
	 * Make scope available
	 */
	this.start = function(scope) 
	{
		_scope = scope;
		var self = this;
		_scope.submit(function(e){
			e.preventDefault();
			if(_scope.data('submit')) {
				self.submit();
			}
		});
	}
	
	
	/**
	 * Submit the form by getting its values, serializing them
	 * creating a background form and submitting that
	 */
	this.submit = function()
	{
		Post.make(_scope.attr('action')).submit(this.values());
	}
	
	
	/**
	 * Submit the form with ajax, with three handlers
	 */
	this.ajax = function(done, fail, always)
	{
		Post.make( _scope.attr('action')).ajax(this.values(), done, fail, always);
	}
	
	
	/**
	 * Get the value for each element
	 */
	this.values = function()
	{
		var values = {};
		var elements = _scope.data('elements');
		elements = y.isObject(elements) ? elements : {};
		for(var key in elements) {
			var element = _scope.fetch(elements[key]);
			if(element.length > 0) {
				values[key] = element.invoke('value');
			}
		}
		return values;
	}
	
	
	/**
	 * Get a registered element by key 
	 * @param string key
	 * @returns object
	 */
	this.element = function(key)
	{
		var elements = _scope.data('elements');
		elements = y.isObject(elements) ? elements : {};
		if(y.isSet(elements[key])) {
			return _scope.fetch(elements[key]);
		}
	}
});