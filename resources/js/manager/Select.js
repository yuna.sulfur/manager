define('manager.Select')
.use('manager.Callback')
.as(function(y, Callback) {
	this.start = function(scope) 
	{
		Callback.invoke(scope.data('callback'), scope.data('data'));
	}
});
