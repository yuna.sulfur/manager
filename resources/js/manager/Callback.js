define('manager.Callback')
.set({
	_instance: null,
	instance: function() 
	{
		if(this._instance === null) {
			this._instance = this.make()
		}
		return this._instance;
	},
	
	register: function(fn)
	{
		return this.instance().register(fn);
	},
	
	invoke: function(id, data)
	{
		return this.instance().invoke(id, data);
	},
})
.as(function(y, self){
	
	var _callbacks;
	
	this.start = function(globalName)
	{
		globalName = globalName || '__callbacks__';
		var outer = y.outer();
		outer[globalName] = outer[globalName] || {};
		_callbacks = outer[globalName];
	};
	
	this.register = function(fn) {
		var id = '__' + new Date().getTime() + Math.round(Math.random() * 100000);
		_callbacks[id] = fn;
		return id;
	};
	
	this.invoke = function(id, data)
	{
		if(y.isSet(_callbacks[id])){
			return _callbacks[id](data);
		}
		return null;
	};
});