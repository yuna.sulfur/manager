define('manager.Loading')
.use('yellow.View')
.use('manager.Overlay')
.template('loading' , '\
<div y-name="__loading__" class="loading" style="width:100%; height: 100%">\
</div>')

.set({
	_instance: null,
	instance: function() 
	{
		if(this._instance === null) {
			this._instance = this.make()
		}
		return this._instance;
	},
	
	show: function(){
		this.instance().show();
	},
	
	hide: function(){
		this.instance().hide();
	},
})

.as(function(y, View, Overlay, template)
{
	// overlay instance
	var _overlay;
	
	this.start = function()
	{
		// check if there is a loading element in the outer window
		var loading = y.outer().window.fetch('__loading__');
		
		if(loading.length > 0) {
			// if there is one, the overlay is in the element data
			_overlay = loading.data('overlay');
		} else {
			// if there isnt one, create loader element
			var loading = View.make(template('loading')).element();
		
			// create overlay with loader element, but do this in the outer overlay
			_overlay = y.outer().get('manager.Overlay').make(loading, {
				close: false,
				width: 50, 
				height: 50,
				show: false
			});
			// set the overlay in the element data
			loading.data('overlay', _overlay);
		}
	}
	
	this.show = function() {
		_overlay.show(200);
	}
	
	this.hide = function() {
		_overlay.hide(200);
	}
});