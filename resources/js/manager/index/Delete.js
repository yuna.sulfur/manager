define('manager.index.Delete')
.use('manager.Loading')
.use('manager.Dialog')
.as(function(y, Loading, Dialog) {
	this.start = function(scope) 
	{
		scope.click(function(e){
			e.preventDefault();
			e.stopPropagation();
			var confirm = Dialog.confirm(scope.data('title'), scope.data('message'), function(){
				confirm.remove();
				Loading.show();
				y.ajax(scope.data('href'), {
					dataType: 'json',
					type : 'POST',
					data: {
						csrf: scope.data('csrf')
					}
				})
				.always(function(){
					Loading.hide();
				})
				.done(function(data){
					if(data.success) {
						// remove the item
						scope.fetch('item', 'closest').remove();
					} else {
						Dialog.alert(data.message);
					}
				});
			});
		});
	}
});