define('manager.index.batch.Delete')
.use('manager.Loading')
.use('manager.Dialog')
.as(function(y, Loading, Dialog) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
	}
	
	
	/**
	 * public delete
	 */
	this.delete = function(items) {
		var confirm = Dialog.confirm(_scope.data('title'), _scope.data('message'), function(){
			confirm.remove();
			Loading.show();
			var queue = items.length;
			for(var i = 0; i < items.length; i++){
				y.ajax(_scope.data('url').replace('{{id}}', items[i].data('id')), {
					dataType: 'json',
					type : 'POST',
					data: {
						csrf: _scope.data('csrf')
					}
				})
				.always(function(){
					queue--;
					if(queue === 0) {
						Loading.hide();
					}
				})
				.done((function(item) {
					return function(data){
						if(data.success) {
							item.remove();
						}
					}
				})(items[i]));
			}
		});
	}
});