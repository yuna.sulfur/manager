define('manager.index.Select')
.use('manager.Callback')
.as(function(y, Callback) {
	this.start = function(scope) 
	{
		scope.click(function(e){
			e.preventDefault();
			e.stopPropagation();
			Callback.invoke(scope.data('callback'), scope.fetch('item', 'closest').data('item'));
		});
	}
});