define('manager.index.Batch')
.as(function(y, Loading, Dialog) {

	var _scope;
	
	var _current;
	
	this.start = function(scope) 
	{
		_scope = scope;
		
		// make selector work
		scope.fetch('select-all').change(function(){
			if(y(this).is(':checked')) {
				scope.fetch('index', 'closest').fetch('select').prop('checked',true);
			} else {
				scope.fetch('index', 'closest').fetch('select').prop('checked', false);
			}
		});
		
		
		scope.fetch('select').change(function(){
			_current = y(this).find(':selected');
			if(_current.data('apply')) {
				scope.fetch('apply').show();
			} else {
				scope.fetch('apply').hide();
			}
		});
		
		
		scope.fetch('apply').click(function(){
			var selected = [];
			_scope.fetch('index', 'closest').fetch('item').each(function(){
				var item = y(this);
				if(item.fetch('select').is(':checked')) {
					selected.push(item);
				}
			});
		
			var invoke = _current.data('invoke');
			if(invoke) {
				_current.invoke(invoke, selected);
			}
		});
	}
});