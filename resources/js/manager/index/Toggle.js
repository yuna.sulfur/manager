define('manager.index.Toggle')
.as(function(y) {
	this.start = function(scope) 
	{
		scope.fetch('state').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			scope.fetch('state').show();
			y(this).hide();
			
			var url = scope.data('url').replace('{{status}}', y(this).data('status'));
			// TODO: csrf
			y.ajax(url, {
				dataType: 'json',
				type : 'POST',
				data: {
					csrf: scope.data('csrf')
				}
			});
			return false;
		});
		
		scope.fetch(scope.data('value')).show();
	}
});