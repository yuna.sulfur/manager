define('manager.index.Crop')
.use('manager.Callback')
.use('manager.Dialog')
.as(function(y, Callback, Dialog) {
	this.start = function(scope) 
	{
		scope.click(function(e){
			var dialog;
			var callback = Callback.register(function(data) {
				dialog.remove();
				var img = scope.fetch('item', 'closest').fetch('image');
				var src = img.attr('src');
				var glue = '?';
				if(src.indexOf('?') > -1) {
					glue = '&';
				}
				img.attr('src', src + glue + new Date().getTime());
			});
			dialog = Dialog.iframe(scope.data('url').replace('{{callback}}', callback));
		});
	}
});