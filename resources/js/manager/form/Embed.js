define('manager.form.Embed')
.use('manager.Callback')
.as(function(y, Callback) {
	
	this.start = function(scope) 
	{
		scope.fetch('submit').click(function(){
			Callback.invoke(scope.data('callback'), scope.fetch('element-image').data('value'));
		});
	}
});