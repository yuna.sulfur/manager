define('manager.form.Link')
.use('manager.Callback')
.as(function(y, Callback) {
	
	
	this.start = function(scope) 
	{
		// pick up changes in the selected url, to use in title
		scope.fetch('url').change(function(){
			var changed = y(this);
			if(changed.data('title') && scope.fetch('element-title').invoke('value') == '' ) {
				scope.fetch('element-title').invoke('value', changed.data('title'));
			}
		});
		
		scope.fetch('submit').click(function(){
			var data = {
				url: scope.fetch('element-url').invoke('value'),
				title: scope.fetch('element-title').invoke('value'),
				blank: scope.fetch('element-blank').invoke('value'),
				nofollow: scope.fetch('element-nofollow').invoke('value'),
			}
			Callback.invoke(scope.data('callback'), data);
		});
	}
});