define('manager.form.Menu')
.use('manager.Callback')
.as(function(y, Callback) {

	this.start = function(scope) 
	{
		// update Title field when a new link was chosen.
		scope.fetch('link').change(function(){
			var changed = y(this);
			if(changed.data('title') && scope.fetch('element-title').invoke('value') == '' ) {
				scope.fetch('element-title').invoke('value', changed.data('title'));
			}
		});
		
		
		scope.fetch('element-type').change(function(){
			var val = y(this).invoke('value');
			if(val == 'default') {
				scope.fetch('element-html').fetch('group', 'closest').hide();
				scope.fetch('element-link').fetch('group', 'closest').show();
				scope.fetch('element-title').fetch('group', 'closest').show();
				
			} else if(val == 'html') {
				scope.fetch('element-html').fetch('group', 'closest').show();
				scope.fetch('element-link').fetch('group', 'closest').hide();
				scope.fetch('element-title').fetch('group', 'closest').hide();
			} else {
				scope.fetch('element-html').fetch('group', 'closest').hide();
				scope.fetch('element-link').fetch('group', 'closest').hide();
				scope.fetch('element-title').fetch('group', 'closest').hide();
			}
		}).change();
	}
});