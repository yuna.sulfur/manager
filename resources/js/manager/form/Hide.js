define('manager.form.Hide')
.use('manager.Callback')
.as(function(y, Callback) {
	
	var _scope ;
	var _hide = [];
	var _show = [];
	
	this.start = function(scope) 
	{
		_scope = scope;
		var hide = scope.data('hide');
		var form = scope.closest('form');
		var selector = form.invoke('element', scope.data('selector'));
		
		for(var value in hide) {
			for(var i = 0; i < hide[value].length; i++) {
				var element = form.invoke('element', hide[value][i]).fetch('group', 'closest');
				if(!y.isSet(_hide[value])) {
					_hide[value] = [];
				}
				_hide[value].push(element);
				_show.push(element);
			}
		}

		var current = null;
		setInterval(function(){
			var val = selector.invoke('value');
			if(val !== current) {
				change(val);
			}
			current = val;
		}, 100);
	}
	
	var change = function(value)
	{
		for(var i = 0; i < _show.length; i++) {
			_show[i].show();
		}
		
		if(y.isSet(_hide[value]) && y.isArray(_hide[value])) {
			for(var i = 0; i < _hide[value].length; i++) {
				_hide[value][i].hide();
			}
		}
	}
});