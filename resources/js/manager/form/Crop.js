define('manager.form.Crop')
.use('manager.Callback')
.as(function(y, Callback) {
	
	this.start = function(scope) 
	{
		var cropper = new Cropper(scope.fetch('image')[0], {
			viewMode: 1,
			//background: false
		});
		
		// https://github.com/fengyuanchen/cropperjs/blob/master/README.md
		
		scope.fetch('ratio').click(function(){
			var ratio = y(this).data('ratio');
			//if(ratio == 0){
				//cropper.clear();
			//} else {
				cropper.setAspectRatio(y(this).data('ratio'));
			//}
		});
		
		scope.fetch('submit').click(function(){
			y.ajax(scope.data('url'), {
				method: 'POST',
				data: cropper.getData(true),
				dataType: 'JSON'
			}).done(function(data) {
				if(data.success) {
					Callback.invoke(scope.data('callback'));
				}
			});
		});
	}
});