define('manager.form.element.Relation')
.use('yellow.View')
.use('manager.Dialog')
.use('manager.Callback')
.as(function(y, View, Dialog, Callback) {
	
	var _scope;
	
	var _value;
	
	var _multiple;
	
	var _max;
	
	var _order;
	
	var _current = null;
	
	this.start = function(scope) 
	{
		
		_scope = scope;
		
		_value = scope.data('value');
		
		_multiple = scope.data('multiple');
		
		_max = _multiple ? scope.data('max') : 0;
		
		_order = scope.data('order') == 'desc' ? 'desc' : 'asc';
		
		_scope.fetch('add').click(function(e){
			var dialog = Dialog.iframe(scope.data('url_select').replace('{{callback}}', Callback.register(function(data) {
				dialog.remove();
				update(data);
				refresh();
			})))
		});
		
		if(_order === 'desc') {
			_scope.fetch('container').before(_scope.fetch('add'));
		}
		
		if(_multiple) {
			var relatives = y.isArray(_value) ? _value : [];
		} else {
			var relatives = y.isObject(_value) ? [ _value ] : [];
		}
		
		if(_order === 'desc') {
			relatives.reverse();
		}
		
		for(var i = 0; i < relatives.length; i++ ) {
			update(relatives[i]);
		}
		
		refresh();
	}
	
	
	this.value = function()
	{
		return _value;
	}
	
	
	/**
	 * Incoming data from the dialog
	 * @param {type} data
	 * @returns {undefined}
	 */
	var update = function(data)
	{
		// make sure junction is set
		data.junction = data.junction || {};
		
		// create a relation element
		var relative = View.make(_scope.template('relative')).element(data);
		
		// set the data
		relative.data('data', data);

		// update values when changing junction vals
		relative.fetch('junction').change(refresh);

		// update button
		if(_scope.data('update')) {
			relative.fetch('update').click(function(e){
				var self = y(this);
				var relative =  self.fetch('relative', 'closest');
				var id = relative.data('id');
				var dialog = Dialog.iframe(_scope.data('url_update').replace('{{id}}', id).replace('{{callback}}', Callback.register(function(data) {
					dialog.remove();
					_current = relative;
					update(data);
					refresh();
				})))
			});
		} else {
			relative.fetch('update').hide();
		}
		
		// delete button
		relative.fetch('delete').click(function(e){
			y(this).fetch('relative', 'closest').remove();
			refresh();
		});
		
		// append element, or replace existing, when editing
		if(_current) {
			// insert the relation after the original relation
			_current.after(relative);
			// move the original junction part to the newly inserted relation
			// and remove the new (empty) junction part
			var relativeJunction = relative.fetch('junction');
			relativeJunction.after(_current.fetch('junction'));
			relativeJunction.remove();
			// remove the now obsolete original relation
			_current.remove();
			_current = null;
		} else {
			if(_order === 'desc') {
				_scope.fetch('container').prepend(relative);
			} else {
				_scope.fetch('container').append(relative);
			}
		}
	}
	
	
	var refresh = function()
	{
		// get the current relations
		var relatives = _scope.fetch('relative');
		
		
		// show / hide add button
		if( (_multiple && relatives.length >= _max) || (! _multiple && relatives.length >= 1) ) {
			_scope.fetch('add').hide();
		} else {
			_scope.fetch('add').show();
		}
		
		// get the data in full and for the serverside
		var full = [];
		var value = [];
		relatives.each(function(){
			var relative = y(this);
			var data = relative.data('data');
			data.junction = {};
			relative.fetch('junction').each(function(){
				var junction = y(this);
				data.junction[junction.data('name')] = junction.val();
			});
			full.push(data);
			
			var item = data.junction;
			item.id = data.id;
			value.push(item);
		});
			
		if (! _multiple) {
			if(full.length > 0) {
				// single relation: only use the first
				full = full[0];
				value = value[0];
			} else {
				full = null
				value = 0
			}
		} 
		
		// set entire dataset for other purposes
		_scope.data('value', full);
		
		// save value as the internal value
		_value = value;
		
	
		// make sortable
		if(_multiple && _max > 1) {
			_scope.fetch('relative').addClass('movable');
			_scope.fetch('container').sortable({
				items: '[y-name^=relative]', 
				containment: _scope,
				tolerance: 'pointer',
				placeholder: 'placeholder',
				stop: refresh,
			});
		}
	}
});