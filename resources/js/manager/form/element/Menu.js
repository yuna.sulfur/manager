define('manager.form.element.Menu')
.as(function(y) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		var value = scope.data('value');
		_scope.find('option').each(function() {
			var element = y(this);
			if( element.val() == value) {
				element.attr('selected', 'selected');
			}
		})
	}
	
	this.value = function()
	{
		return _scope.find('option:selected').val();
	}
});