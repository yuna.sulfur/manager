define('manager.form.element.Checkbox')
.use('yellow.Arr')
.as(function(y, Arr) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		_scope.find('input[type=checkbox]').each(function() {
			var element = y(this);
			if(Arr.has(element.val(), scope.data('value'))) {
				element.attr('checked', 'checked');
			}
		})
	}
	
	this.value = function()
	{
		var value = [];
		_scope.find('input[type=checkbox]:checked').each(function() {
			value.push(y(this).val());
		})
		return value;
	}
});