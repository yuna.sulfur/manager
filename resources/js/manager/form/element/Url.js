define('manager.form.element.Url')
.use('manager.Dialog')
.use('manager.Callback')
.as(function(y, Dialog, Callback) {
	
	var _scope;
	
	
	this.start = function(scope) 
	{
		_scope = scope;
		
		scope.find('input[type=text]').val(scope.data('value'));
		
		scope.fetch('preset').click(function(){
			var clicked = y(this);
			// a preset was clicked, build url from that
			y.ajax(scope.data('url_construct'), {
				method: 'POST',
				dataType: 'json',
				data: {
					route: y(this).data('route'),
					info: JSON.stringify(clicked.data('info'))
				}
			}).done(function(data){
				// set the correct url
				scope.find('input[type=text]').val(data.url);
				// emit a change, so the form can pick up the title
				scope
				.data('title', clicked.data('title'))
				.change();
			});
		})
		
		
		scope.fetch('select').click(function(){
			// a module select was clicked, collect item data and build url from that
			var clicked = y(this);
			var dialog;
			var callback = Callback.register(function(data) {
				dialog.remove();
				y.ajax(scope.data('url_construct'), {
					method: 'POST',
					dataType: 'json',
					data: {
						module: clicked.data('module'),
						info: JSON.stringify({params: data})
					}
				}).done(function(urlData){
					// set the correct url,
					scope.find('input[type=text]').val(urlData.url);
					
					// emit a change, so the form can pick up the title
					scope
					.data('title', data.title || '')
					.change();
				});
			});
			dialog = Dialog.iframe(clicked.data('url').replace('{{callback}}', callback));
		})
	}
	
	this.value = function(value)
	{
		return _scope.find('input[type=text]').val();
	}
});