define('manager.form.element.Select')
.use('yellow.Arr')
.as(function(y, Arr) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		var multiple = _scope.data('multiple');
		var value = scope.data('value');
		if(multiple && ! y.isArray(value)) {
			value = [];
		}
		
		_scope.find('option').each(function() {
			var element = y(this);
			if(multiple && Arr.has(element.val(), value)) {
				element.attr('selected', 'selected');
			} else if(! multiple && element.val() == value) {
				element.attr('selected', 'selected');
				// Safari
				element.prop('selected', true);
			}
		})
	}
	
	this.value = function()
	{
		var multiple = _scope.data('multiple');
		if(multiple) {
			var value = [];
		} else {
			var value = null;
		}
		
		_scope.find('option:selected').each(function() {
			if(multiple) {
				value.push(y(this).val());
			} else {
				value = y(this).val();
			}
		})
		return value;
	}
});