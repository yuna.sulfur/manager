define('manager.form.element.Submit')
.as(function(y) {
	this.start = function(scope) 
	{
		scope.click(function(){
			scope.fetch('form', 'closest').invoke('submit');
		});
	}
});