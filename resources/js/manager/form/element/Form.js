define('manager.form.element.Form')
.use('yellow.Arr')
.use('yellow.View')
.as(function(y, Arr, View) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		var value = scope.data('value');
		value = y.isObject(value) ? value : {};
		
		// testing 
		/*
		value = {
			elements:[
				[
					{
						name: 'firstname',
						label: 'Voornaam',
						type: 'text',
					},
					{
						name: 'lastname',
						label: 'Achternaam',
						type: 'text',
					},
				],
				[
					{
						name: 'gender',
						label: 'Gestlacht',
						type: 'radio',
						options: ['man', 'vrouw'],
						required: true,
						error: 'kies iets'
					},
					{
						name: 'info',
						label: 'Extra informatie',
						type: 'textarea',
					},
				]
			],
			hooks: [
				{
					type: 'email',
					from: 'info@yuna.nl',
					sender: 'Yuna website',
					to: '[email]',
					message: 'this is the message [firstname]'
				}
			]
		};
		*/
		// add elements based on given value
		var elements = value.elements || [];
		for(var i = 0; i < elements.length; i++) {
			var row = addRow();
			for(var j = 0; j < elements[i].length; j++) {
				addElement(row, elements[i][j]);
			}
		}
		
		// add elements button
		scope.fetch('add').click(function(e) {
			e.stopPropagation();
			var element = addElement(addRow());
			active(element);
		});

		// add hooks based on given value
		var hooks = value.hooks || [];
		for(var i = 0; i < hooks.length; i++) {
			addHook(hooks[i]);
		}
		
		// add hook button
		scope.fetch('add-hook').click(function(e) {
			addHook({type: y(this).data('type')});
		});

		// make sure the hooks have all the available input helpers
		updateHooks();

		// deactivate all elements on doc click
		y(document).click(function(e){
			active(null);
		}).click();
	}
	
	
	var addRow = function()
	{
		var row = View.make(_scope.template('row')).element();
		_scope.fetch('grid').append(row);
		return row;
	}


	var addElement = function(row, val){
		
		var element = View.make(_scope.template('element')).element();
		row.append(element);
		
		// delete btn
		element.fetch('delete').click(function(e){
			e.stopPropagation();
			element.remove();
			draggable();
		})
		
		// activate
		element.click(function(e){
			e.stopPropagation();
			active(element);
		})
		
		// dont active on click
		element.fetch('name').click(function(e){
			e.stopPropagation()
		})
		
		// make sure names are unique
		element.fetch('name').change(function(e){
			var name = cleanName(y(this).val());
			// set cleanname
			y(this).val(name);
			// after that, do a unique name swoop, skipping this name
			y(this).val(uniqueName(name, true));
			// fire a change
			change(element);
		})
		
		// set a name
		element.fetch('name').val((y.isObject(val) && val.name) || createName());
			
		// create a settings element
		var settings = View.make(_scope.template('settings')).element();
		element.append(settings);
		
		// set the values in the settings element
		if(y.isObject(val)) {
			settings.fetch('label').val(val.label || '');
			settings.fetch('info').val(val.info || '');
			settings.fetch('type').find('option[value="' + (val.type || 'text') + '"]').prop('selected', true);
	
			var options = y.isArray(val.options) ? val.options : [];
			for(var i = 0; i < options.length; i++) {
				addOption(settings.fetch('options'), options[i]);
			}
			var required = (val.required === 1 || val.required === '1' || val.required === 'true' || val.required === true)
			settings.fetch('required').prop('checked', required);
			settings.fetch('error').val(val.error || '');
		}
		
		// adding options
		settings.fetch('add-option').click(function(){
			addOption(settings.fetch('options'), '');
		});
		
		// change handlers on the settings
		settings.fetch('label').keyup(function(){
			change(element);
		});
		settings.fetch('info').keyup(function(){
			change(element);
		});
		settings.fetch('type').change(function(){
			change(element);
		});
		settings.fetch('options').change(function(){
			change(element);
		});
		settings.fetch('required').change(function(){
			change(element);
		});
		settings.fetch('error').keyup(function(){
			change(element);
		});

		// fire a change
		change(element);
		
		// make draggable again
		draggable();

		return element;
	}
	
	
	var addOption = function(options, val)
	{
		var option = View.make(_scope.template('option')).element();
		options.append(option);
		
		if(val) {
			option.fetch('option').val(val);
		}
		
		option.fetch('delete-option').click(function(){
			option.remove();
			options.change();
		});
		option.fetch('option').keyup(function(){
			options.change();
		})
		options.sortable({
			stop: function(event, ui) {
				options.change();
			},
		});
		options.change();
	}
	

	var elementValue = function(element)
	{
		var settings = element.fetch('settings');
		var options = [];
		settings.fetch('option').each(function(){
			options.push(y(this).val());
		})
		return {
			name: element.fetch('name').val(),
			label: settings.fetch('label').val(),
			info: settings.fetch('info').val(),
			type: settings.fetch('type').val(),
			options: options,
			required: settings.fetch('required').is(':checked'),
			error: settings.fetch('error').val(),
		}
	}


	var createName = function()
	{
		var name = _scope.data('name') + '-' + (_scope.fetch('element').length);
		return uniqueName(cleanName(name));
	}
	

	var cleanName = function(name)
	{
		return name.replace(/\s/g, '-').replace(/[^a-zA-Z0-9\-\_]/g, '').replace(/^\-+|\-$/g,'').toLowerCase();
	}
	
	
	var uniqueName = function(name, skipFirst)
	{
		if(name == '') {
			return createName();
		} else {
			// get names from elements
			var names = [];
			var skipped = false
			_scope.fetch('element').each(function(){
				if(skipFirst && y(this).fetch('name').val() == name && ! skipped ) {
					skipped = true;
				} else {
					names.push(y(this).fetch('name').val());
				}
			});
			
			// non existing: we are good to go
			if(! Arr.has(name, names)) {
				return name;
			}
			
			// exiting: create a new name
			var match = name;
			var result = match;
			var parts = name.split('-');
			var counter = 1;
			
			if(parts.length > 1) {
				var last = parts.pop();
				if(Number(last) == last) {
					counter = last;
					match = parts.join('-');
					result = match + '-' + counter;
				}
			}
			while(Arr.has(result, names)) {
				counter++;
				result = match + '-' + counter;
			}
			return result;
		}
	}
	
	
	var change = function(element)
	{
		// show hide elements depending on settings
		var val = elementValue(element);
		if(val.type == 'radio' || val.type == 'checkbox') {
			element.fetch('settingsOptions').show();
		} else {
			element.fetch('settingsOptions').hide();
		}
		
		if(val.required) {
			element.fetch('error').show();
		} else {
			element.fetch('error').hide();
		}
		
		// create a new preview
		var preview = View.make(_scope.template('preview')).element(val);
		element.fetch('preview').empty().append(preview);
		
		// update the hooks
		updateHooks();
	}
	
	
	var active = function(element)
	{
		// deactiveate all cols
		_scope.fetch('element').removeClass('form-col-active');
		
		// hide all elements
		_scope.fetch('element').fetch('settings').hide();
		
		// show all previews
		_scope.fetch('element').fetch('preview').show();
		
		if(element) {
			// activate col
			element.addClass('form-col-active');
			// show element
			element.fetch('settings').show();
			element.fetch('preview').hide();
		}
	}
	

	var draggable = function() 
	{
		// remove empty rows
		_scope.fetch('row').each(function(){
			if(y(this).fetch('element').length == 0) {
				y(this).remove();
			}
		})
		
		// insert dropzone rows between rows
		_scope.fetch('row').each(function(){
			var row = View.make(_scope.template('row')).element();
			row.insertAfter(y(this));
		})
		
		// insert one at the top
		var row = View.make(_scope.template('row')).element();
		row.insertBefore(_scope.fetch('row').first());
		
		// make sortable
		_scope.fetch('row').sortable({
			connectWith: '.form-row',
			//tolerance: 'intersect',
			placeholder: 'form-placeholder col',
			//containment: '.form-grid',
			start: function( event, ui) {
				active(null);
				var row = ui.item.fetch('row','closest');
				// remove surrounding emprty rows of dragging col when its the only one in a row
				if(row.fetch('element').length <= 1) {
					row.fetch('row', 'next').remove();
					row.fetch('row', 'prev').remove();
					_scope.fetch('row').sortable('refresh')
				}
			},
			stop: function( event, ui) {
				// reset draggable
				draggable();
			},
		});
	}
	
	
	var addHook = function(val)
	{
		val = y.isObject(val) ? val : {};
		
		if(val.type) {
			// create hook
			var hook = View.make(_scope.template('hook-' + val.type)).element();
			_scope.fetch('hooks').append(hook);

			// set values
			if(val.subject) {
				hook.fetch('subject').val(val.subject);
			}
			if(val.from) {
				hook.fetch('from').val(val.from);
			}
			if(val.sender) {
				hook.fetch('sender').val(val.sender);
			}
			if(val.to) {
				hook.fetch('to').val(val.to);
			}
			if(val.message) {
				hook.fetch('message').val(val.message);
			}
			if(val.url) {
				hook.fetch('url').val(val.url);
			}
			if(val.javascript) {
				hook.fetch('javascript').val(val.javascript);
			}
			if(val.preset) {
				hook.fetch('preset').val(val.preset);
			}

			// delete
			hook.fetch('delete-hook').click(function(){
				y(this).fetch('hook', 'closest').remove();
			});

			// update to fill available names
			updateHooks();
		}
	}
	
	
	var updateHooks = function()
	{
		// get available names and emails
		var names = [];
		var emails = [];
		_scope.fetch('element').each(function(){
			var name = y(this).fetch('name').val();
			var type = y(this).fetch('type').val();
			names.push(name);
			if(type === 'email') {
				emails.push(name);
			}
		});
		
		var shortcodes = _scope.data('shortcodes') || [];
		for(var i = 0; i < shortcodes.length; i++) {
			names.push(shortcodes[i]);
		}

		// set available names and emails
		_scope.fetch('hook').each(function(){
			var fields = y(this).fetch('fields');
			fields.empty();
			for(var i = 0; i < names.length; i++) {
				var field = View.make(_scope.template('field')).element({ text: '[' +  names[i] + ']'});
				field.click((function(fld) { return function(e){
					e.preventDefault();
					e.stopPropagation();
					
					var textarea = fields.fetch('hook', 'closest').fetch('message');
					var start = textarea[0].selectionStart;
					var end = textarea[0].selectionEnd;
					var message = textarea.val();
					textarea.val(message.slice(0, start) + fld.text() + message.slice(end));
					textarea.focus();
					textarea[0].selectionStart = start + fld.text().length;
					textarea[0].selectionEnd = start + fld.text().length;
					
				}})(field));
				fields.append(field);
			}
			
			var tos = y(this).fetch('tos');
			tos.empty();
			for(var i = 0; i < emails.length; i++) {
				var to = View.make(_scope.template('field')).element({ text: '[' +  emails[i] + ']'});
				to.click((function(t) { return function(){
					tos.fetch('hook', 'closest').fetch('to').val(t.text());
				}})(to));
				tos.append(to);
			}
		});
	}
	
	
	var hookValue = function(hook)
	{
		var type = hook.data('type');
		if(type == 'email') {
			return {
				type: 'email',
				subject: hook.fetch('subject').val(),
				from: hook.fetch('from').val(),
				sender: hook.fetch('sender').val(),
				to: hook.fetch('to').val(),
				message: hook.fetch('message').val(),
			}
		} else if(type == 'webhook') {
			return {
				type: 'webhook',
				url: hook.fetch('url').val(),
			}
		} else if(type == 'javascript') {
			return {
				type: 'javascript',
				javascript: hook.fetch('javascript').val(),
			}
		} else if(type == 'preset') {
			return {
				type: 'preset',
				preset: hook.fetch('preset').val(),
			}
		}
	}
	

	var value = function()
	{
		var val = {
			elements: [],
			hooks: [],
		};
		
		_scope.fetch('row').each(function(){
			var elements = y(this).fetch('element');
			if(elements.length > 0) {
				var row = [];
				val.elements.push(row);
				elements.each(function(){	
					row.push(elementValue(y(this)));
				})
			}
		});
		
		_scope.fetch('hook').each(function(){
			val.hooks.push(hookValue(y(this)))	
		});
		
		return val;
	}
	
	
	this.value = function() {
		return value();
	}
});


