define('manager.form.element.Hidden')
.as(function(y) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		_scope.find('input').val(scope.data('value'));
	}
	
	this.value = function()
	{
		return _scope.find('input').val();
	}
});