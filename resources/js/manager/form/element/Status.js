define('manager.form.element.Status')
.as(function(y) {
	
	var _scope;
	var _value;
	
	this.start = function(scope) 
	{
		_scope = scope;
		_value = scope.data('value');
		var status = scope.fetch('status');
		
		
		if(_value === 'live') {
			scope.fetch('live').show();
			scope.fetch('edit').hide();
			status.addClass('text-success');
		} else {
			scope.fetch('live').hide();
			scope.fetch('edit').show();
			status.removeClass('text-success');
		}
		
		
		scope.fetch('option').click(function(){
			var option = y(this);
			_value = option.data('value');
			status.text(option.fetch('label').text());
			if(_value === 'live') {
				scope.fetch('live').show();
				scope.fetch('edit').hide();
				status.addClass('text-success');
			} else {
				scope.fetch('live').hide();
				scope.fetch('edit').show();
				status.removeClass('text-success');
			}
		});
	}
	
	
	this.value = function()
	{
		return _value;
	}

});