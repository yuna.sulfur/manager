define('manager.form.element.Upload')
.use('yellow.View')
.use('manager.Message')
.as(function(y, View, Message, self)
{
	var _scope;

	this.start = function(scope)
	{
		_scope = scope;
		
		var id = 'id_' + new Date().getTime();
		var el = View.make(scope.template('zone')).element({id: id});
		scope.append(el);
		var dropzone = new Dropzone('div#' + id , {
			url: scope.data('url'),
			success: function(file, response) {
				file.previewElement.remove();
				response = JSON.parse(response);
				if(response.success && y.isSet(response.items[0])) {
					y('body').fetch('index').invoke('add', response.items, true);
				}
				if(! response.success && response.errors.length > 0) {
					Message.make(response.errors[0], 'error');
				}
			}
		});
	}
});