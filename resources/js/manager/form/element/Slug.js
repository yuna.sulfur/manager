define('manager.form.element.Slug')
.as(function(y)
{
	var _scope;

	this.start = function(scope)
	{
		_scope = scope;
		
		_scope.val(_scope.data('value'));
		
		var source = _scope.fetch('form', 'closest').fetch('element-' + scope.data('source'));
		var mode = _scope.data('value') == '' ? 'create' : 'update';
		var sync = false;

		source.focus(function(e){
			var value = _scope.val();
			// get alias
			var slug = prepare_uri(source.invoke('value'));
			// if the alias is the same as the title, or the alias is blank, it's ok to sync
			if( slug == value || value == ''){
				sync = true;
			} else {
				sync = false;
			}
		});
		

		source.change(function(e){
			// if we are creating and it's ok to sync
			if(mode == 'create' && sync){
				// set the value
				_scope.val(prepare_uri(source.invoke('value')))
			}
		});
		
		_scope.change(function(e){
			// when manually changing, show correct url
			_scope.val(prepare_uri(_scope.val()))

		});
		
	}
	

	this.value = function()
	{
		return _scope.val();
	}
	
	
	var prepare_uri = function(uri)
	{
		uri = uri.trim();
		uri = uri.replace(/ /g,'-');
		uri = iconv(uri);
		uri = uri.replace(/[^A-Za-z0-9-]/g, '');
		uri = uri.replace(/-+/g, '-');
		uri = uri.replace(/^-+|-+$/g, '');
		uri = uri.toLowerCase();
		return uri;
	}
	
	
	var iconv = function(s){
		var in_chrs = 'àáâãäçèéêëìíîïñòóôõöøùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖØÙÚÛÜÝß';
		var out_chrs = 'aaaaaceeeeiiiinoooooouuuuyyAAAAACEEEEIIIINOOOOOOUUUUYs';
		var chars_rgx = new RegExp('[' + in_chrs + ']', 'g');
		var transl = {};
		
		var lookup = function (m) {
			if(m == 'ß'){
				return 'ss';
			} else {
				return transl[m] || m; 
			}
		};
		
		for (var i = 0; i < in_chrs.length; i++) {
			transl[in_chrs[i]] = out_chrs[i];
		}
		return s.replace(chars_rgx, lookup);
	}
});