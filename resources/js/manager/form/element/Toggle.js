define('manager.form.element.Toggle')
.as(function(y) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		if (scope.data('value') == 1) {
			_scope.find('input[type=checkbox]').attr('checked', 'checked');
		}
	}
	
	this.value = function()
	{
		if(_scope.find('input[type=checkbox]:checked').length > 0) {
			return 1;
		} else {
			return 0;
		}
	}
});