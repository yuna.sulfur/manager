define('manager.form.element.Blocks')
.use('yellow.View')
.as(function(y, View) {
	
	var _scope;
	var _value;
	var _max;
	var _autoselect;
	var _prototypes = {};

	this.start = function(scope) 
	{
		_scope = scope;
		
		_value = scope.data('value');
		if(! y.isArray(_value)) {
			_value = [];
		}
		
		_max = scope.data('max');
		_autoselect = scope.data('autoselect');
		
		// fetch prototypes
		scope.fetch('prototype').each(function(){
			_prototypes[y(this).data('type')] = y(this)
		});
		
		// add existing
		for(var i = 0; i < _value.length; i++ ) {
			add(_value[i].type, _value[i]);
		}
		
		// new
		scope.fetch('block-add').click(function(e){
			var block = add(y(this).data('type'), {});
			refresh();
			setTimeout(function(){
				// open editor
				edit(block);
				
				// automagically open the first relation selector
				if(_autoselect) {
					block.fetch('element').each(function(){
						var relation = y(this);
						var create = relation.fetch('create');
						if(create.length > 0) {
							create.click();
							return false
						}
						var create = relation.fetch('add');
						if(create.length > 0) {
							create.click();
							return false
						}
					});
				}
			}, 100);
		});


		// Click on document: check if the click was part of the active block.
		// If not: close all blocks by editing an empty block
		y(document).click(function(e){
			var keep = (
				// dont close on clicks inside the active editor
				y(e.target).fetch('block','closest').hasClass('editing')
				// dont close on all kinds of buttons outside of the editor
				|| y(e.target).hasClass('mce-text') 
				|| y(e.target).hasClass('mce-ico')
				|| y(e.target).hasClass('mce-active')
				|| y(e.target).hasClass('dialog-close')
				|| y(e.target).hasClass('overlay')
			);
			if(! keep) {
				edit(y('<div></div>'))
			}
		});
		
		refresh();
	}
	
	
	var add = function(type, data, before, after)
	{
		if(proto = _prototypes[type]) {
			
			// create entire block
			var block =  View.make(_scope.template('block')).element({
				name: proto.data('name'),
				type: type
			});
			
			if(before) {
				block.insertBefore(before);
			} else if(after) {
				block.insertAfter(after);
			} else {
				_scope.fetch('blocks').append(block);
			}

			
			// make sortable
			_scope.fetch('blocks').sortable({
				stop: function(evt, ui){
					// add a new one
					add(ui.item.data('type'), value(ui.item), ui.item);
					// remove this one
					ui.item.remove();
					refresh();
				}
			});

			// delete button
			block.fetch('block-delete').click(function(e){
				e.stopPropagation();
				block.remove();
				refresh();
			});

			// update button
			block.fetch('block-update').click(function(e){
				e.stopPropagation();
				edit(block)
			});
		
			// up button
			block.fetch('block-up').click(function(e){
				// add a new one
				add(type, value(block), block.prev());
				// remove this one
				block.remove();
				refresh();
			});


			// down button
			block.fetch('block-down').click(function(e){
				e.stopPropagation();
				// add a new one
				add(type, value(block), null, block.next());
				// remove this one
				block.remove();
				refresh();
			});


			// create the editor
			var editor = proto.make();
			block.append(editor);
			
			// set values
			editor.fetch('element').each(function(){
				var element = y(this);
				var key = element.data('key');
				if(y.isSet(data[key])) {
					element.data('value', data[key])
				}
			});
			editor.show();
			editor.start();
			editor.attr('y-name', 'editor');
			render(block);
		
			block.click(function(e){
				if(! block.hasClass('editing')) {
					e.stopPropagation();
					edit(block);
				}
			});
			
			return block;
		}
	}
	

	var edit = function(block)
	{
		// render all blocks if editor is visible
		_scope.fetch('block').each(function(){
			if(y(this).fetch('editor').is(':visible')) {
				render(y(this));
			}
		});
		
		// remove this render
		block.fetch('render').remove();
		
		// activate
		block.addClass('editing');
		
		// show this editor
		block.fetch('editor').show();
	}
	
	
	var render = function(block)
	{
		// remove old render
		block.fetch('render').remove();
		
		// deactivate
		block.removeClass('editing');
		
		// hide editor
		block.fetch('editor').hide();
		
		// create render
		var _render = View.make(block.template('render')).element(value(block));
		_render.attr('y-name', 'render');
		_render.addClass('clickable');
		block.append(_render);
	}
	
	
	var value = function(block)
	{
		// get values
		var data = {
			type: block.data('type')
		};
		
		block.fetch('element').each(function(){
			var element = y(this);
			var key = element.data('key');
			data[key] = element.invoke('value');
		});
					
		// add full relation data
		block.fetch('relation').each(function(){
			var relation = y(this);
			var key = relation.data('key');
			data[key] = relation.data('value');
		});
		
		return data;
	}
	

	this.value = function()
	{
		var values = [];
		_scope.fetch('block').each(function(){
			var data = value(y(this));
			data.type = y(this).data('type');
			values.push(data);
		});
		return values;
	}



	var refresh = function()
	{
		// get the current relations
		var blocks = _scope.fetch('block');
		
		// show / hide add button
		if(blocks.length >= _max) {
			_scope.fetch('blocks-add').hide();
		} else {
			_scope.fetch('blocks-add').show();
		}
		
		blocks.each(function(){
			var block = y(this);
			var first = block.is(':first-child');
			var last = block.is(':last-child');
			if(first && last) {
				block.fetch('up').hide();
				block.fetch('down').hide();
			} else if(first) {
				block.fetch('up').hide();
				block.fetch('down').show();
			} else if(last) {
				block.fetch('up').show();
				block.fetch('down').hide();
			} else {
				block.fetch('up').show();
				block.fetch('down').show();
			}
		});
	}
});