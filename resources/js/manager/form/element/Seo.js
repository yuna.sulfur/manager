define('manager.form.element.Seo')
.use('yellow.View')
.as(function(y, View)
{
	var _scope;
	
	var _title;
	var _description;
	var _keyword;
	var _value;
	
	var _website;
	var _base;
	var _locale;
	var _lang;
	var _readability;
	var _findability;
	
	var _sourceTitle;
	var _sourceSlug;
	var _sourceBody;
	
	var _snippet;
	var _bar;
	var _content;
	var _seo;
	
	var _viewSnippet;
	var _viewBar;
	var _viewResult;

	this.start = function(scope)
	{
		_scope = scope;
		
		// cache internal elements
		_title = scope.fetch('title');
		_description = scope.fetch('description');
		_keyword = scope.fetch('keyword');

		// set form values
		_value = scope.data('value') || {};
		
		scope.fetch('title').val(_value.title || '');
		scope.fetch('description').val(_value.description || '');
		scope.fetch('keyword').val(_value.keyword || '');
		
		// cache data
		_website = scope.data('website');
		_base = scope.data('base');
		_locale = scope.data('locale') || 'en_US';
		_lang = scope.data('lang') || null;
		_readability = scope.data('readability') ;
		_findability = scope.data('findability') ;
		
		// cache  source elements
		_sourceTitle = scope.closest('form').fetch('element-' + scope.data('source_title'));
		_sourceSlug = scope.closest('form').fetch('element-' + scope.data('source_slug'));

		// cache body source
		_sourceBody = [];
		var sourceBody = scope.data('source_body');
		for(var i = 0; i < sourceBody.length; i++) {
			_sourceBody.push(scope.closest('form').fetch('element-' + sourceBody[i]));
		}


		// cache containers
		_snippet = scope.fetch('snippet');
		_bar = scope.fetch('bar');
		_content = scope.fetch('result-content');
		_seo = scope.fetch('result-seo');
		
		// cache views
		_viewSnippet = View.make(scope.template('snippet'));
		_viewBar = View.make(scope.template('bar'));
		_viewResult = View.make(scope.template('result'));
		
		
		// open the advanced options
		scope.fetch('more').click(function(){
			//load the big script and then do the rest
			var parent = document.getElementsByTagName('body')[0];
			var script = document.createElement('script');
			script.src = scope.data('script');
			script.setAttribute('async', false);
			
			// add callback
			if(script.addEventListener){
				script.onload = start;
			} else if(script.readyState) {
				script.onreadystatechange = function(){
					if(script.readyState == 'loaded' || script.readyState == 'complete'){
						start();
					}
				};
			}
			// add script to the page to start loading
			document.getElementsByTagName('body')[0].appendChild(script);
		});


		// render preview
		var snippet = _viewSnippet.element({
			title: _title.val().replace(/\[title\]/g, _sourceTitle.val()).replace(/\[website\]/g, _website),
			base: _base,
			slug: _sourceSlug.val(),
			description: _description.val()
		});
		var snippetTitle = snippet.find('.yf-google-title');
		_snippet.empty().append(snippet);
		
		// render overall
		_bar.empty().append(_viewBar.element({
			percent: _value.score ? 10 * _value.score :  1
		}));
		
	}
	
	var start = function()
	{
		_scope.fetch('advanced').show();
		_scope.fetch('more').hide();
		var interval = setInterval(update, 500);
	}
	
	
	var update = function()
	{
		var snippet = _viewSnippet.element({
			title: _title.val().replace(/\[title\]/g, _sourceTitle.val()).replace(/\[website\]/g, _website),
			base: _base,
			slug: _sourceSlug.val(),
			description: _description.val()
		});
		var snippetTitle = snippet.find('.yf-google-title');
		_snippet.empty().append(snippet);
		
		var body = '';
		for(var i = 0; i < _sourceBody.length; i++) {
			var render = _sourceBody[i].fetch('render');
			if(render.length > 0 ) {
				var val = '';
				render.each(function(){
					val += y(this).html();
				})
			} else if(_sourceBody[i].fetch('image').length > 0) {
				var val = '<img src="source" alt="' + _keyword.val() + '" title="title" />';
			} else {
				var val = _sourceBody[i].invoke('value');
			}
			
			if(! y.isString(val)) {
				val = JSON.stringify(val);
			}
			body += '<p>' + val + '</p>';
		};
		
	
		// Do assessment
		var assessor = yoast.assessor(_lang && _lang != 'en'  ? lang[_lang] : null);
		var result = assessor.assess(body, {
			keyword: _keyword.val(),
			synonyms: "",
			description: _description.val(),
			title: snippetTitle.text(),
			titleWidth: snippetTitle.width(),
			url: _sourceSlug.val(),
			permalink: _base + _sourceSlug.val(),
			locale: _locale
		});

		// Content result
		result.content.sort(function(a, b){
			return a.score > b.score ? 1 : -1;
		});
		
		var sum = 0;
		var total = result.content.length;
		for(var i = 0; i < total; i++) {
			sum += result.content[i].score;
		}
		var avgContent = total > 0 ? Math.round(sum / total) : 1;
		_content.empty().append(_viewResult.element({
			label: _readability,
			score: avgContent,
			results: result.content
		}));
		
		
		// Seo result
		result.seo.sort(function(a, b){
			return a.score > b.score ? 1 : -1;
		});
		
		var sum = 0;
		var total = result.seo.length;
		for(var i = 0; i < total; i++) {
			sum += result.seo[i].score;
		}
		var avgSeo = total > 0 ? Math.round(sum / total) : 1;
		_seo.empty().append(_viewResult.element({
			label: _findability,
			score: avgSeo,
			results: result.seo
		}));
		
		// Overall bar
		_bar.empty().append(_viewBar.element({
			percent: Math.max(1, 10 * ((avgContent + avgSeo) / 2))
		}));
		
		// value
		_value = {
			title: _title.val(),
			description: _description.val(),
			keyword: _keyword.val(),
			score: (avgContent + avgSeo) / 2
		};
	}
	
	
	this.value = function() {
		return _value;
	}


	var lang = {
		nl: {
			"":{},
			"You have far too little content, please add some content to enable a good analysis.":[
				"Je hebt veel te weinig content, voeg wat content toe om een goede analyse te kunnen maken"
			],

			"%1$s of the sentences contain %2$spassive voice%3$s, which is less than or equal to the recommended maximum of %4$s.":[
				"%1$s van de zinnen bevat %2$spassieve toon%3$s, wat minder of gelijk is aan het aanbevolen maximum van %4$s."
			],
			"%1$s of the sentences contain %2$spassive voice%3$s, which is more than the recommended maximum of %4$s. Try to use their active counterparts.":[
				"%1$s van de zinnen bevat %2$spassieve toon%3$s, wat meer is dan het aanbevolen maximum van %4$s., Probeer actieve toon te gebruiken."
			],
			"The text contains %2$d consecutive sentences starting with the same word. Try to mix things up!":[
				"De tekst bevat %2$d opeenvolgende zinnen die beginnen met hetzelfde woord. Probeer af te wisselen.",
				"De tekst bevat %1$d gevallen waar %2$d of meer opeenvolgende zinnen beginnen met hetzelfde woord. Probeer af te wisselen."
			],
			"%1$s of the sentences contain a %2$stransition word%3$s or phrase, which is less than the recommended minimum of %4$s.":[
				"%1$s van de zinnen bevat een %2$sovergangswoord%3$s of zin, wat minder is dan het aanbevolen minimum van %4$s."
			],
			"%1$s of the sentences contain a %2$stransition word%3$s or phrase, which is great.":[
				"%1$s van de zinnen bevat een %2$sovergangswoord%3$s of zin. Dit is prima!"
			],
			"%1$s of the words contain %2$sover %3$s syllables%4$s, which is less than or equal to the recommended maximum of %5$s.":[
				"%1$s van de woorden bevat %2$s meer dan %3$s lettergrepen, wat minder of gelijk is dan het aanbevolen maximum van %5$s."
			],
			"%1$s of the words contain %2$sover %3$s syllables%4$s, which is more than the recommended maximum of %5$s.":[
				"%1$s van de woorden bevat %2$s meer dan %3$s lettergrepen, wat meer is dan het aanbevolen maximum van %5$s."
			],


			"The copy scores %1$s in the %2$s test, which is considered %3$s to read. %4$s":[
				"De tekst scoort %1$s in de %2$s test, wat beschouwd wordt als %3$s te lezen. %4$s"
			],
			"very easy":[
				"zeer gemakkelijk"
			],
			"easy":[
				"gemakkelijk"
			],
			"fairly easy":[
				"betrekkelijk gemakkelijk"
			],
			"ok":[
				"goed"
			],
			"fairly difficult":[
				"betrekkelijk moeilijk"
			],
			"difficult":[
				"moeilijk"
			],
			"very difficult":[
				"zeer moeilijk"
			],


			"Try to make shorter sentences to improve readability.":[
				"probeer kortere zinnen te maken om de leesbaarheid te vergroten."
			],
			"Try to make shorter sentences, using less difficult words to improve readability.":[
				"probeer kortere zinnen te maken met eenvoudiger woorden om de leesbaarheid te vergroten."
			],
			"None of the paragraphs are too long, which is great.":[
				"Geen van de paragrafen is te lang, wat goed is."
			],
			"%1$d of the paragraphs contains more than the recommended maximum of %2$d words. Are you sure all information is about the same topic, and therefore belongs in one single paragraph?":[
				"%1$d van de de paragrafen bevat meer de het aanbevolen maximum van %2$d woorden. Weet je zeker dat alle informatie over hetzelfde onderwerp gaat en in een paragraaf hoort?",
				"%1$d van de de paragrafen bevat meer de het aanbevolen maximum van %2$d woorden. Weet je zeker dat alle informatie over hetzelfde onderwerp gaat en in een paragraaf hoort?",
			],
			"%1$s of the sentences contain %2$smore than %3$s words%4$s, which is less than or equal to the recommended maximum of %5$s.": [
				"%1$s van de zinnen bevat %2$s meer dan %3$s woorden%4$s, wat minder of gelijk is aan het aanbevolen maximum van %5$s."
			],
			"%1$s of the sentences contain %2$smore than %3$s words%4$s, which is more than the recommended maximum of %5$s. Try to shorten the sentences.": [
				"%1$s van de zinnen bevat %2$s meer dan %3$s woorden%4$s, wat meer is dan het aanbevolen maximum van %5$s. Probeer korteren zinnen te maken."
			],


			"Great job with using %1$ssubheadings%2$s!":[
				"Het gebruik van %1$ssubkoppen%2$s is prima."
			],
			"%1$d section of your text is longer than %2$d words and is not separated by any subheadings. Add %3$ssubheadings%4$s to improve readability.":[
				"%1$d sectie van de tekst is langer dan %2$d woorden en wordt niet gescheiden door subkoppen. Voeg %3$ssubkoppen%4$s toe om leesbaarheid te vergroten",
				"%1$d secties van de tekst zijn langer dan %2$d woorden en worden niet gescheiden door subkoppen. Voeg %3$ssubkoppen%4$s toe om leesbaarheid te vergroten",
			],
			"You are not using any subheadings, although your text is rather long. Try and add  some %1$ssubheadings%2$s.":[
				"Je gebruikt geen subkoppen, maar de tekst is tamelijk lang. Probeer %1$sssubkoppen%2$s toe te voegen"
			],
			"You are not using any %1$ssubheadings%2$s, but your text is short enough and probably doesn't need them.":[
				"Je gebruikt geen subkoppen, maar dat de tekst zo kort dat deze niet nodig zijn"
			],


			"No %1$simages%2$s appear in this page, consider adding some as appropriate.":[
				"Er komen geen %1$safbeeldingen%2$s voor op deze pagina, overweeg deze toe te voegen."
			],
			"The %1$simages%2$s on this page contain alt attributes with the focus keyword.":[
				"De %1$safbeeldingen%2$s op deze pagina bevatten een alt-attribuut met het trefwoord."
			],
			"The %1$simages%2$s on this page do not have alt attributes containing the focus keyword.":[
				"De %1$safbeeldingen%2$s op deze pagina bevatten een geen alt-attribuut met het trefwoord."
			],
			"The %1$simages%2$s on this page contain alt attributes.":[
				"De %1$safbeeldingen%2$s op deze pagina bevatten een alt-attribuut."
			],
			"The %1$simages%2$s on this page are missing alt attributes.":[
				"De %1$safbeeldingen%2$s op deze pagina bevatten geen alt-attribuut."
			],

			"No %1$sinternal links%2$s appear in this page, consider adding some as appropriate.":[
				"Er komen geen %1$sinterne links%2$s voor op deze pagina, overweeg deze toe te voegen."
			],
			"This page has %1$s %2$sinternal link(s)%3$s, all nofollowed.":[
				"Deze pagina heeft %1$s %2$sinterne link(s)%3$s, allemaal nofollow"
			],
			"This page has %1$s %2$sinternal link(s)%3$s.":[
				"Deze pagina heeft %1$s %2$sinterne link(s)%3$s"
			],
			"This page has %1$s nofollowed %2$sinternal link(s)%3$s and %4$s normal internal link(s).":[
				"Deze pagina heeft %1$s nofollow %2$sinterne link(s)%3$s en %4$s normale interne link(s)."
			],

			"No %1$soutbound links%2$s appear in this page, consider adding some as appropriate.":[
				"Er komen geen %1$sexterne links%2$s voor op deze pagina, overweeg deze toe te voegen."
			],
			"This page has %1$s %2$soutbound link(s)%3$s, all nofollowed.":[
				"Deze pagina heeft %1$s %2$sexterne link(s)%3$s, allemaal nofollow"
			],
			"This page has %1$s %2$soutbound link(s)%3$s.":[
				"Deze pagina heeft %1$s %2$sexterne link(s)%3$s"
			],
			"This page has %1$s nofollowed %2$soutbound link(s)%3$s and %4$s normal outbound link(s).":[
				"Deze pagina heeft %1$s nofollow %2$sexterne link(s)%3$s en %4$s normale externe link(s)."
			],


			"The %1$sSEO title%2$s is too short. Use the space to add keyword variations or create compelling call-to-action copy.":[
				"De %1$sSEO titel%2$s is te kort. Gebruik de ruimte voor trefwoord variaties of voor een aantrekkelijke call-to-action."
			],
			"The %1$sSEO title%2$s has a nice length.":[
				"De %1$sSEO titel%2$s heeft een goede lengte."
			],
			"The %1$sSEO title%2$s is wider than the viewable limit.":[
				"De %1$sSEO titel%2$s is langer dan de beschikbare ruimte."
			],
			"Please create an %1$sSEO title%2$s.":[
				"Voeg een %1$sSEO titel%2$s toe."
			],


			"The slug for this page is a bit long, consider shortening it.":[
				"De url voor deze pagina is een beetje lang, overweeg om deze korter te maken."
			],
			"The slug for this page contains a %1$sstop word%2$s, consider removing it.":[
				"De url voor deze pagina bevat een %1$sstopwoord%2$s, overweeg om deze te verwijderen.",
				"De url voor deze pagina bevat %1$sstopwoorden%2$s, overweeg om deze te verwijderen."
			],


			"A meta description has been specified, but it %1$sdoes not contain the focus keyword%2$s.":[
				"De meta-omschrijving is gegeven, maar het bevat niet het trefwoord"
			],
			"No %1$smeta description%2$s has been specified. Search engines will display copy from the page instead.":[
				"Er is geen %1$smeta-omschrijving%2$s gegeven. Zoekmachines zullen conten uit de pagina weergeven."
			],
			"The %1$smeta description%2$s is under %3$d characters long. However, up to %4$d characters are available.":[
				"De %1$smeta-omschrijving%2$s is minder dan %3$d karakters lang, maar er zijn %4$d karakters beschikbaar."
			],
			"The %1$smeta description%2$s is over %3$d characters. Reducing the length will ensure the entire description will be visible.":[
				"De %1$smeta-omschrijving%2$s is langer dan %3$d karakters. Maak de omschrijving korter om zeker te weten dat deze helemaal zichtbaar is."
			],
			"The %1$smeta description%2$s has a nice length.":[
				"De %1$smeta-omschrijving%2$s heeft een goede lengte."
			],
			"The meta description %1$scontains the focus keyword%2$s.":[
				"De meta beschrijving %1$sbevat het trefwoord%2$s.",
			],
			"The meta description contains no sentences %1$sover %2$s words%3$s.":[
				"De meta-omschrijving bevat geen zinnen met meer dan %2$s woorden."
			],
			"The meta description contains %1$d sentence %2$sover %3$s words%4$s. Try to shorten this sentence.":[
				"De meta-omschrijving bevat %1$d zin met meer dan %3$s woorden. Probeer deze zin korter te maken.",
				"De meta-omschrijving bevat %1$d zinnen met meer dan %3$s woorden. Probeer deze zinnen korter te maken.",
			],




			"No %1$sfocus keyword%2$s was set for this page. If you do not set a focus keyword, no score can be calculated.":[
				"Er is geen %1$strefwoord%2$s ingevuld. Als er geen trefwoord aanwezig is, kan er geen score bepaald worden."
			],
			"The focus keyword contains a stop word. This may or may not be wise depending on the circumstances. %1$sLearn more about the stop words%2$s.":[
				"Het trefwoord bevat een stopwoord. Dit kan wel of niet verstandig zijn. %1$sLees meer over stopwoorden%2$s.",
				"Het trefwoord bevat meerdere stopwoorden. Dit kan wel of niet verstandig zijn. %1$sLees meer over stopwoorden%2$s."
			],



			"The focus keyword doesn't appear in the %1$sfirst paragraph%2$s of the copy. Make sure the topic is clear immediately.":[
				"Het trefwoord is niet aanwezig in de %1$seerste paragraaf%2$s van de tekst. Zorg ervoor dat het onderwerp direct duidelijk is."
			],
			"The focus keyword appears in the %1$sfirst paragraph%2$s of the copy.":[
				"Het trefwoord komt voor in de %1$seerste paragraaf%2$s van de tekst."
			],
			"The focus keyword does not appear in the %1$sURL%2$s for this page. If you decide to rename the URL be sure to check the old URL 301 redirects to the new one!":[
				"Het trefwoord komt niet voor in de %1$sURL%2$s van deze pagina. "
			],
			"The focus keyword appears in the %1$sURL%2$s for this page.":[
				"Het trefwoord komt voor in de %1$sURL%2$s van deze pagina. "
			],
			"The focus keyword '%1$s' does not appear in the %2$sSEO title%3$s.":[
				"Het trefwoord '%1$s' komt niet in de %2$sSEO title%3$s voor."
			],
			"The focus keyword appears in %1$d (out of %2$d) %3$ssubheadings%4$s in your copy.":[
				"Het trefwoord komt voor in %1$s van %2$d %3$ssubkoppen%4$s in de tekst."
			],
			"You have not used the focus keyword in any %1$ssubheading%2$s (such as an H2) in your copy.":[
				"Het trefwoord komt niet voor in %1$ssubkoppen%1$s in de tekst."
			],
			"The %1$sSEO title%2$s contains the focus keyword, at the beginning which is considered to improve rankings.":[
				"De %1$sSEO titel%2$s bevat het trefwoord aan het begint, wat goed werkt voor een betere ranking."
			],
			"The %1$sSEO title%2$s contains the focus keyword, but it does not appear at the beginning; try and move it to the beginning.":[
				"De %1$sSEO titel%2$s bevat het trefwoord, maar niet aan het begin. Probeer de titel met het trefwoord te beginnen."
			],
			"Use your keyword or synonyms more often in your text so that we can check %1$skeyword distribution%2$s.":[
				"Gebruik het trefwoord vaker in de tekst zodat de %1$strefwoord verdeling%2$s gecheckt kan worden"
			],
			"Large parts of your text do not contain the keyword. Try to %1$sdistribute%2$s the keyword more evenly.":[
				"Grote delen van de tekst bevatten het trefwoord niet. Probeer het trefwoord beter te %1$sverdelen%2$s.",
			],
			"Some parts of your text do not contain the keyword. Try to %1$sdistribute%2$s the keyword more evenly.":[
				"Sommige delen van de tekst bevatten het trefwoord niet. Probeer het trefwoord beter te %1$sverdelen%2$s.",
				"Sommige delen van de tekst bevatten het trefwoord niet. Probeer het trefwoord beter te %1$sverdelen%2$s."
			],
			"Your keyword is %1$sdistributed%2$s evenly throughout the text. That's great.":[
				"Het trefwoord is goed over de tekst %1$sverdeeld%2$s.",
				"Het trefwoord is goed over de tekst %1$sverdeeld%2$s.",
			],

			"The %1$skeyphrase%2$s is over 10 words, a keyphrase should be shorter.":[
				"The %1$skernzin%2$s is meer dan 10 woorden, en kernzin hoort korter te zijn.",
			],




			"The exact-match %3$skeyword density%4$s is %1$s, which is too low; the focus keyword was found %2$d time.":[
				"De %3$strefwoord dichtheid%4$s is %1$s, wat te laag is. Het trefwoord komt %2$d keer voor.",
				"De %3$strefwoord dichtheid%4$s is %1$s, wat te laag is. Het trefwoord komt %2$d keer voor."
			],
			"The exact-match %3$skeyword density%4$s is %1$s, which is great; the focus keyword was found %2$d time.":[
				"De %3$strefwoord dichtheid%4$s is %1$s, wat goed is. Het trefwoord komt %2$d keer voor.",
				"De %3$strefwoord dichtheid%4$s is %1$s, wat goed is. Het trefwoord komt %2$d keer voor."
			],
			"The exact-match %4$skeyword density%5$s is %1$s, which is over the advised %3$s maximum; the focus keyword was found %2$d time.":[
				"De %4$strefwoord dichtheid%5$s is %1$s, wat hoger is dan het aanbevolen maximum van %3$s. Het trefwoord komt %2$d keer voor.",
				"De %4$strefwoord dichtheid%5$s is %1$s, wat hoger is dan het aanbevolen maximum van %3$s. Het trefwoord komt %2$d keer voor.",
			],
			"The exact-match %4$skeyword density%5$s is %1$s, which is way over the advised %3$s maximum; the focus keyword was found %2$d time.":[
				"De %4$strefwoord dichtheid%5$s is %1$s, wat veel hoger is dan het aanbevolen maximum van %3$s. Het trefwoord komt %2$d keer voor.",
				"De %4$strefwoord dichtheid%5$s is %1$s, wat veel hoger is dan het aanbevolen maximum van %3$s. Het trefwoord komt %2$d keer voor.",
			],




			"The text contains %1$d word.":[
				"De tekst bevat %1$d woord",
				"De tekst bevat %1$d woorden"
			],
			"This is more than or equal to the %2$srecommended minimum%3$s of %4$d word.":[
				"Dit is meer of gelijk aan het %2$saanbevolen minimum%3$s van %4$d woord.",
				"Dit is meer of gelijk aan het %2$saanbevolen minimum%3$s van %4$d woorden."
			],
			"This is slightly below the %2$srecommended minimum%3$s of %4$d word. Add a bit more copy.":[
				"Dit is iets minder dan het %2$saanbevolen minimum%3$s van %4$d woord. Voeg iets meer tekst toe.",
				"Dit is iets minder dan het %2$saanbevolen minimum%3$s van %4$d woorden. Voeg iets meer tekst toe."
			],
			"This is below the %2$srecommended minimum%3$s of %4$d word. Add more content that is relevant for the topic.":[
				"Dit is minder dan het %2$saanbevolen minimum%3$s van %4$d woord. Voeg meer relevante content toe.",
				"Dit is minder dat het %2$saanbevolen minimum%3$s van %4$d woorden. Voeg meer relevante content toe."
			],
			"This is far below the %2$srecommended minimum%3$s of %4$d word. Add more content that is relevant for the topic.":[
				"Dit is lager dan het %2$saanbevolen minimum%3$s van %4$d woord. Voeg meer relevante content toe.",
				"Dit is lager dan het %2$saanbevolen minimum%3$s van %4$d woorden. Voeg meer relevante content toe."
			],
		}
	}

});