define('manager.form.element.Link')
.use('yellow.View')
.use('manager.Dialog')
.use('manager.Callback')
.as(function(y, View, Dialog, Callback) {
	
	var _scope;
	var _data;
	
	this.start = function(scope) 
	{
		_scope = scope;
		
		_data = scope.data('value');
		if(! y.isObject(_data)) {
			_data = {};
		}
	
		scope.fetch('create').click(function(){
			dialog(null);
		});
		

		if(_data && y.isSet(_data.url)) {
			update(_data);
		} else {
			refresh();
		}
	}
	
	this.value = function()
	{
		return _data;
	}
	
	var dialog = function(value)
	{
		var dialog;
		var callback = Callback.register(function(data) {
			dialog.remove();
			update(data);
		});
		dialog = Dialog.iframe(_scope.data('url').replace('{{callback}}', callback), value);
	}
	
	
	var update = function(data)
	{
		// save the data 
		_data = data;
		
		// render the data, by making a copy
		var json = JSON.stringify(data);
		var data = JSON.parse(json);
		
		// add the json itself
		data.value = json.replace(/\"/g, '\"');
		
		var link = View.make(_scope.template('link')).element(data);
		
		_scope.fetch('container')
		.empty()
		.append(link);

		link.fetch('update').click(function(){
			dialog(_data);
		});
		
		link.fetch('delete').click(function(){
			remove();
		});
		
		// broadcast change, so a form can pick it up
		_scope.data('title', data.title || '')
		.change();

		refresh();
	}
	
	
	var remove = function()
	{
		_scope.fetch('container').empty();
		_data = null;
		refresh();
	}
	
	
	
	var refresh = function()
	{
		if(_data && y.isSet(_data.url)) {
			_scope.fetch('create').hide();
		} else {
			_scope.fetch('create').show();
		}
	}
});