define('manager.form.element.Radio')
.as(function(y) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		_scope.find('input[type=radio]').each(function() {
			var element = y(this);
			if(element.val() == scope.data('value')) {
				element.attr('checked', 'checked');
			}
		})
	}
	
	this.value = function()
	{
		return _scope.find('input[type=radio]:checked').val(); 
	}
});