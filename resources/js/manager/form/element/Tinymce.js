define('manager.form.element.Tinymce')
.use('yellow.View')
.use('manager.Callback')
.use('manager.Dialog')
.template('link',
'<a href="{{ url }}" data-mce-href="{{ url }}"{% if blank== 1%} target="_blank"{% endif %}{% if nofollow == 1%} rel="nofollow"{% endif %} title="{{ title }}">{{{ body }}}</a>'
)
.template('image',
'<img align="{{ align }}" src="{{ src }}" alt="{{ title }}"/>'
).set({
	started: false,
})

.as(function(y, View, Callback, Dialog, Template, self)
{
	var _scope;
	
	/**
	 * init
	 */
	this.start = function(scope)
	{
		if(! self.started) {
			// add custom link plugin
			tinymce.PluginManager.add('link', function(editor) {
				editor.addButton('link', {
					text: false,
					icon: 'link',
					stateSelector: 'a[href]',
					onclick: function(){
						link(editor)
					}
				});
			});
			
			tinymce.PluginManager.add('unlink', function(editor) {
				editor.addButton('unlink', {
					text: false,
					icon: 'unlink',
					stateSelector: 'a[href]',
					onclick: function(){
						unlink(editor)
					}
				});
			});
		
			self.started = true;
		}
		
		
		_scope = scope;
		
		_scope.fetch('textarea').val(scope.data('value'));
		
		
		// get config
		var config = scope.data('config');
		
		// wait until loaded
		config.init_instance_callback = function(editor){
			// capture click on image button
			scope.find('.mce-i-image').closest('.mce-widget').click(function(e){
				var node = editor.selection.getNode();
				if(node.nodeName != 'IMG' && node.nodeName != 'FIGURE' && node.nodeName != 'FIGCAPTION') {
					image(editor);
					// dont call original image
					e.stopPropagation();
				}
			});
		}
		
		//var id = '__' + new Date().getTime() + Math.round(1000000 * Math.random());
		
		//scope.fetch('textarea').attr('id', id)
		
		config.target = scope.fetch('textarea')[0];
		
		// init tiny
		tinymce.init(config);
	};
	
	
	this.value = function()
	{
		// manually trigger tiny save
		tinymce.triggerSave();
		return _scope.find('textarea').val();
	}
	

	var link = function(editor)
	{
		// data for the dialog
		var data = null;
		
		// get selected node
		var node = y(editor.selection.getNode()).closest('a');
		
		// fill up data with node
		if(node.length > 0){
			data = {
				url: node.attr('href'),
				title: node.text(),
				blank: node.attr('target') == '_blank' ? 1 : 0,
				nofollow:  node.attr('rel') == 'nofollow' ? 1 : 0
			}
   		} else {
			data = {
				title: editor.selection.getContent({format : 'text'})
			}
		}
		
		var dialog;
		var callback = Callback.register(function(data) {
			dialog.remove();
			
			if(node.length == 0){
				var template = _scope.template('link');
				if(! template) {
					template = Template('link');
				}
				// new node
				var content = editor.selection.getContent();
				if(content) {
					data.body = content
				} else {
					data.body = data.title
				}
				editor.selection.setContent(View.make(template).render(data));
			} else {
				// existing node
				node.attr('href',data.url);
				node.attr('data-mce-href', data.url);
				node.attr('title', data.title);
				node.text(data.title);
				if(data.blank == 1) {
					node.attr('target', '_blank');
				} else {
					node.removeAttr('target');
				}
				if(data.nofollow == 1) {
					node.attr('rel', 'nofollow');
				} else {
					node.removeAttr('rel');
				}
			}
		});
		dialog = Dialog.iframe(_scope.data('url_link').replace('{{callback}}', callback), data);
	};


	var unlink = function(editor)
	{
		// get selected text
		var selected = editor.selection.getContent({format : 'text'});
		
		// get selected a node
		var node = y(editor.selection.getNode()).closest('a');
		
		if(node.length > 0) {
			// get linked html
			var linked = node.html();


			if(selected == '') {
				// no text was selected: remove link
				node.replaceWith(linked);
			} else {
				// split on selected text
				var parts = linked.split(selected);
				if(parts.length > 2) {
					// multiple occurences of selected: too hard: just remove link
					node.replaceWith(linked);
				} else if (parts.length < 2) {
					// selected not found (impossible): just remove link
					node.replaceWith(linked);
				} else if(parts[0] == '' && parts[1] == '') {
					// linked = selection : just remove link
					node.replaceWith(linked);
				} else if(parts[0] == '') {
					// selected was at the front
					// put back the remaining linked html and before that the selected text
					node.html(parts[1]).before(selected)
				} else if(parts[1] == '') {
					// selected was at the end
					// put back the remaining linked html and after that the selected text
					node.html(parts[0]).after(selected)
				} else {
					// selected was in the middle: just remove link
					node.replaceWith(linked);
				}
			}
		}
	}


	var image = function(editor)
	{
		var dialog;
		var callback = Callback.register(function(data) {
			dialog.remove();
			var template = _scope.template('image');
			if(! template) {
				template = Template('image');
			}
			editor.selection.setContent(View.make(template).render(data));
		});
		dialog = Dialog.iframe(_scope.data('url_image').replace('{{callback}}', callback));
	};
});
