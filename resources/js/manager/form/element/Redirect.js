define('manager.form.element.Redirect')
.use('yellow.View')
.as(function(y, View) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		
		
		var value = scope.data('value');
		
		if(! y.isObject(value)) {
			value = {};
		}

		for(var i in value) {
			add(i, value[i]);
		}
		
		// add empty element
		scope.fetch('add').click(function(){
			add('','');
		}).click();
	}
	
	
	var add = function(from, to)
	{
		var element = View.make(_scope.template('redirect')).element({});
		element.fetch('from').val(from);
		element.fetch('to').val(to);
		_scope.fetch('container').append(element);
		element.fetch('delete').click(function(){
			element.remove();
		});
	}
	
	
	this.value = function()
	{
		var value = {};
		_scope.fetch('pair').each(function(){
			var from = y(this).fetch('from').val().replace(/^\/+|\/+$/g, '');
			var to = y(this).fetch('to').val().replace(/^\/+|\/+$/g, '');
			if(from || to) {
				value[from] = to;
			}
		});
		return value;
	}
});