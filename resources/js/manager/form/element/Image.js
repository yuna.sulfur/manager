define('manager.form.element.Image')
.use('yellow.View')
.use('manager.Dialog')
.use('manager.Callback')
.use('manager.Message')
.as(function(y, View, Dialog, Callback, Message, self)
{
	var _scope;
	
	var _value;
	
	var _multiple;
	
	var _max;
	
	var _dropzone;
	
	var _current = null;

	this.start = function(scope)
	{
		_scope = scope;
			
		_value = scope.data('value');

		_multiple = scope.data('multiple');
		
		_max = _multiple ? scope.data('max') : 1;
		
		_dropzone = new Dropzone(scope.fetch('zone')[0] , {
			maxFiles: _max,
			url: _scope.data('url_create'),
			success: function(file, response) {
				this.removeFile(file);
				response = JSON.parse(response);
				if(response.success && y.isSet(response.items[0])) {
					update(response.items[0].data);
				}
				if(! response.success && response.errors.length > 0) {
					Message.make(response.errors[0], 'error');
				}
				refresh();
			},
			init: function() {
				this.on('addedfile', function(file){
					if(_scope.fetch('image').length + this.getQueuedFiles().length >= _max) {
						this.removeFile(file);
					}
				});
			}
		});

		
		if(_multiple) {
			var images = y.isArray(_value) ? _value : [];
		} else {
			var images = y.isObject(_value) && _value.id && _value != '0' ? [ _value ] : [];
		}
		
	
		for(var i = 0; i < images.length; i++ ) {
			update(images[i]);
		}
		refresh();
	}
	
	
	this.value = function()
	{
		return _value;
	}
	
	
	/**
	 * Incoming data from upload
	 * @param {type} data
	 * @returns {undefined}
	 */
	var update = function(data)
	{
		// make sure junction is set
		data.junction = data.junction || {};
		
		// create a relation element
		var image = View.make(_scope.template('image')).element(data);
		
		// set the data
		image.data('data', data);

		// update values when changing junction vals
		image.fetch('junction').change(refresh);


		// Crop button
		image.fetch('crop').click(function(e){
			var dialog;
			var callback = Callback.register(function(data) {
				dialog.remove();
				var img = image.fetch('img');
				var src = img.attr('src');
				var glue = '?';
				if(src.indexOf('?') > -1) {
					glue = '&';
				}
				img.attr('src', src + glue + new Date().getTime());
			});
			dialog = Dialog.iframe(_scope.data('url_crop').replace('{{id}}', data.id).replace('{{callback}}', callback));
		});

		// Delete button
		image.fetch('delete').click(function(e){
			var dialog = Dialog.make({
				title: _scope.data('title'),
				body: _scope.data('message'),
				close: true,
				width: 600,
				height: 300,
				buttons: [
					{type: 'primary', label: _scope.data('instance'), action: function(){
						image.remove();
						refresh();
						dialog.remove(); 
					}},
					{type: 'secondary', label: _scope.data('original'), action: function(){
						image.remove();
						y.ajax(_scope.data('url_delete').replace('{{id}}', data.id), {
							type : 'POST',
							data: {
								csrf: _scope.data('csrf')
							}
						})
						refresh();
						dialog.remove()
					}},
				]
			});
			return dialog;
		});
		
		_scope.fetch('container').append(image);
	}
	
	
	var refresh = function()
	{
		// get the current images
		var images = _scope.fetch('image');
		
		// show / hide add button
		if( (_multiple && images.length >= _max) || (! _multiple && images.length >= 1) ) {
			_scope.fetch('zone').hide();
		} else {
			_scope.fetch('zone').show();
		}
		
		// get the data in full and for the serverside
		var full = [];
		var value = [];
		
		images.each(function(){
			var image = y(this);
			var data = image.data('data');
			data.junction = {};
			image.fetch('junction').each(function(){
				var junction = y(this);
				data.junction[junction.data('name')] = junction.val();
			});
			full.push(data);
			
			var item = data.junction;
			item.id = data.id;
			value.push(item);
		});
			
			
		if (! _multiple) {
			if(full.length > 0) {
				// single relation: only use the first
				full = full[0];
				value = value[0];
			} else {
				full = null
				value = 0
			}
		} 
		
		// set entire dataset for other purposes
		_scope.data('value', full);
	
		// save value as the internal value
		_value = value;

	
		// make sortable
		if(_multiple && _max > 1) {
			_scope.fetch('image').addClass('movable');
			_scope.fetch('container').sortable({
				items: '[y-name^=image]', 
				containment: _scope,
				tolerance: 'pointer',
				placeholder: 'placeholder',
				stop: refresh,
			});
		}
	}
});