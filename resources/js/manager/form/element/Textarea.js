define('manager.form.element.Textarea')
.as(function(y) {
	
	var _scope;
	
	this.start = function(scope) 
	{
		_scope = scope;
		_scope.val(scope.data('value'));
	}
	
	this.value = function(value)
	{
		if(y.isSet(value)) {
			_scope.val(value)
		}
		return _scope.val();
	}
});