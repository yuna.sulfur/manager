define('manager.form.element.Tag')
.use('yellow.View')
.as(function(y, View) {
	

	var _scope;

	this.start = function(scope)
	{
		_scope = scope;
		var value = scope.data('value');
		if(! y.isArray(value)) {
			value = [];
		}


		// get popular tags
		y.ajax(scope.data('url_popular'), {
			dataType: 'JSON'
		}).done(function(data) {
			for(var i = 0; i < data.items.length; i++) {
				var item = data.items[i].data;
				var suggestion = View.make(_scope.template('suggestion')).element(item);
				suggestion.data('item', item);
				scope.fetch('popular').append(suggestion);
				suggestion.click(function(e){
					add(y(this).data('item'));
					scope.fetch('input').val('');
				});
			}
		});


		// add initial tags
		for(var i = 0; i < value.length; i++) {
			add(value[i]);
		}
		
		
		// check val, show/hide add
		setInterval(function(){
			var input = scope.fetch('input');

			if(input.val() !== '') {
				scope.fetch('add').show();
			} else {
				scope.fetch('add').hide();
			}
		}, 200);


		// add button
		scope.fetch('add').click(function(){
			var input = scope.fetch('input');
			if(input.val() !== '') {
				create(input.val());
				input.val('');
				scope.fetch('add').hide();
			}
		});
		
		
		// keyboard events on textfield
		var timeout;
		scope.fetch('input').keyup(function(e){
			var input = y(this);
			
			// catch enter or comma
			if(e.keyCode == '13' || e.keyCode == '188') {
				// don't submit form by accident
				if(e.keyCode == '13') {
					e.preventDefault();
				}
				var val = input.val().trim().replace(/\,/g, '');
				if(val != ''){
					create(val);
					input.val('');
					scope.fetch('suggestions').hide()
				}
            }
			
			// clear existing timeout fo suggestions
			clearTimeout(timeout);
			if(input.val().length > 0){
				// create new interval
				timeout = setTimeout(function(){
					// fire an ajax call to get suggestions
					y.ajax(scope.data('url_search').replace('{{query}}',encodeURIComponent(input.val())), {
						dataType:'json'
					}).done(function(data){
						var suggestions = scope.fetch('suggestions')
						suggestions.empty();
						if(data.items.length > 0) {
							scope.fetch('suggestions').show();
							for(var i = 0; i < data.items.length; i++) {
								var item = data.items[i].data;
								var suggestion = View.make(_scope.template('suggestion')).element(item);
								suggestion.data('item', item);
								suggestions.append(suggestion);
								suggestion.click(function(e){
									add(y(this).data('item'));
									scope.fetch('input').val('');
									scope.fetch('suggestions').empty();
									scope.fetch('suggestions').hide();
								});
								y('body').click(function(){
									suggestions.hide();
								})
							}
						} else {
							scope.fetch('suggestions').hide();
						}
					});
				},500);
			} else {
				scope.fetch('suggestions').hide();
			}
		}).keyup();
	}
	
	
	this.value = function() {
		var val = [];
		_scope.fetch('tag').each(function(){
			val.push(y(this).data('id'))
		});
		return val;
	}
	
	
	/**
	 * Create a tag
	 */	
	var create = function(title)
	{
		var tag;
		if(tag = add({
			id: 0,
			title: title,
		})) {
			y.ajax(_scope.data('url_create'), {
				dataType: 'json',
				method: 'POST',
				data: {
					title: title,
					csrf: _scope.data('csrf')
				}
			}).done(function(data){
				tag.data('id', data.id);
			});
		}
	}
	
	
	
	/**
	 * add a tag
	 */
	var add = function(item)
	{
		var unique = true;
		_scope.fetch('tag').each(function(){
			if(item.title == y(this).data('title')) {
				unique = false;
			}
		});
		
		if (! unique) {
			return false;
		}
		
		var tag = View.make(_scope.template('tag')).element(item);
		tag.data('id', item.id);
		tag.data('title', item.title);
	
		tag.appendTo(_scope.fetch('tags'));
		
		tag.fetch('remove').click(function(){
			tag.remove();
		});
		
		return tag;
	}
});