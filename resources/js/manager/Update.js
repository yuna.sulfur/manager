define('manager.Update')
.use('yellow.View')
.use('manager.Loading')
.use('manager.Message')
.use('manager.Dialog')
.use('manager.Post')
.use('manager.Callback')
.as(function(y, View, Loading, Message, Dialog, Post, Callback)
{
	var _scope;
	var _id;
	var _status;
	var _state;
	var _skipUnload = false;
	
	
	this.start = function(scope)
	{
		_scope = scope;
		_id = scope.data('id');
		_status = scope.data('status');
		_state = state();


		// lock
		if(scope.data('lock')) {
			if(_id) {
				y.ajax(scope.data('url_locked'), {
					dataType: 'JSON',
					type: 'POST',
					data: {
						id: _id
					}
				}).done(function(data){
					if(data[_id]) {
						// item is locked
						Dialog.confirm(scope.data('claim_title'), scope.data('claim_message').replace(/\{\{username\}\}/g, data[_id]), function(){
							claimLock(pollLock);
							restore();
						}, function(){
							leave();
						})
					} else {
						// item is not locked
						claimLock(pollLock);
						restore();
					}
				});
			}
		} else if(_id) {
			// no locks for this module
			restore();
		}


		// catch form changed outbound click
		y('a[href]').click(function(e){
			var href = y(this).attr('href');
			if(href.indexOf('#') === -1){
				if(state() !== _state){
					Dialog.confirm(scope.data('abandon_title'), scope.data('abandon_message'), function(){
						// user agreed: skip the beforeunload
						_skipUnload = true;
						// go to the clicked url
						window.location.href = href;
					});
					e.preventDefault();
				}
			}
		});
		
		
		// catch unload other than click
		window.onbeforeunload = function(e, callback) {
			if(state() !== _state && !_skipUnload){
				if(y.isFunction(callback)) {
					// A callback was supplied when triggering the beforeunload, 
					// we're in a iframe that is closing: launch a dialog and use the callback
					Dialog.confirm(scope.data('abandon_title'), scope.data('abandon_message'), function(){
						// call the callback when done
						callback();
					});
				} else {
					// Regular actual unload: launch a browser alert by returning text
					return scope.data('abandon_message').replace(/\<br \/\>/g, '\n');
				}
			} else if(y.isFunction(callback)) {
				// A callback was supplied when triggering the beforeunload, 
				// There is no alert needed, so we can proceed by calling the callback
				callback();
			}
		};
		
		
		// regular save button
		scope.fetch('btn-save').click(function(e){
			e.preventDefault();
			save(
				saveDoneSuccess, 
				saveDoneError, 
				saveFail, 
				saveAlways
			);
		});
		// route form submit through the save button
		scope.find('form').submit(function(e){
			e.preventDefault();
			e.stopPropagation();
			scope.fetch('btn-save').click();
		});
		
		// update button: first save, then go to callback url
		scope.fetch('btn-update').click(function(e){
			e.preventDefault();
			var vals = values();
			vals.id = _id;
			save(
				function(){
					_state = state();
					// do save always here, because, the Callback.invoke will remove the popup
					// so saveAlways will never be reached
					saveAlways();
					Callback.invoke(scope.data('callback'), vals)
				},
				saveDoneError,
				saveFail, 
				saveAlways
			);
		});
		

		// select button: check abandon, then go to callback url
		scope.fetch('btn-select').click(function(e){
			e.preventDefault();
			var vals = values();
			vals.id = _id;
			if(state() !== _state){
				Dialog.confirm(scope.data('abandon_title'), scope.data('abandon_message'), function(){
					// user agreed: skip the beforeunload
					_skipUnload = true;
					// go to the clicked url
					Callback.invoke(scope.data('callback'), vals)
				});
			} else {
				Callback.invoke(scope.data('callback'), vals)
			}
		});
		

		// delete button
		scope.fetch('btn-delete').click(function(e){
			e.preventDefault();
			Dialog.confirm(scope.data('delete_title'), scope.data('delete_message'), function(){
				y.ajax(scope.data('url_delete').replace('{{id}}', _id),{
					type : 'POST',
					dataType : 'json',
					data: {
						csrf: scope.data('csrf')
					}
				}).always(function(){
					leave();
				});
			});
		});
		
		// preview button
		scope.fetch('btn-preview').click(function(e){
			window.open('', 'preview');
			
			// create base64 encoded string. Escape multibyte chars.
			// https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding#Solution_2_%E2%80%93_escaping_the_string_before_encoding_it
			var contents =  btoa(encodeURIComponent(JSON.stringify(values())).replace(/%([0-9A-F]{2})/g,
				function (match, p1) {
					return String.fromCharCode('0x' + p1);
			}));
			/// create base64 encoded string, because chrome will not allow raw iframes an script to go through post
			Post.make(scope.data('url_preview')).submit({values: contents}, 'preview');
		});
		
		// history button
		history(scope.data('revisions'), scope.data('revision'));
		
		
		// status button
		var btnStatus = scope.fetch('btn-status');
		btnStatus.fetch('live').click(function(){
			_status = 'edit';
			btnStatus.fetch('live').hide();
			btnStatus.fetch('edit').show();
		});
		
		btnStatus.fetch('edit').click(function(){
			_status = 'live';
			btnStatus.fetch('live').show();
			btnStatus.fetch('edit').hide();
		});
	}
	
	
	this.id = function()
	{
		return _id;
	}
	
	
	var claimLock = function(callback)
	{
		// claim lock
		y.ajax(_scope.data('url_lock').replace('{{id}}', _id) + '?force=1', {
			type: 'POST',
			data: {
				csrf: _scope.data('csrf'),
				force: 1
			}
		}).done(function(){
			// lock claimed, do next thing
			callback();
		});
	}
	
	
	var pollLock = function()
	{
		var poll = function() {
			if(_id) {
				// try to get lock
				y.ajax(_scope.data('url_lock').replace('{{id}}', _id), {
					dataType: 'json',
					type: 'POST',
					data: { 
						csrf: _scope.data('csrf') 
					}
				}).done(function(data){
					if(! data.success) {
						// failed to get lock
						clearInterval(interval);
						// rescue the data
						rescue();
						// message
						Dialog.alert(_scope.data('claimed_message').replace(/\{\{username\}\}/g, data.username), function(){
							leave();
						})
					}
				});
			}
		}
		
		// poll once, after that at an interval
		poll();
		var interval = setInterval(function(){
			poll();
		}, 10 * 1000);
	}
	
	
	var leave = function()
	{
		// skip check
		_skipUnload = true;
		// go back
		window.location.href = _scope.data('url_back');

	}
	
	
	var pollAuthenticated = function()
	{
		
	}
	
	
	var rescue = function()
	{
		var current = state();
		if(current !== _state) {
			var current =  JSON.parse(current);
			var previous = JSON.parse(_state);
			var diff = {}
			for(var key in current) {
				if(JSON.stringify(current[key]) !== JSON.stringify(previous[key])) {
					diff[key] = current[key];
				}
			}
			var key = _scope.data('module') + ':' + _id;
			localStorage.setItem(key, JSON.stringify(diff));
		}
	}
	
	
	var unrescue = function()
	{
		var key = _scope.data('module') + ':' + _id;
		localStorage.removeItem(key);
	}
	
	
	
	var restore = function()
	{
		var key = _scope.data('module') + ':' + _id;
		var rescued = localStorage.getItem(key);
		localStorage.removeItem(key);
		try {
			if(rescued) {
				rescued = JSON.parse(rescued);
				if (y.isObject(rescued)) {
					Dialog.confirm(_scope.data('restore_title'), _scope.data('restore_message'), function(){
						Post.make(_scope.data('url_restore').replace('{{id}}', _id)).submit(rescued);
					});
				}
			}
		} catch(e) {}
	}
	
	
	var save = function(doneSuccess, doneError, fail, always)
	{
		// get the form data
		var data = values();
		
		// add csrf
		data.csrf =_scope.data('csrf');

		// remove errors
		_scope.fetch('error').hide();
		_scope.fetch('group').removeClass('form-error-group');

		// show loading
		Loading.show();
		
		// rescue data
		rescue()

		// post with ajax
		Post.make(_scope.data('url_save').replace('{{id}}', _id)).ajax(
			data,
			function(response){
				if(response.success) {
					doneSuccess(response);
				} else {
					doneError(response);
				}
			},
			fail,
			always
		);
	}
	
	
	var saveDoneSuccess = function(response)
	{
		// save succesful: work with this new dataset as startingpoint
		_state = state();

		if(_id == 0){
			// set id
			_id = response.id;
			// change the title
			_scope.fetch('header').fetch('title-create').hide();
			_scope.fetch('header').fetch('title-update').show();

			// turn on the delete button
			_scope.fetch('header').fetch('delete').show();

			// turn on the version button
			_scope.fetch('header').fetch('version').show();

			// turn on the select button
			_scope.fetch('header').fetch('select').show();
			
			// update url
			y.window[0].history.replaceState({},'', 'update/' + _id);
			
		}
		// update the revisions
		if(response.revisions) {
			history(response.revisions, response.revisions.length > 0 ? response.revisions[0].revision : 0);
		}
		
		// show the seelct button (if it's there)
		_scope.fetch('btn-select').show();
		
		// show message
		Message.make(response.message, 'success');
	}
	
	
	var saveDoneError = function(response)
	{
		// show message
		Message.make(response.message, 'error');
		// show errors
		for(var key in response.errors){
			var group = _scope.fetch('element-' + key).fetch('group', 'closest');
			var error = group.fetch('error');
			error.show();
			error.text(response.errors[key])
			group.addClass('form-group-error')
		}
	}
	

	var saveFail = function(request, status, errorThrown){
		// show error message
		Message.make(errorThrown, 'error');
	}
	
	
	var saveAlways = function(){
		// always unrescue data when done
		unrescue();
		// always hide loading screen when done
		Loading.hide();
	};
	
	
	var values = function()
	{
		var data = _scope.fetch('form').invoke('values');
		data.status = _status;
		return data;
	}
	
	var state = function()
	{
		return JSON.stringify(values());
	}
	
	/**
	 * update histoty button
	 * @param array revisions
	 * @param int revision
	 * @returns void
	 */
	var history = function(revisions, revision)
	{
		var container = _scope.fetch('revisions');
		container.empty();
		if(revisions.length > 0){
			View.make(_scope.template('revisions')).element({
				revisions:revisions,
				current: revision,
				url:_scope.data('url_revision').replace('{{id}}', _id).replace('{{revision}}', '__revision__')
			}).appendTo(container);
		}
	}
});


