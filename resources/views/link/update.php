<?php view::extend('template/item'); ?>

<?php view::start('main') ?>
	<div
		y-use="manager.form.Link"
		data-callback="<?php view::callback(); ?>"
	>
		<div class="content-header">
			<div class="content-header-1">
				<span class="btn btn-primary float-right" y-name="submit"><?php view::lang('label.submit'); ?></span>
			</div>
		</div>

		<div class="content-main">
			<?php view::file('form/form', ['form' => $form]); ?>
		</div>

	</div>
<?php view::end(); ?>