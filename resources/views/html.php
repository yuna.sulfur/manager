<!DOCTYPE html>
<html lang="nl">
	<head>
		<title>Application Manager</title>

		<!-- Prevent quirks mode: Force using highest IE mode available, force using chrome plugin for IE if installed -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<!-- Site information -->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta http-equiv="Content-Script-Type" content="text/javascript" />

		<!-- Responsive behaviour -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		<!-- Developer info -->
		<meta name="author" content="Yuna internetontwerp" />

		<!-- Load stylesheets -->

		<link href='//fonts.googleapis.com/css?family=Roboto:400,300,700,100' rel="stylesheet" type="text/css">
		<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
		<link rel="stylesheet" type="text/css" href="<?php view::base(); ?>vendor/bootstrap/css/bootstrap.css" media="screen" />
		<?php if(fetch::env('env') === 'development'): ?>
			<link rel="stylesheet" type="text/css" href="<?php view::base(); ?>css.php?build=manager" media="screen" />
		<?php else: ?>
			<link rel="stylesheet" type="text/css" href="<?php view::base(); ?>css/manager.css?v=<?php view::env('version', 1) ?>" media="screen" />
		<?php endif; ?>
		<!-- Other CSS assets -->
		<?php foreach(view::assets('css') as $asset): ?>
			<link type="text/css" href="<?php view::attr($asset) ?>" rel="stylesheet" media="screen" />
		<?php endforeach; ?>
	</head>


	<body
		class="<?php view::block('body.class', '') ?>"
		y-use="manager.Main"
		data-lang="<?php view::attr(json_encode([
			'ok' => fetch::lang('label.ok'),
			'cancel' => fetch::lang('label.cancel')
		])) ?>"
		data-keepalive="<?php view::route('keepalive') ?>"
	>

		<?php view::block('body',''); ?>

		<!-- jQuery -->
		<script type="text/javascript" src="//code.jquery.com/jquery-2.2.0.min.js"></script>

		<!-- Bootstrap -->
		<script type="text/javascript" src="<?php view::base(); ?>vendor/popper/popper.js"></script>
		<script type="text/javascript" src="<?php view::base(); ?>vendor/bootstrap/js/bootstrap.js"></script>

		<!-- Other JS assets -->
		<?php foreach(view::assets('js') as $asset): ?>
			<script src="<?php view::attr($asset) ?>"></script>
		<?php endforeach; ?>

		<!-- Yellow -->
		<script
			type="text/javascript"
			src="<?php view::base() ?>vendor/yellow/Yellow.js"
			<?php if(fetch::env('env') === 'development'): ?>
				data-main="<?php view::base() ?>js.php?rebuild=manager"
				data-src="<?php view::base() ?>js.php?build=manager&file="
				data-console="1"
			<?php else: ?>
				data-main="<?php view::base() ?>js/manager.js?v=<?php view::env('version', 1) ?>"
				data-src="<?php view::base() ?>js/"
			<?php endif; ?>
		></script>
	</body>

</html>