<div class="navigation-top">
	<div class="expanded">
		<a href="<?php view::route('start', ['zone' => $zone]) ?>" class="navigation-item">
			<div class="navigation-item-icon"><i class="icon">blur_on</i></div>
			<div class="navigation-item-label"><strong><?php view::lang('section.start'); ?></strong></div>
		</a>
	</div>
	<div class="expand" y-name="expand">
		<div class="navigation-item">
			<div class="navigation-item-icon"><i class="icon">menu</i></div>
		</div>
	</div>
</div>


<div class="navigation-items">
	<div class="navigation-item navigation-item-expandable" y-name="expandable">

		<div class="navigation-item-icon" y-name="expand"><i class="icon icon-sm">person</i></div>

		<div class="navigation-item-label"><?php view::text(fetch::identity()->screenname ?: fetch::identity()->username); ?></div>

		<div class="navigation-item-button" >
			<span y-name="more"><i class="icon icon-sm">expand_more</i></span>
			<span y-name="less" style="display:none"><i class="icon icon-sm">expand_less</i></span>
		</div>

		<div class="navigation-subitems" y-name="expanded" style="display:none">

			<div y-name="button" href="<?php view::action('preferences', 'index') ?>" class="navigation-item">
				<div class="navigation-item-icon"><i class="icon icon-sm">settings</i></div>
				<div class="navigation-item-label">
					<?php view::lang('section.settings'); ?>
				</div>
			</div>

			<div y-name="button" href="<?php view::route('logout') ?>" class="navigation-item">
				<div class="navigation-item-icon"><i class="icon icon-sm">exit_to_app</i></div>
				<div class="navigation-item-label">
					<?php view::lang('label.logout') ?>
				</div>
			</div>
		</div>
	</div>


	<?php if(isset($zones) && count($zones) > 0): ?>
		<div class="navigation-item navigation-item-expandable" y-name="expandable">
			<div class="navigation-item-icon" y-name="expand"><i class="icon icon-sm">label</i></div>

			<div class="navigation-item-label"><?php view::text($zones[$zone]['title']); ?></div>

			<?php if (count($zones) > 1 || (isset($frontend) && $frontend)): ?>
				<div class="navigation-item-button" >
					<span y-name="more"><i class="icon icon-sm">expand_more</i></span>
					<span y-name="less" style="display:none"><i class="icon icon-sm">expand_less</i></span>
				</div>
			<?php endif; ?>


			<div class="navigation-subitems" y-name="expanded" style="display:none">
				<?php if (isset($frontend) && $frontend): ?>
					<div y-name="button" href="<?php view::attr($frontend); ?>" target="frontend" class="navigation-item">
						<div class="navigation-item-icon"><i class="icon icon-sm">public</i></div>
						<div class="navigation-item-label"><?php view::lang('label.frontend') ?></div>
					</div>
				<?php endif; ?>

				<?php foreach($zones as $zn => $data): ?>
					<?php if ($zn != $zone): ?>
						<div y-name="button" href="<?php view::route('start', ['zone' => $zn]) ?>" class="navigation-item">
							<div class="navigation-item-icon"><i class="icon icon-sm">label</i></div>
							<div class="navigation-item-label">
								<?php view::text($data['title']); ?>
							</div>
						</div>
					<?php endif; ?>

				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>


	<div class="navigation-item">
		<div class="navigation-item-icon">&nbsp;</div>
	</div>


	<?php foreach($items as $item): ?>

		<?php if ($item['items']): ?>
			<div class="navigation-item navigation-item-expandable" y-name="expandable">

				<div class="navigation-item-icon" y-name="expand"><i class="icon icon-sm"><?php view::text($item['icon']) ?></i></div>

				<div class="navigation-item-label"><?php view::text($item['label']); ?></div>

				<div class="navigation-item-button" >
					<span y-name="more"><i class="icon icon-sm">expand_more</i></span>
					<span y-name="less" style="display:none"><i class="icon icon-sm">expand_less</i></span>
				</div>


				<div class="navigation-subitems" y-name="expanded" style="display:none">
					<?php foreach ($item['items'] as $subItem): ?>
						<div y-name="button" href="<?php view::attr($subItem['href']); ?>" class="navigation-item">
							<div class="navigation-item-icon"><i class="icon icon-sm"><?php view::text($subItem['icon']) ?></i></div>
							<div class="navigation-item-label"><?php view::text($subItem['label']); ?></div>
							<?php if ($subItem['button']): ?>
								<span y-name="button" href="<?php view::attr($subItem['button']['href']); ?>" class="navigation-item-button">
									<i class="icon icon-sm"><?php view::text($subItem['button']['icon']) ?></i>
								</span>
							<?php endif; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php else: ?>
			<div y-name="button" href="<?php view::attr($item['href']) ?>" class="navigation-item">
				<div class="navigation-item-icon">
					<i class="icon icon-sm"><?php view::text($item['icon']) ?></i>
				</div>

				<div class="navigation-item-label">
					<?php view::text($item['label']) ?>
				</div>

				<?php if ($item['button']): ?>
					<div class="navigation-item-button">
						<span href="<?php view::attr($item['button']['href']); ?>">
							<i class="icon icon-sm"><?php view::text($item['button']['icon']) ?></i>
						</span>
					</div>
				<?php endif; ?>
			</div>
		<?php endif; ?>

	<?php endforeach; ?>
</div>


<!--
<div class="navigation-bottom"></div>
-->