<script type="text/html" y-name="search">
	<div class="input-group">
		<div class="input-group-prepend clickable" y-name="remove">
			<div class="input-group-text">
				<i class="icon icon-sm">close</i>
			</div>
		</div>
		<input type="text" class="form-control" y-name="input" value="{{value}}"/>
		<div class="input-group-append clickable" y-name="submit">
			<div class="input-group-text">
				<i class="icon icon-sm">search</i>
			</div>
		</div>
	</div>
</script>