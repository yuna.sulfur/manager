{% if item.update %}
	<a
		href="{{ item.update }}"
		title="<?php view::lang('label.update') ?>"
		class="btn btn-sm btn-outline-primary"
	>
		 <span y-name="locked" style="display: none"></span> <i class="icon icon-sm">edit</i>
	</a>
{% endif %}