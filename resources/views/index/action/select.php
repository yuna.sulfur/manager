{% if item.select %}
	<a class="btn btn-sm btn-outline-primary" y-use="manager.index.Select" href="#" data-callback="<?php view::callback(); ?>" title="<?php view::lang('label.use') ?>" >
		<i class="icon icon-sm">check</i>
	</a>
{% endif %}
