{% if item.delete %}
	<a
		y-use="manager.index.Delete"
		data-csrf="<?php view::csrf(); ?>"
		data-href="{{ item.delete }}"
		data-title="<?php view::lang('dialog.delete.title') ?>"
		data-message="<?php view::attr(fetch::lang('dialog.delete.message')) ?>"
		href="#"
		title="<?php view::lang('label.delete') ?>"
		class="btn btn-sm btn-outline-primary"
	>
		<i class="icon icon-sm">delete</i>
	</a>
{% endif %}