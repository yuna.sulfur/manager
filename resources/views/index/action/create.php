{% if item.create %}
	<a class="btn btn-sm btn-outline-primary" href="{{ item.create }}" title="<?php view::lang('label.create') ?>" >
		<i class="icon icon-sm">add_circle</i>
	</a>
{% endif %}