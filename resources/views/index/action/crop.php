{% if item.crop %}
	<a
		y-use="manager.index.Crop"
		data-url="{{ item.crop }}"
		href="#"
		title="<?php view::lang('label.crop') ?>"
		class="btn btn-sm btn-outline-primary"
	>
		<i class="icon icon-sm">aspect_ratio</i>
	</a>
{% endif %}