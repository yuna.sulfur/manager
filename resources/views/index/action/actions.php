{% if item.actions %}
	<span class="btn btn-sm btn-outline-primary text-primary clickable" data-toggle="dropdown">
		<i class="icon icon-sm">more_vert</i>
		<div class="dropdown-menu dropdown-menu-right float-right">
			{% each item.actions as action : info %}
				{% if 1 == 0 %}
				<?php foreach($actions as $action => $info): ?>
					<?php if(is_int($action)) {
						$action = $info;
						$info = false;
					} ?>

					{% elseif action == '<?php view::text($action) ?>' %}

					<div class="dropdown-item">
						<?php if (is_array($info) && isset($info['view'])): ?>
							<?php view::file('index/action/' . $info['view']); ?>
						<?php else: ?>
							<a href="{{ info }}" title="<?php view::lang('label.' . $action) ?>" >
								<?php view::lang('label.' . $action) ?>
							</a>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
				{% endif %}
			{% endeach %}
		</div>
	</span>
{% endif %}