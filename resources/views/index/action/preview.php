{% if item.preview %}
	<a class="btn btn-sm btn-outline-primary" href="{{ item.preview }}" target="_blank" title="<?php view::lang('label.preview') ?>" >
		<i class="icon">search</i>
	</a>
{% endif %}