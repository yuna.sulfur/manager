<script type="text/html" y-name="pagination">
	<div class="btn-group">


		{% if previous !== false %}
			<span class="btn btn-outline-secondary" y-name="button" data-skip="{{ previous }}"><i class="icon icon-sm">keyboard_arrow_left</i></span>
		{% endif %}

		<span class="btn btn-outline-secondary disabled">
			{{first}}-{{last}} / {{total}}
		</span>

		{% if next !== false %}
			<span class="btn btn-outline-secondary" y-name="button" data-skip="{{ next }}"><i class="icon icon-sm">keyboard_arrow_right</i></span>
		{% endif %}
	</div>
</script>