<script type="text/html" y-name="list">
	<ul class="branch" y-name="container" ></ul>
</script>


<script type="text/html" y-name="item">
	<li class="" y-name="item item-{{ item.data.id }}" data-id="{{ item.data.id }}">
		<div class="leaf">
			<?php if(fetch::config('sortable')): ?>
				<?php view::file('index/action/move'); ?>
			<?php endif; ?>

			<?php if($batch): ?>
				<?php view::file('index/batch/select', ['batch' => $batch]); ?>
			<?php endif; ?>

			<?php foreach (fetch::config('toggle', []) as  $view): ?>
				<?php view::file('index/toggle/' . $view); ?>
			<?php endforeach; ?>

			<?php foreach (fetch::config('columns', []) as $column => $info): ?>
				<?php
				if(is_numeric($column)){
					$column = $info;
					$info = [];
				}
				?>
				<span
					<?php if(fetch::task() == 'select'): ?>
						{% if item.select %}
							y-use="manager.index.Select"
							data-callback="<?php view::callback(); ?>"
							title="<?php view::lang('label.use') ?>"
							class="clickable"
						{% endif %}
					<?php else: ?>
						{% if item.update %}
							y-use="manager.index.Update"
							data-url="{{ item.update }}"
							title="<?php view::lang('label.update') ?>"
							class="clickable"
						{% endif %}
					<?php endif; ?>
				>
					<?php if (isset($info['view'])): ?>
						<?php view::file('index/column/' . $info['view']); ?>
					<?php else: ?>
						{{ item.data.<?php view::text($column); ?> }}
					<?php endif; ?>
				</span>
			<?php endforeach; ?>

			<div class="btn-group float-right">
				<?php view::file('index/action/select'); ?>

				<?php view::file('index/action/create'); ?>

				<?php view::file('index/action/update'); ?>

				<?php view::file('index/action/delete'); ?>

				<?php view::file('index/action/actions', ['actions' => fetch::config('actions', [])]); ?>
			</div>
		</div>
		<ul class="branch" y-name="children"></ul>
	</li>
</script>