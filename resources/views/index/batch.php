<div y-use="manager.index.Batch" class="input-group w-100">
	<div class="input-group-prepend clickable">
		<div class="input-group-text">
			<input y-name="select-all" type="checkbox" />
		 </div>
	</div>

	<select class="form-control" y-name="select">
		<option><?php view::lang('label.with_selected'); ?></option>
		<?php foreach ($batch as $action => $info): ?>
			<?php if ($action === 'delete'): ?>
				<?php view::file('index/batch/delete') ?>
			<?php elseif (is_array($info) && isset($info['view']) ): ?>
				<?php view::file('info/batch/' . $info['view']) ?>
			<?php else: ?>
				<option data-apply="true" data-url="<?php view::action(fetch::module(), $action. '{{id}}') ?>">
					<?php view::lang('label.' . $action) ?>
				</option>
			<?php endif; ?>
		<?php endforeach; ?>
	</select>

	<div y-name="apply" class="input-group-append clickable" style="display:none">
		<div class="input-group-text">
			<?php view::lang('label.apply'); ?>
		 </div>
	</div>
</div>
