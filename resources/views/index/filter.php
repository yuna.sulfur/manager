<script type="text/html" y-name="filter">
	<div class="input-group input-group filter w-25 mr-2 float-left" y-name="filter">
		<div class="input-group-prepend clickable" y-name="remove" style="display:none">
			<div class="input-group-text">
				<i class="icon icon-sm">close</i>
			 </div>
		</div>
		<select name="{{name}}" class="form-control" y-name="select">
			{% each options as value:label %}
				<option value="{{value}}">{{label}}</option>
			{% endeach %}
		</select>
	</div>
</script>