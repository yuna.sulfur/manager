<script type="text/html" y-name="list">
	<table class="table table-hover">
		<thead>
			<tr>
				<?php if(fetch::config('sortable')): ?>
					<?php $sort = []; ?>
					<th width="33"></th>
				<?php elseif(! fetch::config('tree')): ?>
					<?php $sort = fetch::config('sort', []); ?>
				<?php endif; ?>

				<?php if($batch): ?>
					<th width="33"></th>
				<?php endif; ?>

				<?php foreach (fetch::config('toggle', []) as $view): ?>
					<th width="33"></th>
				<?php endforeach; ?>

				<?php foreach (fetch::config('columns', []) as $key => $value): ?>

					<?php
					if(is_numeric($key)){
						$column = $value;
						$params = [];
					} else {
						$column = $key;
						$params = $value;
					}
					?>

					<th<?php view::raw(isset($params['width']) ? ' width="' . $params['width'] . '"' : ''); ?>>
						<?php if (isset($sort[$column])): ?>
							<span y-name="sort" class="clickable btn btn-light btn-sm" data-column="<?php view::attr($column); ?>" data-direction="<?php view::attr($sort[$column]); ?>" href="#">
								<?php view::lang('field.' . $column); ?>&nbsp;
								<i class="icon icon-sm" y-name="asc" style="display:none">keyboard_arrow_up</i>
								<i class="icon icon-sm" y-name="desc" style="display:none">keyboard_arrow_down</i>
							</span>
						<?php else: ?>
							<?php view::lang('field.' . $column); ?>
						<?php endif; ?>
					</th>
				<?php endforeach; ?>

				<th><!-- tools --></th>

			</tr>
		</thead>

		<tbody y-name="container"></tbody>

	</table>
</script>


<script type="text/html" y-name="item">
	<tr
		y-name="item item-{{ item.data.id }}"
		data-id="{{ item.data.id }}"
	>

		<?php if(fetch::config('sortable')): ?>
			<td>
				<?php view::file('index/action/move'); ?>
			</td>
		<?php endif; ?>

		<?php if($batch): ?>
			<td width="33">
				<?php view::file('index/batch/select', ['batch' => $batch]); ?>
			</td>
		<?php endif; ?>

		<?php foreach (fetch::config('toggle', []) as  $view): ?>
			<td>
				<?php view::file('index/toggle/' . $view); ?>
			</td>
		<?php endforeach; ?>


			<?php foreach (fetch::config('columns', []) as $key => $value): ?>
				<?php
				if(is_numeric($key)){
					$column = $value;
					$params = [];
				} else {
					$column = $key;
					$params = $value;
				}
				?>
				<td
					<?php if(fetch::task() == 'select'): ?>
						{% if item.select %}
							y-use="manager.index.Select"
							data-callback="<?php view::callback(); ?>"
							title="<?php view::lang('label.use') ?>"
							class="clickable"
						{% endif %}
					<?php else: ?>
						{% if item.update %}
							y-use="manager.index.Update"
							data-url="{{ item.update }}"
							title="<?php view::lang('label.update') ?>"
							class="clickable"
						{% endif %}
					<?php endif; ?>
				>
					<?php if (isset($params['view'])): ?>
						<?php view::file('index/column/' . $params['view']); ?>
					<?php else: ?>
						{{ item.data.<?php view::text($column); ?> }}
					<?php endif; ?>
				</td>
			<?php endforeach; ?>


		<td>
			<div class="btn-group float-right">
				<?php view::file('index/action/select'); ?>

				<?php view::file('index/action/update'); ?>

				<?php view::file('index/action/delete'); ?>

				<?php view::file('index/action/actions', ['actions' => fetch::config('actions', [])]); ?>

			</div>
		</td>
	</tr>
</script>