<script type="text/html" y-name="list">
	<div>
		<div class="mb-3">
			<span class="btn btn-default btn-sm disabled"><?php view::lang('label.sort') ?></span>
			<?php foreach (fetch::config('sort', []) as $column => $direction): ?>
				<span y-name="sort" class="clickable btn btn-outline-secondary btn-sm" data-column="<?php view::attr($column); ?>" data-direction="<?php view::attr($direction); ?>" href="#">
					<?php view::lang('field.' . $column); ?>&nbsp;
					<i class="icon icon-sm" y-name="asc" style="display:none">keyboard_arrow_up</i>
					<i class="icon icon-sm" y-name="desc" style="display:none">keyboard_arrow_down</i>
				</span>
			<?php endforeach; ?>
		</div>

		<div class="row" y-name="container"></div>
	</div>
</script>


<script type="text/html" y-name="item">
	<div class="col-sm-12 col-md-6 col-lg-3 col-xl-2" y-name="item item-{{ item.data.id }}" data-id="{{ item.data.id }}">
		<div class="card mb-2">
			<div class="card-header">
				<?php if($batch): ?>
					<?php view::file('index/batch/select', ['batch' => $batch]); ?>
				<?php endif; ?>
				<div class="btn-group float-right">
					<?php view::file('index/action/select'); ?>

					<?php view::file('index/action/update'); ?>

					<?php view::file('index/action/delete'); ?>

					<?php view::file('index/action/crop'); ?>

					<?php view::file('index/action/actions', ['actions' => fetch::config('actions', [])]); ?>
				</div>
			</div>
			<div class="card-img" style="background-color: #DDD; display:flex;">
				<img y-name="image" class="" height="130" src="<?php view::action('image','serve','{{ item.data.id }}', 'preset=manager') ?>" style="object-fit: contain; object-position: 50% 50%; width:100%">
			</div>
			<div class="card-body">
				{{ item.data.title }}
			</div>
		</div>
	</div>
</script>