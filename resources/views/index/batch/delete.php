
<option
	data-apply="true"
	data-invoke="delete"

	y-use="manager.index.batch.Delete"
	data-csrf="<?php view::csrf(); ?>"
	data-url="<?php view::action(fetch::module(), 'delete', '{{id}}') ?>"
	data-title="<?php view::lang('dialog.delete.title') ?>"
	data-message="<?php view::attr(fetch::lang('dialog.delete_batch.message')) ?>"

	class="clickable"
>
	<?php view::lang('label.delete') ?>
</option>