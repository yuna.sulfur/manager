{% if item.status %}
	<span
		class=""
		y-use="manager.index.Toggle"
		data-value="{{ item.data.status }}"
		data-url="{{ item.status }}"
		data-csrf="<?php view::csrf(); ?>"
	>
		<span y-name="state live" data-status="edit" class="clickable text-success" style="display: none"><i class="icon icon-sm">visibility</i></span>
		<span y-name="state edit" data-status="live" class="clickable" style="display: none; opacity: 0.3;"><i class="icon icon-sm">visibility_off</i></span>
	</span>
{% endif %}