<span>
	{% if item.data.type == 'default' %}
		{{ item.data.title }}
	{% elseif item.data.type == 'html' %}
		{% var html = item.data.html ? item.data.html.substr(0, 100) : '<>'; %}
		{{ html }}
	{% else %}
		{{ item.data.type }}
	{% endif  %}
</span>