<?php if(fetch::task() == 'select'): ?>
	{% if item.select %}
		<a y-use="manager.index.Select" href="#" data-callback="<?php view::callback(); ?>" title="<?php view::lang('label.use') ?>" >
			{{ item.data.title }}
		</a>
	{% endif %}
<?php else: ?>
	{% if item.update %}
		<a
			href="{{ item.update }}"
			title="<?php view::lang('label.update') ?>"
		>
			 {{ item.data.title }}
		</a>
	{% endif %}
<?php endif; ?>