<?php view::extend('html'); ?>

<?php view::block('body.class', 'login') ?>

<?php view::start('body'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-6 col-offset-3 card login-card">
				<span class="title"><?php view::lang('label.login'); ?></span>
				<?php if($error): ?>
					<div class="alert alert-warning">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php view::lang(['error', $error]); ?>
					</div>
				<?php endif ?>
				<?php view::file('form/form', ['form' => $form]); ?>
			</div>
		</div>
	</div>
<?php view::end(); ?>