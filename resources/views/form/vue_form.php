<?php
$wrap = function($elements, $parentType = 'none') use (& $wrap) {
	$config = [
		'column' => 'row',
		'tab' => 'tabs'
	];

	$wrapping = null;

	foreach($elements as $index => $element) {
		if($element->type === 'group') {
			// first, recursively wrap nested elements
			$element->group->elements = $wrap($element->group->elements, $element->group->type);
		}

		if($element->type === 'group' && isset($config[$element->group->type]) && $parentType !== $config[$element->group->type]) {
			// this is a group that should be wrapped, but without a proper wrapper
			// elements should be placed inside a wrapper group
			if($wrapping === null || $wrapping->group->type !== $config[$element->group->type]){
				// there is no active wrapping group
				// or there is an active wrapping group for a different type
				// Create one, with the current element as its first element
				$wrapping = (object) [
					'type' => 'group',
					'group' => (object) [
						'type' => $config[$element->group->type],
						'elements' => [$element]
					]
				];
				// replace the current element with the tabs element
				$elements[$index] = $wrapping;
			} else {
				// already wrapping
				// place current element in the currently wrapping group
				$wrapping->group->elements[] = $element;
				// remove it from it's current position
				unset($elements[$index]);
			}
		} else {
			// this is no wrapping group: unset the wrapper
			$wrapping = null;
		}
	}
	return $elements;
};




$vue = function() {

}
?>







<?php
// $layout = $vue($wrap($form->layout()));




/*
foreach($wrap($form->layout()) as $element) {
	if($element->type === 'group'){
		view::file('form/group/' . $element->group->type, ['group' => $element->group]);
	} elseif($element->type === 'markup') {
		view::file('form/markup/' . $element->markup->type, ['markup' => $element->markup]);
	} else {
		view::file('form/element', ['element' => $element->element]);
	}
} ?>
 *
 */
?>

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

<?php $id = '__id__' . rand(0, 10000000); ?>
<div id="<?php echo $id ?>">
	<form-group>

	</form-group>
</div>

<?php
$types = [];
foreach($form->elements() as $element){
	var_dump($element);
	$types = [];
	if(! in_array($element->type, $types)) {
		$types[] = $element->type;
		view::file('form/vue/element-' . $element->type);
	}
}
?>



<script>
Vue.component('form-group', {
	template: '<div><element-text></element-text><element-password></element-password></div>'
});

Vue.component('form-element', {
	template: '<div></div>'
});


var app = new Vue({
	el: '#<?php echo $id ?>',
	data: {
		layout: <?php view::raw(json_encode(['elements' => ['<\'"', 'bar']])); ?>
	},
})

</script>




<?php
/*
$elements = [];
foreach($form->elements() as $element) {
	$id = '__' . substr(md5(rand(0, 10000000)), 0, 10);
	$element->id = $id;
	$elements[$element->key] = $id;
}
?>

<form
	y-use="manager.Form<?php view::attr($form->attribute('y-use') ? ' '.$form->attribute('y-use') : '') ?>"
	y-name="form"
	action="<?php view::attr($form->attribute('action')); ?>"
	method="<?php view::attr($form->attribute('method')); ?>"
	enctype="<?php view::attr($form->attribute('enctype')); ?>"
	data-submit="<?php view::attr($form->attribute('submit') ? 'true' : 'false'); ?>"
	data-elements="<?php view::attr(json_encode($elements)); ?>"
	<?php if ($class = $form->attribute('class')): ?>
		class="<?php view::attr($class); ?>"
	<?php endif; ?>
>
	<?php view::file('form/group', ['elements' => $wrap($form->layout())]); ?>
</form>

 */?>