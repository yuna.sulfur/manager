<?php view::asset('js', fetch::base().'vendor/moment/Moment.js'); ?>

<div
	y-use="manager.form.element.Date"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr($element->value ? $element->value : date('Y-m-d H:i:s') ) ?>"
	data-lang="<?php view::lang(); ?>"
	data-time="<?php view::attr($element->time ? 'true' : 'false') ?>"
	data-offset="<?php
		if($element->timezone) {
			$time = new DateTime();
			view::attr(
				(
					timezone_offset_get(new DateTimeZone($element->timezone), $time) -
					timezone_offset_get(new DateTimeZone(date_default_timezone_get()), $time)
				) / 60
			);
		} else {
			view::attr(0);
		}
	?>"
>
	<input type="text" class="form-control" data-toggle="dropdown" y-name="date" readonly="readonly" />
	<div class="dropdown-menu dropdown-menu-right float-right" y-name="container"></div>


	<script type="text/html" y-name="calendar">
		<div>
			<div class="row">
				<div class="col-2">
					<i class="icon clickable" y-on="click|stop:previous">keyboard_arrow_left</i>
				</div>
				<div class="col-8 text-center">
					<h2>{{ month }} {{ year }}</h2>
				</div>
				<div class="col-2 float-right">
					<i class="icon clickable" y-on="click|stop:next">keyboard_arrow_right</i>
				</div>
			</div>
			<table class="table">
				<thead>
					<tr>
						{% each days as day %}
							<th scope="col" class="text-center">{{ day }}</th>
						{% endeach %}
					</tr>
				</thead>
				<tbody>
					{% each weeks as week %}
						<tr>
							{% each week as day %}
								{% if day %}
									<td class="text-center"  y-on="click|stop:date({{ year }}, {{ monthnumber }}, {{ day }})">
										<span y-name="day day-{{ day }}" class="badge {% if active == day %}badge-primary{% else %}badge-light{% endif %} clickable">
											{{ day }}
										</span>
									</td>
								{% else %}
									<td>&nbsp;</td>
								{% endif %}
							{% endeach %}
						</tr>
					{% endeach %}
				</tbody>
			</table>
			{% if time %}
				<div class="row">
					<div class="col-2"></div>
					<div class="col-8">
						<table class="table table-borderless table-sm">
							<tr>
								<td  class="text-center"><i class="icon clickable" y-on="click|stop:hourup">keyboard_arrow_up</i></td>
								<td  class="text-center"><i class="icon clickable" y-on="click|stop:minuteup">keyboard_arrow_up</i></td>
							</tr>
							<tr>
								<td  class="text-center"><input type="text" class="form-control form-control-lg text-center" y-on="change:hourchange" y-name="hour" value="{{ current.hour }}" /></td>
								<td  class="text-center"><input type="text" class="form-control form-control-lg text-center" y-on="change:minutechange" y-name="minute" value="{{ current.minute }}" /></td>
							</tr>
							<tr>
								<td  class="text-center"><i class="icon clickable" y-on="click|stop:hourdown">keyboard_arrow_down</i></td>
								<td  class="text-center"><i class="icon clickable" y-on="click|stop:minutedown">keyboard_arrow_down</i></td>
							</tr>
						</table>
					</div>
				</div>
			{% endif %}
		</div>
	</script>
</div>