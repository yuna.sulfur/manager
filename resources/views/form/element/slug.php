<input
	y-use="manager.form.element.Slug"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr($element->value) ?>"
	data-source="<?php view::attr($element->source); ?>"
	class="form-control"
	type="text"
/>