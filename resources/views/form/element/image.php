<?php view::asset('js', fetch::base() . 'vendor/jquery/UI.js'); ?>
<?php view::asset('js', fetch::base().'vendor/dropzone/Dropzone.js'); ?>
<?php $module = $element->module ? $element->module : 'image' ?>
<div
	y-use="manager.form.element.Image"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?> relation"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-url_create="<?php view::action($module, 'create'); ?>"
	data-url_crop="<?php view::action($module, 'crop', '{{id}}', 'callback={{callback}}'); ?>"
	data-url_delete="<?php view::action($module, 'delete', '{{id}}'); ?>"
	data-csrf="<?php view::csrf(); ?>"
	data-multiple="<?php view::attr($element->multiple ? 'true' : 'false') ?>"
	data-max="<?php view::attr($element->multiple ? ($element->max ? $element->max : '9999') : 1 ) ?>"
	data-title="<?php view::lang('dialog.delete_image.title') ?>"
	data-message="<?php view::attr(fetch::lang('dialog.delete_image.message')) ?>"
	data-instance="<?php view::attr(fetch::lang('label.delete_instance')) ?>"
	data-original="<?php view::attr(fetch::lang('label.delete_original')) ?>"
>


	<div y-name="container list-group"></div>
	<div y-name="zone" class="upload-area mt-1 clickable"><?php view::lang('title.drop', $module); ?></div>


	<script type="text/html" y-name="image">
		<div y-name="image" data-id="{{ id }}" class="list-group-item">
			<div class="row">
				<div class="col-3">
					{% var ts = new Date().getTime() %}
					<img y-name="img" class="w-100" src="<?php view::action($module,'serve','{{ id }}', 'preset=manager&_={{ ts }}') ?>">
				</div>

				<?php if (is_array($element->junction)): ?>
					<span class="col-6">
						<?php foreach ($element->junction as $junction): ?>
							<?php $label = isset($junction['label']) ? $junction['label'] : fetch::lang('field.' . $junction[0]); ?>
							<?php if (! isset($junction[1]) || $junction[1] == 'text'): ?>
								<input type="text" class="form-control mb-1" placeholder="<?php view::attr($label) ?>" y-name="junction" data-name="<?php view::attr($junction[0]) ?>" value="{{ junction.<?php view::attr($junction[0]); ?> }}" />
							<?php elseif (! isset($junction[1]) || $junction[1] == 'textarea'): ?>
								<textarea class="form-control mb-1" placeholder="<?php view::attr($label) ?>" y-name="junction" data-name="<?php view::attr($junction[0]) ?>">{{ junction.<?php view::attr($junction[0]); ?> }}</textarea>
							<?php elseif ($junction[1] == 'select'): ?>
								<select class="form-control mb-1" y-name="junction" data-name="<?php view::attr($junction[0]) ?>">
									<option><?php view::text($label); ?></option>
									<?php foreach($junction['options'] as $option => $label): ?>
										<?php if(is_int($option)) {
											$option = $label;
											$label = fetch::lang('option.' . $junction[0] . '.' . $option);
										} ?>
										<option {% if junction.<?php view::attr($junction[0]); ?> == "<?php view::attr($option) ?>" %}selected="selected"{% endif %} value="<?php view::attr($option); ?>"><?php view::text($label); ?></option>
									<?php endforeach; ?>
								</select>
							<?php endif; ?>
						<?php endforeach; ?>
					</span>
				<?php endif; ?>

				<div class="col">
					<div class="float-right">
						<span class="clickable text-secondary mr-1" y-name="crop" title="<?php view::lang('label.crop') ?>" ><i class="icon icon-sm">aspect_ratio</i></span>
						<span class="clickable text-secondary" y-name="delete" title="<?php view::lang('label.delete') ?>" ><i class="icon icon-sm">delete</i></span>
					</div>
				</div>
			</div>

		</div>
	</script>
</div>