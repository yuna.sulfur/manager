<div
	y-use="manager.form.element.Url"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?> url"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr($element->value) ?>"
	data-url_construct="<?php view::attr(fetch::action('link', 'build')); ?>"
>
	<div class="input-group">
		<input class="form-control" type="text" />
		<div class="input-group-append">
			<button class="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown"><?php view::lang('label.select'); ?></button>
			<div class="dropdown-menu">
				<?php foreach ($element->presets as $preset => $info): ?>
					<?php if(is_int($preset)) {
						$preset = $info;
						$info = [];
					}
					if(isset($info['route'])) {
						$route = $info['route'];
					} else {
						$route = $preset;
					}
					?>
					<a class="dropdown-item" href="#" y-name="preset" data-title="<?php view::lang('preset.' . $preset); ?>" data-route="<?php view::attr($route); ?>" data-info="<?php view::attr(json_encode($info)); ?>">
						<?php view::lang('preset.' . $preset); ?>
					</a>
				<?php endforeach; ?>

				<?php foreach ($element->modules as $module): ?>
					<a class="dropdown-item" href="#" y-name="select" data-module="<?php view::attr($module); ?>" data-url="<?php view::action($module, 'index', null, 'viewport=module&task=select&callback={{callback}}') ?>">
						<?php view::lang('single', $module); ?>
					</a>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>