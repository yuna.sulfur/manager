<div
	y-use="manager.form.element.Seo"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-script="<?php view::attr(fetch::base().'vendor/yoast/yoast.js') ?>"

	data-source_title="<?php view::attr($element->source['title']) ?>"
	data-source_slug="<?php view::attr($element->source['slug']) ?>"
	data-source_body="<?php view::attr(json_encode($element->source['body'])) ?>"
	data-base="<?php view::url(fetch::config()->navigation('frontend')) ?>"
	data-website="<?php view::attr(fetch::config()->zones(fetch::zone() . '.title')); ?>"
	data-lang="<?php view::lang(); ?>"
	data-locale="<?php view::attr($element->locale ? $element->locale : 'false' ) /* en_US de_DE es_ES nl_NL it_IT fr_FR pl_PL ru_RU */ ?>"
	data-readability="<?php view::lang('label.readability') ?>"
	data-findability="<?php view::lang('label.findability') ?>"
>


	<div class="list-group">
		<div class="list-group-item">
			<div y-name="snippet"></div>
		</div>
		<div class="list-group-item">
			<div y-name="bar"></div>
		</div>

		<div y-name="advanced" class="list-group-item form-group" style="display:none;">
			<div class="card bg-light">
				<div class="card-body">
					<div class="form-group">
						<label><?php view::lang('label.seo_title') ?></label>
						<input y-name="title" class="form-control" type="text"/>
					</div>

					<div class="form-group">
						<label><?php view::lang('label.seo_description') ?></label>
						<textarea y-name="description" class="form-control"></textarea>
					</div>
					<div class="form-group">
						<label><?php view::lang('label.seo_keyword') ?></label>
						<input y-name="keyword" class="form-control" type="text" />
					</div>
				</div>
			</div>
			<div class="mt-3" y-name="result-seo"></div>

			<div class="mt-4" y-name="result-content"></div>

		</div>

		<div y-name="more" class="list-group-item">
			<span class="btn btn-sm btn-primary clickable"><?php view::lang('label.improve'); ?></span>
		</div>

	</div>







	<script type="text/html" y-name="snippet">
		<div>
			<h5 class="google-title text-primary yf-google-title" style="display:inline">{{ title }}</h5>
			<br />
			<small class="google-url text-success">{{ base }} › {{ slug }}</small>
			<div class="google-description">{{ description }}</div>
		</div>
	</script>


	<script type="text/html" y-name="bar">
		<div class="progress">
			<div class="progress-bar progress-bar-striped bg-{% if percent <= 40 %}danger{% elseif percent > 40 && percent <= 70 %}warning{% else %}success{% endif %}" style="width: {{ percent }}%"></div>
		</div>
	</script>


	<script type="text/html" y-name="result">
		<div>
			<div class="alert alert-{% if score <= 4%}danger{% elseif score > 4 && score <= 6 %}warning{% else %}success{% endif %}">
				<i class="icon">{% if score <= 4 %}warning{% elseif score > 4 && score <= 6 %}error{% else %}check{% endif %}</i>
				{{ label }}
			</div>

			<div class="list-group list-group-flush">
				{% each results as result %}
					<div class="d-flex mt-3 text-{% if  result.score <= 4 %}danger{% elseif result.score > 4 && result.score <= 6 %}warning{% else %}success{% endif %}">
						<i class="icon mr-2">{% if result.score <= 4 %}warning{% elseif result.score > 4 && result.score <= 6 %}error{% else %}check{% endif %}</i>
						<span>{{{ result.text }}}</span>
					</div>
				{% endeach %}
			</div>
		</div>

	</script>

</div>