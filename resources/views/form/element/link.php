<div
	y-use="manager.form.element.Link"
	y-name="element element-<?php view::attr($element->key) ?>  <?php view::attr($element->id); ?> link"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-url="<?php view::attr(fetch::action('link', 'update', null, 'callback={{callback}}')); ?>"
>
	<span y-name="container"></span>

	<span class="clickable btn btn-outline-secondary" y-name="create">+ <?php view::lang('label.add'); ?></span>


	<script type="text/html" y-name="link">
		<div class="list-group">
			<div class="list-group-item">
				<div y-name="update">
					{% if typeof blank != 'undefined' && blank == 1 %}<i class="icon icon-sm">open_in_new</i> {% endif %}
					{% if typeof nofollow != 'undefined' && nofollow == 1 %}<i class="icon  icon-sm">block</i> {% endif %}
					{{ title }} <i class="icon">forward</i> {{ url }}
				</div>
				<div class="float-right">
					<span class="clickable text-secondary mr-1" y-name="update" title="<?php view::lang('label.update') ?>" ><i class="icon icon-sm">edit</i></span>
					<span class="clickable text-secondary" y-name="delete" title="<?php view::lang('label.delete') ?>" ><i class="icon icon-sm">delete</i></span>
				</div>
			</div>
		</div>
	</script>
</div>