<div class="row" y-use="manager.form.element.Timed">
	<div class="col-6">
		<div
			y-use="manager.form.element.Toggle"
			y-name="element start <?php view::attr($element->id); ?>"
			data-key="<?php view::attr($element->key . '[use_time_start]') ?>"
			data-value="<?php view::attr($element->value('use_time_start') ?  $element->value('use_time_start') :  0) ?>"
			class="form-check"
		>
			<input class="form-check-input" type="checkbox">
			<label class="form-check-label"><?php view::lang('field.time_start'); ?></label>
		</div>
		<div y-name="start">
			<?php view::file('form/element/dynamic', ['element' => [
				$element->key . '[time_start]',
				'date',
				'label' => false,
				'time' => false,
				'value' => $element->value('time_start') ?  $element->value('time_start') :  date('Y-m-d H:i:s')
			]]) ?>

		</div>
	</div>

	<div class="col-6">
		<div
			y-use="manager.form.element.Toggle"
			y-name="element <?php view::attr($element->id); ?> end"
			data-key="<?php view::attr($element->key . '[use_time_end]') ?>"
			data-value="<?php view::attr($element->value('use_time_end') ?  $element->value('use_time_end') :  0) ?>"
			class="form-check"
		>
			<input class="form-check-input" type="checkbox">
			<label class="form-check-label"><?php view::lang('field.time_end'); ?></label>
		</div>
		<div y-name="end">
			<?php view::file('form/element/dynamic', ['element' => [
				$element->key . '[time_end]',
				'date',
				'label' => false,
				'time' => false,
				'value' => $element->value('time_end') ?  $element->value('time_end') :  date('Y-m-d H:i:s')
			]]) ?>
		</div>
	</div>
</div>