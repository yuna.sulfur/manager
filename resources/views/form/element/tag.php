
<div
	y-use="manager.form.element.Tag"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-url_search="<?php view::action('tag', 'items', null, 'search={{query}}&amount=10') ?>"
	data-url_popular="<?php view::action('tag', 'items', null, 'sort=popular&amount=20') ?>"
	data-url_create="<?php view::action('tag', 'save') ?>"
	data-csrf="<?php view::csrf() ?>"
>


	<div class="input-group mb-1">
		<div class="input-group-prepend dropdown">

				<span class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown"><?php view::lang('label.popular'); ?></span>
				<div class="dropdown-menu" y-name="popular"></div>

		</div>

		<div class="dropdown">
			<div y-name="suggestions" class="dropdown-menu"></div>
		</div>

		<input class="form-control" type="text" y-name="input" />



		<div class="input-group-append" y-name="add" style="display:none;">
			<span class="btn btn-outline-secondary"><?php view::lang('label.add'); ?></span>
		</div>
	</div>


	<span y-name="tags"></span>




	<script type="text/html" y-name="suggestion">
		<div class="dropdown-item clickable">{{ title }}</div>
	</script>

	<script type="text/html" y-name="tag">
		<span class="btn btn-sm btn-secondary mr-1 mb-1" y-name="tag">{{ title }} <span y-name="remove" class="clickable">&times;</span></span>
	</script>
</div>

