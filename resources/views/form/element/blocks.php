<?php view::asset('js', fetch::base() . 'vendor/jquery/UI.js'); ?>

<div
	y-use="manager.form.element.Blocks"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?> blocks"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-url="<?php view::attr(fetch::action('block', 'update', '{{type}}', 'callback={{callback}}')); ?>"
	data-max="<?php view::attr($element->max ? $element->max : '9999' ) ?>"
	data-autoselect="<?php view::attr($element->autoselect ? 'true' : 'false' ) ?>"
>

	<div y-name="blocks" class="mb-2 list-group bg-light"></div>

	<div class="btn-group" y-name="blocks-add">
		<span class="btn btn-secondary">+</span>
		<?php foreach ($element->types as $type => $options): ?>
			<?php if(is_int($type)) {
				$type = $options;
				$options = [];
			} ?>
			<span class="btn btn-outline-secondary" y-name="block-add" data-type="<?php view::attr($type) ?>">
				<?php view::lang('type.' . $type, 'block'); ?>
			</span>
		<?php endforeach; ?>

		<?php if(is_array($element->more) && count($element->more) > 0): ?>
			<div class="btn-group" role="group">
				<button class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">
					<?php view::lang('label.more') ?>
				 </button>
				<div class="dropdown-menu">
					<?php foreach ($element->more as $type => $options): ?>
						<?php if(is_int($type)) {
							$type = $options;
							$options = [];
						} ?>
						<span class="dropdown-item clickable" y-name="block-add" data-type="<?php view::attr($type) ?>">
							<?php view::lang('type.' . $type, 'block'); ?>
						</span>
					<?php endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	</div>

	<?php
	$types = $element->types;
	if(is_array($element->more)) {
		$types = array_merge($types, $element->more);
	}
	?>
	<?php foreach ($types as $type => $options): ?>
		<?php if(is_int($type)) {
			$type = $options;
			$options = [];
		} ?>
		<div
			y-name="prototype"
			data-type="<?php view::attr($type) ?>"
			data-name="<?php view::lang('type.' . $type, 'block'); ?>"
			style="display:none"
		>
			<?php view::file('form/element/block/' . $type, $options); ?>
		</div>
	<?php endforeach; ?>

	<script type="text/html" y-name="block">
		<div y-name="block" class="list-group-item" data-type="{{type}}">
			<div class="relation-header mb-4">
				<small>{{ name }}</small>
				<div class="float-right">
					<span y-name="block-up" class="text-secondary clickable mr-1">
						<i class="icon icon-sm">keyboard_arrow_up</i>
					</span>
					<span y-name="block-down" class="text-secondary clickable mr-1">
						<i class="icon icon-sm">keyboard_arrow_down</i>
					</span>
					<span y-name="block-update" class="text-secondary clickable">
						<i class="icon icon-sm">edit</i>
					</span>
					<span y-name="block-delete" class="text-secondary clickable">
						<i class="icon icon-sm">delete</i>
					</span>
				</div>
			</div>
		</div>
	</script>
</div>