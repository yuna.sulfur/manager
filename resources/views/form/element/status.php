<?php
$options = ['edit', 'live'];
$value = in_array($element->value, $options) ? $element->value : 'edit';
?>
<div
	y-use="manager.form.element.Status"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr($value) ?>"
>

	<span y-name="container">
		<div class="btn-group">
			<span class="btn btn-outline-secondary dropdown-toggle" data-toggle="dropdown">
				<i class="icon icon-sm text-success" style="display:none;" y-name="live">visibility</i>
				<i class="icon icon-sm" style="opacity: 30%; display:none;" y-name="edit">visibility_off</i>
				<span y-name="status"><?php view::lang('option.status.' . $value) ?></span>
			</span>
			<div class="dropdown-menu">
				<span y-name="option" class="dropdown-item clickable" data-value="edit">
					<i class="icon icon-sm" style="opacity: 30%">visibility_off</i> <span y-name="label"><?php view::lang('option.status.edit') ?></span>
				</span>
				<span y-name="option" class="dropdown-item clickable text-success" data-value="live">
					<i class="icon icon-sm">visibility</i> <span y-name="label"><?php view::lang('option.status.live') ?></span>
				</span>
			</div>
		</div>
	</span>
</div>