<?php view::asset('js', fetch::base().'vendor/jquery/UI.js'); ?>

<div
	y-use="manager.form.element.Form"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?> form"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
	data-name="<?php view::lang('label.name'); ?>"
	data-shortcodes="<?php view::attr(json_encode(fetch::config('shortcodes',[]))); ?>"
>

	<style>
		.form-grid {
			background: #F3F3F3;
			padding: 0 20px 0 20px;
		}

		.form-row {
			min-height: 20px;
		}

		.form-col {
			margin: 0 10px 0 10px;
			min-height: 50px;
			background: #FFF;
			border-radius: 3px;
		}

		.form-col-active {
			background: #FFF;
			border: 2px solid #80bdff;
		}

		.form-placeholder {
			height: 50px;
			margin: 0 10px 0 10px;
			background: #fff;
			border: 3px dotted #DDD;
		}
	</style>


	<div class="row">
		<div class="col-8">
			<div class="form-grid mb-3" y-name="grid"></div>
			<span class="clickable btn btn-outline-secondary" y-name="add">+ <?php view::lang('label.add'); ?></span>
		</div>
		<div class="col-4">
			<label><?php view::lang('label.hook'); ?></label>
			<div y-name="hooks"></div>
			<div class="btn-group">
				<span class="btn btn-secondary">+</span>
				<span class="clickable btn btn-outline-secondary" y-name="add-hook" data-type="email"><?php view::lang('label.email'); ?></span>
				<span class="clickable btn btn-outline-secondary" y-name="add-hook" data-type="webhook"><?php view::lang('label.webhook'); ?></span>
				<span class="clickable btn btn-outline-secondary" y-name="add-hook" data-type="javascript"><?php view::lang('label.javascript'); ?></span>
				<?php if(count(fetch::config('hooks', [])) > 0): ?>
					<span class="clickable btn btn-outline-secondary" y-name="add-hook" data-type="preset"><?php view::lang('label.preset'); ?></span>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<script type="text/html" y-name="row">
		<div class="row form-row" y-name="row"></div>
	</script>

	<script type="text/html" y-name="element">
		<div class="col form-col movable p-2" y-name="element">
			<div class="mb-1">
				<div class="input-group">
					<div class="input-group-prepend">
						<div class="input-group-text form-control">
							<i class="icon icon-sm">drag_indicator</i>
						</div>
					</div>
					<input type="text" y-name="name" class="form-control mb-2" placeholder="<?php view::lang('element.name'); ?>" />
					<div class="input-group-append clickable" >
						<div class="input-group-text form-control" y-name="delete" title="<?php view::lang('label.delete') ?>">
							<i class="icon icon-sm">delete</i>
						</div>
					</div>
				</div>
			</div>
			<div y-name="preview"></div>
		</div>
	</script>


	<script type="text/html" y-name="preview">
		<div>
			{% if type == 'toggle' %}
				<div class="form-check">
					<input type="checkbox" class="form-check-input" disabled />
					<label class="form-check-label">{% if label %}{{label}}{% else %}<?php view::lang('label.no_label') ?>{% endif %}</label>
				</div>
			{% else %}
				 <label>{% if label %}{{label}}{% else %}<?php view::lang('label.no_label') ?>{% endif %}</label>
			{% endif %}
			<br />
			{% if info %}<small>{{ info }}</small><br />{% endif %}

			{% if type == 'text' %}
				<input type="text" class="form-control" readonly disabled />
			{% elseif type == 'email' %}
				<div class="input-group mb-2">
					<div class="input-group-prepend">
						<div class="input-group-text">@</div>
					</div>
					<input type="text" class="form-control" readonly disabled />
				</div>
			{% elseif type == 'textarea' %}
				<textarea  class="form-control" readonly disabled></textarea>
			{% elseif type == 'radio' %}
				{% each options as option %}
					<div class="form-check">
						<input type="radio" class="form-check-input" disabled />
						<label class="form-check-label">{{ option }}</label>
					</div>
				{% endeach %}
			{% elseif type == 'checkbox' %}
				{% each options as option %}
					<div class="form-check">
						<input type="checkbox" class="form-check-input" disabled />
						<label class="form-check-label">{{ option }}</label>
					</div>
				{% endeach %}
			{% endif %}
			{% if required %}
				<div class="mt-1">
					<i class="icon icon-sm">error</i> <label><?php view::lang('label.required') ?></label>: {% if error %}"{{error}}"{% else %}<?php view::lang('label.no_error') ?>{% endif %}
				</div>
			{% endif %}
		</div>
	</script>

	<script type="text/html" y-name="settings">
		<div y-name="settings">
			<div class="row">
				<div class="col">
					<label><?php view::lang('field.label'); ?></label>
					<input type="text" y-name="label" class="form-control mb-2" />
					<label><?php view::lang('field.type'); ?></label>
					<select y-name="type" class="form-control mb-2">
						<option value="text"><?php view::lang('option.type.text'); ?></option>
						<option value="textarea"><?php view::lang('option.type.textarea'); ?></option>
						<option value="radio"><?php view::lang('option.type.radio'); ?></option>
						<option value="checkbox"><?php view::lang('option.type.checkbox'); ?></option>
						<option value="email"><?php view::lang('option.type.email'); ?></option>
						<option value="toggle"><?php view::lang('option.type.toggle'); ?></option>
					</select>

					<label>
						<div class="form-check mb-2">
							<input type="checkbox" class="form-check-input" y-name="required" />
							<?php view::lang('label.required'); ?>
						</div>
					</label>

					<input type="text" y-name="error" class="form-control" placeholder="<?php view::lang('field.error'); ?>" />
				</div>

				<div class="col">
					<label><?php view::lang('field.info'); ?></label>
					<input type="text" y-name="info" class="form-control mb-2" />
					<div class="mb-2" y-name="settingsOptions">
						<label><?php view::lang('field.options'); ?></label>
						<div y-name="options"></div>
						<span class="clickable btn btn-outline-secondary" y-name="add-option">+ <?php view::lang('label.add'); ?></span>
					</div>
				</div>
			</div>
		</div>
	</script>


	<script type="text/html" y-name="option">
		<div class="input-group">
			<div class="input-group-prepend">
				<div class="input-group-text form-control">
					<i class="icon icon-sm">drag_indicator</i>
				</div>
			</div>
			<input type="text" y-name="option" class="form-control mb-2" placeholder="<?php view::lang('field.option'); ?>" />
			<div class="input-group-append clickable" >
				<div class="input-group-text form-control" y-name="delete-option" title="<?php view::lang('label.delete') ?>">
					<i class="icon icon-sm">delete</i>
				</div>
			</div>
		</div>
	</script>


	<script type="text/html" y-name="hook-email">
		<div class="card bg-light mb-4" y-name="hook" data-type="email">
			<div class="card-body">
				<i class="icon icon-sm float-right clickable" y-name="delete-hook">delete</i>
				<label><?php view::lang('field.subject'); ?></label>
				<input type="text" y-name="subject" class="form-control mb-2" placeholder="" />
				<label><?php view::lang('field.from'); ?></label>
				<input type="text" y-name="from" class="form-control mb-2" placeholder="" />
				<label><?php view::lang('field.sender'); ?></label>
				<input type="text" y-name="sender" class="form-control mb-2" placeholder="" />
				<label><?php view::lang('field.to'); ?></label>
				<div y-name="tos"></div>
				<input type="text" y-name="to" class="form-control mb-2" placeholder="" />
				<label><?php view::lang('field.message'); ?></label>
				<div y-name="fields"></div>
				<textarea type="text" y-name="message" class="form-control mb-2" rows="10"></textarea>
			</div>
		</div>
	</script>

	<script type="text/html" y-name="hook-webhook">
		<div class="card bg-light mb-4" y-name="hook" data-type="webhook">
			<div class="card-body">
				<i class="icon icon-sm float-right clickable" y-name="delete-hook">delete</i>
				<label><?php view::lang('field.webhook'); ?></label>
				<input type="text" y-name="url" class="form-control mb-2" placeholder="" />
			</div>
		</div>
	</script>

	<script type="text/html" y-name="hook-javascript">
		<div class="card bg-light mb-4" y-name="hook" data-type="javascript">
			<div class="card-body">
				<i class="icon icon-sm float-right clickable" y-name="delete-hook">delete</i>
				<label><?php view::lang('field.javascript'); ?></label>
				<textarea type="text" y-name="javascript" class="form-control mb-2" rows="5" />
			</div>
		</div>
	</script>

	<script type="text/html" y-name="hook-preset">
		<div class="card bg-light mb-4" y-name="hook" data-type="preset">
			<div class="card-body">
				<i class="icon icon-sm float-right clickable" y-name="delete-hook">delete</i>
				<label><?php view::lang('field.preset'); ?></label>
				<select y-name="preset" class="form-control mb-2">
					<?php foreach (fetch::config('hooks', []) as $key): ?>
						<option value="<?php view::attr($key) ?>"><?php view::lang('hook.' . $key) ?></option>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
	</script>

	<script type="text/html" y-name="field">
		<div class="btn btn-sm btn-secondary mr-1 mb-1" y-name="field">{{ text }}</div>
	</script>
</div>