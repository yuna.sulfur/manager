<?php
$attributes = function($attributes = [], $class = null) {
	if(!is_array($attributes)){
		$attributes = [];
	}

	if(isset($attributes['class']) && $class !== null){
		$attributes['class'] .= ' '.$class;
	} elseif($class !== null){
		$attributes['class'] = $class;
	}

	if(is_array($attributes)){
		foreach($attributes as $key => $value){
			echo ' ' . fetch::text($key) . '="' . fetch::attr($value) . '"';
		}
	}
};


$addLabels = function($options, $key) {
	$keys = array_keys($options);
	$assoc = array_keys($keys) !== $keys;

	if(! is_array($options)) {
		$options = [];
	} elseif(! $assoc){
		$labeled = [];
		foreach($options as $option){
			$labeled[$option] = fetch::lang('option.' . $key . '.' . $option);
		}
		return $labeled;
	} else {
		return $options;
	}
}
?>

<?php if ($element->type === 'radio'): ?>
	<div
		y-use="manager.form.element.Radio"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->value) ?>"

	>
		<?php foreach ($addLabels($element->options, $element->key) as $option => $label): ?>
			<div class="form-check<?php view::attr($element->inline ? ' form-check-inline' : '') ?>">
				<input class="form-check-input" name="<?php view::attr($element->key); ?>" type="radio" value="<?php view::attr($option) ?>">
				<label class="form-check-label"><?php view::text($label) ?></label>
			</div>
		<?php endforeach; ?>
	</div>
<?php elseif ($element->type === 'checkbox'): ?>
	<div
		y-use="manager.form.element.Checkbox"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr(json_encode($element->value)) ?>"
	>
		<?php foreach ($addLabels($element->options, $element->key) as $option => $label): ?>
			<div class="form-check<?php view::attr($element->inline ? ' form-check-inline' : '') ?>">
				<input class="form-check-input" type="checkbox" value="<?php view::attr($option) ?>">
				<label class="form-check-label"><?php view::text($label) ?></label>
			</div>
		<?php endforeach; ?>
	</div>
<?php elseif ($element->type === 'select'): ?>
	<?php
	$attrs = $element->attributes;
	if($element->multiple){
		$attrs['multiple'] = 'multiple';
	}
	?>
	<select
		y-use="manager.form.element.Select"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->multiple ? json_encode($element->value) : $element->value) ?>"
		data-multiple="<?php view::attr($element->multiple ? 'true' : 'false') ?>"
		<?php $attributes($attrs, 'form-control'); ?>
	>
		<?php foreach ($addLabels($element->options, $element->key) as $option => $label): ?>
			<option value="<?php view::attr($option) ?>"><?php echo str_replace('__nbsp__', '&nbsp', fetch::text($label)) ?></option>
		<?php endforeach; ?>
	</select>
<?php elseif ($element->type === 'text'): ?>
	<input
		y-use="manager.form.element.Text"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->value) ?>"
		type="text"
		<?php $attributes($element->attributes, 'form-control') ?>
	/>
<?php elseif ($element->type === 'password'): ?>
	<input
		y-use="manager.form.element.Text"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->value) ?>"
		type="password"
		<?php $attributes($element->attributes, 'form-control') ?>
	/>
<?php elseif ($element->type === 'textarea'): ?>
	<textarea
		y-use="manager.form.element.Textarea"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->value) ?>"
		rows="4"
		<?php $attributes($element->attributes, 'form-control') ?>
	></textarea>
<?php endif; ?>