<?php view::file('form/element/dynamic', ['element' => ['body', 'textarea', 'label' => false]]) ?>

<?php view::file('form/element/dynamic', ['element' => ['position', 'select', 'options' => [
	'column' => fetch::lang('option.position.column', 'block'),
	'left' => fetch::lang('option.position.left', 'block'),
	'right' => fetch::lang('option.position.right', 'block'),
], 'label' => false]]) ?>

<script type="text/html" y-name="render">
	<quote>
		{% if body %}
			{{{ body }}}
		{% else %}
			 <?php view::lang('label.add_content', 'block') ?>
		{% endif %}
		<br />
		<?php view::file('form/element/block/position'); ?>
	</quote>
</script>