<?php view::file('form/element/dynamic', ['element' => ['image', 'image', 'label' => false]]) ?>
<?php view::file('form/element/dynamic', ['element' => ['title', 'text', 'label' => false]]) ?>
<?php view::file('form/element/dynamic', ['element' => ['body', 'textarea', 'label' => false]]) ?>
<?php view::file('form/element/dynamic', ['element' => ['link', 'link', 'label' => false]]) ?>

<script type="text/html" y-name="render">
	<div>
		{% if title || body || image %}
			{% if image && image.id && image.id != 0 %}
				<img style="width: 320px; max-height: 480px;" alt="{% if image.junction && image.junction.caption %}{{ image.junction.caption }}{% endif %}" src="<?php view::action('image','serve','{{ image.id }}', 'preset=640') ?>" />
			{% endif %}
			<h4>{{ title }}</h4>
			{{{ body }}}
		{% else %}
			 <?php view::lang('label.add_content', 'block') ?>
		{% endif %}
	</div>
</script>