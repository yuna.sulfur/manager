<?php view::file('form/element/dynamic', ['element' => ['title', 'text', 'label' => false]]) ?>
<?php view::file('form/element/dynamic', ['element' => ['file', 'file', 'label' => false]]) ?>

<script type="text/html" y-name="render">
	<div>
		{% if file %}
			{{{ file.file }}}
		{% else %}
			 <?php view::lang('label.add_content', 'block') ?>
		{% endif %}
	</div>
</script>