{%
var lang = {
	full : '<?php view::lang('option.position.full', 'block') ?>',
	column : '<?php view::lang('option.position.column', 'block') ?>',
	left : '<?php view::lang('option.position.left', 'block') ?>',
	right : '<?php view::lang('option.position.right', 'block') ?>',
	outside : '<?php view::lang('option.position.outside', 'block') ?>',
}
%}
{% if position %}
	<br />
	<small>
		{{ lang[position] }}
	</small>
{% endif %}