<?php view::file('form/element/dynamic', ['element' => ['body', 'tinymce_small', 'label' => false]]) ?>

<?php view::file('form/element/dynamic', ['element' => ['position', 'select', 'options' => [
	'left' => fetch::lang('option.position.left', 'block'),
	'right' => fetch::lang('option.position.right', 'block'),
	'column' => fetch::lang('option.position.column', 'block'),
], 'label' => false]]) ?>


<script type="text/html" y-name="render">
	<div>
		{% if body %}
			{{{ body }}}
		{% else %}
			 <?php view::lang('label.add_content', 'block') ?>
		{% endif %}
		<?php view::file('form/element/block/position'); ?>
	</div>
</script>