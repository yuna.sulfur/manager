<?php view::file('form/element/dynamic', ['element' => ['image', 'image', 'junction' => [['caption', 'text'], ['credits', 'text'], ['alt', 'text']], 'label' => false]]) ?>

<?php view::file('form/element/dynamic', ['element' => ['position', 'select', 'options' => [
	'column' => fetch::lang('option.position.column', 'block'),
	'left' => fetch::lang('option.position.left', 'block'),
	'right' => fetch::lang('option.position.right', 'block'),
], 'label' => false]]) ?>

<script type="text/html" y-name="render">

	<div>
		{% if image && image.id && image.id != 0 %}
			<img style="width: 320px; max-height: 480px;" alt="{% if image.junction && image.junction.caption %}{{ image.junction.caption }}{% endif %}" src="<?php view::action('image','serve','{{ image.id }}', 'preset=640') ?>" />
		{% else %}
			 <?php view::lang('label.add_image', 'block') ?>
		{% endif %}
		<?php view::file('form/element/block/position'); ?>
	</div>
</script>