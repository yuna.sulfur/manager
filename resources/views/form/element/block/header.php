<?php view::file('form/element/dynamic', ['element' => ['title', 'text', 'label' => false]]) ?>
<?php if (isset($types)): ?>
	<?php view::file('form/element/dynamic', ['element' => ['type', 'select', 'options' => $types, 'label' => false]]) ?>
<?php endif; ?>

<script type="text/html" y-name="render">
	<div>
		{% if type %}
			<{{ type}}>
		{% endif %}
			{{ title }}
		{% if type %}
			</{{ type}}>
		{% endif %}
	</div>
</script>