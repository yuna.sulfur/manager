<div
	y-use="manager.form.element.Toggle"
	y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr($element->value) ?>"
	class="form-check"
>
	<input class="form-check-input" type="checkbox">
	<label class="form-check-label"><?php $element->label ? view::text($element->label) : view::lang('field.' . $element->key) ?></label>
</div>