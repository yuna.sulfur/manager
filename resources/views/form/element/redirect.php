<div
	y-use="manager.form.element.Redirect"
	y-name="element element-<?php view::attr($element->key) ?> <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
 >

	<div class="card bg-light">
		<div class="card-body">
			<div class="row mb-2">
				<div class="col-5"><?php view::lang('label.redirect_from') ?></div>
				<div class="col-5"><?php view::lang('label.redirect_to') ?></div>
				<div class="col-2"></div>
			</div>
			<div y-name="container"></div>
			<span class="clickable btn btn-primary" y-name="add"><?php view::lang('label.add'); ?></span>
		</div>
	</div>

	<script type="text/html" y-name="redirect">
		<div class="row mb-2" y-name="pair">
			<div class="col-5"><input class="form-control" y-name="from" type="text" /></div>
			<div class="col-5"><input class="form-control" y-name="to" type="text" /></div>
			<div class="col-2"><a class="float-right" y-name="delete" href="#" title="<?php view::lang('label.delete') ?>"><i class="icon">delete</i></span></div>
		</div>
	</script>
</div>