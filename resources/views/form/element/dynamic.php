<?php
$definition = isset($element) && is_array($element) ? $element : [];
if(isset($definition[0]) && isset($definition[1])) {
	$element = new Class($definition) {
		protected $data = [];
		public function __construct($definition = []) {
			$definition['key'] = array_shift($definition);
			$definition['type'] = array_shift($definition);

			$this->data = array_merge([
				'required' => false,
			], $definition);
		}
		public function __get($name) {
			return isset($this->data[$name]) ? $this->data[$name] : null;
		}
	};
	view::file('form/element', ['element' => $element]);
}
?>