<?php
$options = ['default' => fetch::lang('option.' . $element->name . '.default')];
foreach($element->presets as $preset) {
	$options[$preset] = fetch::lang('preset.' . $preset);
}
$options['html'] = fetch::lang('option.' . $element->name . '.html');
?>

<div
	y-use="manager.form.element.Menu"
	y-name="element element-<?php view::attr($element->key) ?>  <?php view::attr($element->id); ?>"
	data-key="<?php view::attr($element->key) ?>"
	data-value="<?php view::attr(json_encode($element->value)) ?>"
>
	<select class="form-control">
		<?php foreach ($options as $option => $label): ?>
			<option value="<?php view::attr($option); ?>"><?php view::text($label); ?></option>
		<?php endforeach; ?>
	</select>
</div>