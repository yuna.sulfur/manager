<?php if ($element->type === 'hidden'): ?>
	<input
		y-use="manager.form.element.Hidden"
		y-name="element element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
		data-key="<?php view::attr($element->key) ?>"
		data-value="<?php view::attr($element->value) ?>"
		type="hidden"
	/>
<?php else: ?>
	<div
		y-name="group"
		class="form-group <?php view::attr($element->required ? ' required' : '') ?> "
	>

		<?php
		if($element->label === false) {
			$label = false;
		} elseif($element->label === '') {
			$label = '&nbsp;';
		} elseif($element->label) {
			 $label = $element->label;
		} else {
			$label = fetch::lang('field.' . $element->key);
		}
		?>


		<?php if ($element->type !== 'submit' && $element->type !== 'toggle' && $label): ?>
			<label><?php view::raw($label) ?></label>
		<?php endif; ?>


		<div class="error" y-name="error" style="display:none;"></div>


		<?php if(in_array($element->type, ['radio', 'checkbox', 'select', 'text', 'password', 'textarea'])): ?>
			<?php view::file('form/element/common', [
				'element' => $element,
			]); ?>

		<?php elseif ($element->type === 'submit'): ?>
			<span
				y-use="manager.form.element.Submit"
				y-name="element-<?php view::attr($element->key); ?> <?php view::attr($element->id); ?>"
				class="btn btn-primary clickable"
			><?php view::text($label) ?></span>
			<input type="submit" style="display:none" />

		<?php else: ?>
			<?php view::file('form/element/' . $element->type, [
				'element' => $element,
			]); ?>

		<?php endif; ?>
	</div>
<?php endif; ?>