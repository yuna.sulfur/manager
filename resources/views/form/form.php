<?php
$wrap = function($elements, $parentType = 'none') use (& $wrap) {
	$config = [
		'column' => 'row',
		'tab' => 'tabs'
	];

	$wrapping = null;

	foreach($elements as $index => $element) {
		if($element->type === 'group') {
			// first, recursively wrap nested elements
			$element->group->elements = $wrap($element->group->elements, $element->group->type);
		}

		if($element->type === 'group' && isset($config[$element->group->type]) && $parentType !== $config[$element->group->type]) {
			// this is a group that should be wrapped, but without a proper wrapper
			// elements should be placed inside a wrapper group
			if($wrapping === null || $wrapping->group->type !== $config[$element->group->type]){
				// there is no active wrapping group
				// or there is an active wrapping group for a different type
				// Create one, with the current element as its first element
				$wrapping = (object) [
					'type' => 'group',
					'group' => (object) [
						'type' => $config[$element->group->type],
						'elements' => [$element]
					]
				];
				// replace the current element with the tabs element
				$elements[$index] = $wrapping;
			} else {
				// already wrapping
				// place current element in the currently wrapping group
				$wrapping->group->elements[] = $element;
				// remove it from it's current position
				unset($elements[$index]);
			}
		} else {
			// this is no wrapping group: unset the wrapper
			$wrapping = null;
		}
	}
	return $elements;
};
?>

<?php
$elements = [];
foreach($form->elements() as $element) {
	$id = '__' . substr(md5(rand(0, 10000000)), 0, 10);
	$element->id = $id;
	$elements[$element->key] = $id;
}
?>

<form
	y-use="manager.Form<?php view::attr($form->attribute('y-use') ? ' '.$form->attribute('y-use') : '') ?>"
	y-name="form"
	action="<?php view::attr($form->attribute('action')); ?>"
	method="<?php view::attr($form->attribute('method')); ?>"
	enctype="<?php view::attr($form->attribute('enctype')); ?>"
	data-submit="<?php view::attr($form->attribute('submit') ? 'true' : 'false'); ?>"
	data-elements="<?php view::attr(json_encode($elements)); ?>"
	<?php if ($class = $form->attribute('class')): ?>
		class="<?php view::attr($class); ?>"
	<?php endif; ?>
>
	<?php view::file('form/group', ['elements' => $wrap($form->layout())]); ?>
</form>