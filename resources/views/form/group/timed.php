<div class="row">
	<div class="col-6">
		<div>
			<?php if (isset($group->elements[0])): ?>
				<?php view::file('form/element', ['element' => $group->elements[0]->element]) ?>
			<?php endif; ?>
		</div>

		<div>
			<?php if (isset($group->elements[2])): ?>
				<?php view::file('form/element', ['element' => $group->elements[2]->element]) ?>
			<?php endif; ?>
		</div>
	</div>

	<div class="col-6">
		<div>
			<?php if (isset($group->elements[1])): ?>
				<?php view::file('form/element', ['element' => $group->elements[1]->element]) ?>
			<?php endif; ?>
		</div>

		<div>
			<?php if (isset($group->elements[3])): ?>
				<?php view::file('form/element', ['element' => $group->elements[3]->element]) ?>
			<?php endif; ?>
		</div>
	</div>
</div>