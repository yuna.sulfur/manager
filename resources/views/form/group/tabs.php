<nav>
	<?php $id = 'tab_' . md5(uniqid()); ?>
	<div class="nav nav-tabs" id="nav-tab" role="tablist">
		<?php foreach($group->elements as $index => $element) : ?>
			<?php if ($element->type === 'group' && $element->group->type == 'tab'): ?>

				<?php if(isset($element->group->label)) {
					$label = $element->group->label;
				} elseif(isset($element->group->name)) {
					$label = fetch::lang('label.' . $element->group->name);
				} else {
					$label = '-';
				}
				?>
				<a class="nav-item nav-link <?php view::attr($index == 0 ? 'active' : '') ?>" data-toggle="tab" href="#<?php view::attr($id . '_' . $index) ?>"><?php view::text($label) ?></a>
			<?php endif; ?>
		<?php endforeach; ?>
	</div>
</nav>

<div class="tab-content mt-4" id="nav-tabContent">
	<?php foreach($group->elements as $index => $element) : ?>
		<?php if ($element->type === 'group' && $element->group->type == 'tab'): ?>
			<div class="tab-pane <?php view::attr($index == 0 ? 'active' : '') ?>" id="<?php view::attr($id . '_' . $index) ?>">
				<?php view::file('form/group', ['elements' => $element->group->elements]); ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
