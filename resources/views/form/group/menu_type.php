<div y-use="manager.form.group.Menu">
	<?php foreach ($group->elements as $element): ?>
		<?php if ($element->element->name == 'type'): ?>
			<?php $element->element->attributes = ['y-name' => 'select']; ?>
			<?php view::file('form/element', ['element' => $element->element]) ?>
		<?php else: ?>
			<div y-name="type type-<?php view::attr($element->element->name) ?>" style="display: none;">
				<?php view::file('form/element', ['element' => $element->element]) ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>