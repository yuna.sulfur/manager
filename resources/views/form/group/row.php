<div class="row">
	<?php foreach($group->elements as $element) : ?>
		<?php if ($element->type === 'group' && $element->group->type == 'column'): ?>
			<div class="col<?php view::attr(isset($element->group->width) ? ('-' . $element->group->width) : '') ?>">
				<?php view::file('form/group', ['elements' => $element->group->elements]); ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>
</div>