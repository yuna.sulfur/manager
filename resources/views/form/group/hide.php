<?php
$hide = [];
foreach($group->hide as $key => $value) {
	$hide[$key] = is_array($value) ? $value : [$value];
}
?>

<div
	y-use="manager.form.Hide"
	data-selector="<?php view::attr($group->selector); ?>"
	data-hide="<?php view::attr(json_encode($hide)); ?>"
>
	<?php view::file('form/group', ['elements' => $group->elements]) ?>
</div>