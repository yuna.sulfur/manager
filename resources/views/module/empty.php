<?php view::extend('template/main'); ?>


<?php view::start('main') ?>
	<div class="content-header" y-name="header-fixed">
		<div class="content-header-1">
			<h2 class="float-left">[title]</h2>
		</div>

		<div class="content-header-2">
			[navigation]
		</div>
	</div>

	<div class="content-main">
		[content]
	</div>

<?php view::end(); ?>