<?php view::extend('template/' . fetch::viewport() ); ?>

<?php view::start('main') ?>
	<div
		y-use="manager.Update"
		data-id="<?php view::attr(isset($id) && $id ? $id : 0) ?>"

		data-title_confirm="<?php view::attr(fetch::lang('dialog.confirm')) ?>"
		data-abandon_title="<?php view::attr(fetch::lang('dialog.abandon.title')) ?>"
		data-abandon_message="<?php view::attr(fetch::lang('dialog.abandon.message')) ?>"
		data-delete_title="<?php view::attr(fetch::lang('dialog.delete.title')) ?>"
		data-delete_message="<?php view::attr(fetch::lang('dialog.delete.message')) ?>"
		data-claim_title="<?php view::attr(fetch::lang('dialog.claim.title')) ?>"
		data-claim_message="<?php view::attr(fetch::lang('dialog.claim.message')) ?>"
		data-claimed_message="<?php view::attr(fetch::lang('dialog.claimed.message')) ?>"
		data-restore_title="<?php view::attr(fetch::lang('dialog.restore.title')) ?>"
		data-restore_message="<?php view::attr(fetch::lang('dialog.restore.message')) ?>"

		data-url_save="<?php view::action(fetch::module(), isset($action) ? $action : 'save', '{{id}}'); ?>"
		data-url_revision="<?php view::action(fetch::module(), fetch::action() == 'create' ? 'update' : fetch::action(), '{{id}}', 'revision={{revision}}'); ?>"
		data-url_delete="<?php view::action(fetch::module(), 'delete', '{{id}}'); ?>"
		data-url_preview="<?php view::attr(isset($preview) ? $preview: '') ?>"
		data-url_publish="<?php view::attr(isset($publish) ? $publish: '') ?>"

		data-url_restore="<?php view::action(fetch::module(), 'update', '{{id}}'); ?>"
		data-url_back="<?php view::action(fetch::module()); ?>"
		data-url_lock="<?php view::action(fetch::module(), 'lock', '{{id}}'); ?>"
		data-url_locked="<?php view::action(fetch::module(), 'locked'); ?>"

		data-callback="<?php view::callback(); ?>"
		data-revisions="<?php view::attr(json_encode(isset($revisions) ? $revisions : [])); ?>"
		data-revision="<?php view::attr(isset($revision) ? $revision : 0); ?>"
		data-lock="<?php view::attr(fetch::config('lock', false) ? 'true' : 'false'); ?>"
		data-status="<?php view::attr(isset($status) ? $status: 'edit') ?>"

		data-module="<?php view::module(); ?>"
		data-csrf="<?php view::csrf(); ?>"

		class="update"
	>
		<script type="text/html" y-name="revisions">
			<div>
				<span class="btn btn-outline-secondary" data-toggle="dropdown">
					<i class="icon icon-sm">history</i>
				</span>
				<div class="dropdown-menu">
					{% each revisions as revision %}
						{% var href = url.replace('__revision__', revision.revision); %}
						{%
						var date = new Date(revision.time * 1000);
						var time = date.toLocaleDateString()+' '+ date.toLocaleTimeString()
						%}
						<a class="dropdown-item{% if current == revision.revision %} active"{% endif %}" href="{{ href }}">{{ time }} <?php view::lang('label.by'); ?> {{ revision.username }}</a>
					{% endeach %}
				</div>
			</div>
		</script>

		<div class="content-header" y-name="header-fixed">
			<div class="content-header-1">
				<div class="row">
					<div class="col">

						<?php if (fetch::task() !== 'update'): ?>
							<span class="btn-group float-left  mr-3">
								<a class="btn btn-outline-secondary " href="<?php view::action(fetch::module()); ?>" y-name="back"><i class="icon icon-sm">chevron_left</i></a>

								<?php if (isset($delete) && $delete): ?>
									<span class="btn btn-outline-secondary" y-name="btn-delete" <?php view::raw(! isset($id) || ! $id ? ' style="display:none"' : '') ?>><i class="icon icon-sm">delete</i></span>
								<?php endif; ?>

								<div class="btn-group" y-name="revisions" role="group"></div>
							</span>

							<h2 class="float-left">
								<span y-name="title-create"<?php view::raw(isset($id) && $id ? ' style="display:none"' : ''); ?>><?php view::lang('title.create') ?></span>
								<span y-name="title-update"<?php view::raw(! isset($id) || ! $id ? ' style="display:none"' : ''); ?>><?php view::lang('title.' . fetch::action()) ?></span>
							</h2>

						<?php endif; ?>
					</div>


					<div class="col">
						<div class="float-right">


							<?php if (fetch::task() === 'select'): ?>
								<a class="btn btn-md btn-default" y-name="btn-select" href="#"<?php view::raw(! isset($id) || ! $id ? ' style="display:none"' : '') ?>><?php view::lang('label.use') ?></a>
							<?php endif; ?>

							<?php if (isset($preview) && $preview): ?>
								<a class="btn btn-info" y-name="btn-preview" href="#"><i class="icon icon-sm">tv</i>&nbsp;&nbsp;<?php view::lang('label.preview') ?></a>
							<?php endif; ?>


							<?php if (isset($publish) && $publish): ?>
								<span class="btn btn-default" y-name="btn-status">
									<span y-name="live" class="text-success" style="<?php view::raw($status == 'edit' ? 'display:none;' : '') ?>">
										<i class="icon">toggle_on</i>&nbsp;<?php view::lang('option.status.live') ?>
									</span>
									<span y-name="edit" style="opacity: 0.5; <?php view::raw($status == 'live' ? 'display:none;' : '') ?>">
										<i class="icon ">toggle_off</i>&nbsp;<?php view::lang('option.status.live') ?>
									</span>
								</span>
							<?php endif; ?>


							<?php if (fetch::task() !== 'update'): ?>
								<a class="btn btn-primary" y-name="btn-save" href="#"><i class="icon icon-sm">save</i>&nbsp;&nbsp;<?php view::lang('label.save') ?></a>
							<?php else: ?>
								<a class="btn btn-primary" y-name="btn-update" href="#"><i class="icon icon-sm">save</i>&nbsp;&nbsp;<?php view::lang('label.saveclose') ?></a>
							<?php endif; ?>

						</div>
					</div>
				</div>
			</div>
		</div>


		<div class="content-main">
			<div class="row">
				<div class="col">
					<?php view::file('form/form', ['form' => $form]); ?>
				</div>
			</div>
		</div>
	</div>
<?php view::end(); ?>