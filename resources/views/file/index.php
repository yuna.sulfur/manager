<?php view::extend('index' ); ?>

<?php view::asset('js', fetch::base().'vendor/dropzone/Dropzone.js'); ?>

<?php view::start('main_before') ?>
	<?php if ($upload): ?>
		<div
			y-use="manager.form.element.Upload"
			data-url="<?php view::attr($upload); ?>"
			class="mb-3"
		>
			<script y-name="zone" type="text/html">
				<div class="upload-area mt-1" id="{{ id }}"><?php view::lang('title.drop'); ?></div>
			</script>
		</div>

	<?php endif; ?>
<?php view::end(); ?>