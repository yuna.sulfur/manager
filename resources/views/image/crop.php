<?php view::extend('template/item'); ?>


<?php view::start('main') ?>
	<?php view::asset('js', fetch::base().'vendor/cropper/Cropper.js'); ?>
	<?php view::asset('css', fetch::base().'vendor/cropper/cropper.css'); ?>

	<div
		y-use="manager.form.Crop"
		data-url="<?php view::attr($url); ?>"
		data-callback="<?php view::callback(); ?>"
	>
		<div class="content-header">
			<div class="content-header-1">
				<span class="btn btn-primary float-right" y-name="submit"><?php view::lang('label.submit'); ?></span>
			</div>
		</div>

		<div class="content-main">

			<div class="row">
				<div class="col-9">
					<img style="width: 100%; max-width: 800px; max-height: 450px;" y-name="image" src="<?php view::attr($image) ?>" />
				</div>
				<div class="col-3">
					<span class="btn btn-primary clickable mb-1 ml-1" y-name="ratio" data-ratio="0"><?php view::lang('label.no_ratio') ?></span><br />
					<?php foreach ($presets as $preset => $info): ?>

						<?php if (isset($info['height']) && isset($info['width'])): ?>
							<span class="btn btn-primary clickable mb-1 ml-1" y-name="ratio" data-ratio="<?php view::attr($info['width'] / $info['height']) ?>">
								<?php view::lang('preset.' . $preset) ?> (<?php view::text($info['width']) ?> &times; <?php view::text($info['height']) ?>)
							</span><br />
						<?php endif; ?>
					<?php endforeach; ?>
				</div>

			</div>
		</div>
	</div>
<?php view::end(); ?>