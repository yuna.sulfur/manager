<?php view::extend('template/' . fetch::viewport() ); ?>

<?php view::asset('js', fetch::base() . 'vendor/jquery/UI.js'); ?>
<?php view::asset('js', fetch::base() . 'vendor/jquery/Nestable.js'); ?>

<?php view::start('main') ?>
	<div
		y-use="manager.Index"
		y-name="index"
		data-url="<?php view::action(fetch::module(), 'items', null, 'count={{count}}&amount={{amount}}&skip={{skip}}&sort={{sort}}&filter={{filter}}&search={{search}}'); ?>"
		data-url_move="<?php view::action(fetch::module(), 'move', '{{id}}', 'after={{after}}&parent={{parent}}'); ?>"
		data-csrf="<?php view::csrf(); ?>"
		data-filter="<?php view::attr(json_encode(fetch::state('filter', []))); ?>"
		data-filters="<?php
		$filters = [];
		foreach(fetch::config('filters') as $name => $options){
			$filters[$name] = [
				'-1' =>  fetch::lang('field.' . $name)
			];
			foreach($options as $option){
				$filters[$name][$option] = fetch::lang(['option' , $name, $option]);
			}
		}
		view::attr(json_encode($filters));
		?>"
		data-sort="<?php view::attr(json_encode(fetch::state('sort', []))); ?>"
		data-search="<?php view::attr(fetch::state('search', '')); ?>"
		data-skip="<?php view::attr(fetch::state('skip', '0')); ?>"
		data-amount="<?php view::attr(fetch::config('pagination', false)); ?>"
		data-sortable="<?php view::attr(fetch::config('sortable', false) ? 'true' : 'false'); ?>"
		data-tree="<?php view::attr(fetch::config('index') == 'tree' ? 'true' : 'false'); ?>"
		data-lock="<?php view::attr(fetch::config('lock', false) ? 'true' : 'false'); ?>"
		data-url_locked="<?php view::action(fetch::module(), 'locked'); ?>"
	>
		<?php view::file('index/filter') ?>

		<?php view::file('index/search') ?>

		<?php view::file('index/' . fetch::config('index', 'table'), ['batch' => $batch]); ?>

		<?php view::file('index/pagination') ?>

		<div class="content-header" y-name="header-fixed">


			<div class="content-header-1">
				<?php view::block('header_before') ?>
				<div class="row ">
					<div class="col-3">
						<h2 class="float-left"><?php view::lang('name'); ?></h2>
						<?php if ($create): ?>
							<a class="btn btn-primary ml-2" href="<?php view::attr($create) ?>"><i class="icon icon-sm">add</i> <?php view::lang('label.create') ?></a>
						<?php endif; ?>
					</div>
					<div class="col-9">
						<div class="float-right w-25" y-name="search"></div>
					</div>
				</div>
			</div>

			<div class="content-header-2">
				<div class="row">
					<?php if($batch): ?>
						<div class="col-3">
							<?php view::file('index/batch', ['batch' => $batch]); ?>
						</div>
					<?php endif; ?>

					<div class="col" y-name="filters"><?php view::block('filters') ?></div>

					<div class="col-2">
						<div class="float-right" y-name="pagination"></div>
					</div>
				</div>
				<?php view::block('header_after') ?>
			</div>
		</div>


		<div class="content-main">
			<?php view::block('main_before') ?>
			<div class="" y-name="list"></div>
			<?php view::block('main_after') ?>
		</div>
	</div>
<?php view::end(); ?>