<?php view::extend('html') ?>

<?php view::start('body') ?>
	<div class="navigation" y-use="manager.Navigation">

		<div class="navigation-column">
			<div class="navigation-inner">
				<?php view::navigation('main'); ?>
			</div>
		</div>


		<div class="navigation-overlay" y-name="overlay"></div>


		<div class="navigation-body">
			<?php view::block('main'); ?>
		</div>
	</div>
<?php view::end(); ?>