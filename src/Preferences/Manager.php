<?php

namespace Sulfur\Manager\Preferences;

use Sulfur\Manager\Manager as BaseManager;
use Sulfur\Data\Finder;

class Manager extends BaseManager
{

	public function index($zone, $module)
	{
		return $this->update($zone, $module);
	}

	protected function zone(Finder $finder, $zone)
	{
		// dont filter zone
	}

	public function update($zone, $module, $id = null)
	{
		return parent::update($zone, $module, $this->identity->id);
	}


	protected function form($name, $zone, $module, $id = null)
	{
		// add dependensies to the form
		return $this->form->get($name, [
			'identity' => $this->identity,
			'finder' => $this->finder(),
			'languages' => $this->config->lang('available'),
		]);
	}

	protected function populate($form, $entity = null)
	{
		parent::populate($form, $entity);
		// unset password in form
		$form->value('password', '');
	}


	protected function hydrate($entity, & $relations, $data, $zone, $module)
	{
		if($data['password'] === '') {
			// dont set an empty password, just leave it as-is
			unset($data['password']);
		} else {
			// has a new password
			$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		}
		parent::hydrate($entity, $relations, $data, $zone, $module);
	}
}