<?php

namespace Sulfur\Manager\Preferences;

use Sulfur\Form\Builder;

class Form extends Builder
{
	public function elements()
	{
		return [
			['username', 'text'],
			['password', 'password'],
			['screenname', 'text'],
			['email', 'text'],
			['lang', 'select', 'options' => $this->languages],
		];
	}


	public function rules()
	{
		return [
			['username', 'required'],
			['username', 'unique', function($username){
				$user = $this->finder
				->clear()
				->where('username', $username)
				->where('id', '<>', $this->identity->id)
				->one();

				if($user) {
					return false;
				} else {
					return true;
				}
			}],
			['email', 'required'],
			['email', 'email'],
			['email', 'unique', function($email){
				$user = $this->finder
				->clear()
				->where('email', $email)
				->where('id', '<>', $this->identity->id)
				->one();
				if($user) {
					return false;
				} else {
					return true;
				}
			}],
			['password', 'password', function($password){
				return $password == '' || strlen($password) >= 6;
			}]
		];
	}


	public function layout()
	{
		return [
			['column', ['username', 'password', 'email', 'screenname']],
			['column', ['lang']],
		];
	}
}