<?php

namespace Sulfur\Manager\Image;

use Sulfur\Manager\File\Manager as FileManager;

use Exception;
use Sulfur\Upload;
use Sulfur\Image;
use Sulfur\Filesystem;
use Sulfur\Data\Entity;
use Sulfur\Manager\Payload;

class Manager extends FileManager
{

	protected $upload;

	protected $image;

	public function __construct(Upload $upload, Image $image)
	{
		$this->upload = $upload;
		$this->image = $image;
	}


	public function embed($zone, $module)
	{
		$form = $this->form->get('Sulfur\Manager\Image\Embed', []);
		return new Payload('image/embed', [
			'form' => $form,
		]);
	}

	public function serve($id)
	{
		if($entity = $this->entity($id)) {
			$this->image->serve($entity->path . $entity->file, $this->request->query('preset', null));
		}
	}


	public function crop($zone, $id)
	{
		$entity = $this->entity($id);
		if($data = $this->request->post()) {

			$success = false;
			if(isset($data['width']) && isset($data['height']) && isset($data['x']) && isset($data['y']) ) {
				$image = $this->image->image($entity->path . $entity->file);
				$image->crop((int) $data['width'], (int) $data['height'], (int) $data['x'], (int) $data['y']);
				$this->image->delete($entity->path . $entity->file);
				$this->image->save($image, $entity->path . $entity->file);

				$entity->width = $data['width'];
				$entity->height = $data['height'];
				$this->data->save($entity);

				$success = true;
			}
			return $this->payload('json', [
				'data' => [
					'success' => $success
				]
			]);
		} else {
			return $this->payload('image/crop', [
				'presets' => $this->config->image('presets'),
				'image' => $this->url->action($zone, 'image', 'serve', $id),
				'url' => $this->url->action($zone, 'image', 'crop', $id)
			]);
		}
	}


	public function valid($file)
	{
		$dimensions = $this->config->get('dimensions.max');
		try {
			// check if image and max dimensions
			$size = getimagesize($file);
			if(is_array($size)) {
				$dimensions = $this->config->get('dimensions.max');
				if($size[0] <=  $dimensions['width'] && $size[1] <=  $dimensions['height']) {
					return true;
				}
			}
			return false;
		} catch (Exception $e) {
			return false;
		}
	}



	public function path($name, Filesystem $filesystem)
	{
		$parts = pathinfo($name);
		$extension = preg_replace('#[^a-zA-Z]#', '',  (isset($parts['extension']) ? $parts['extension'] : ''));
		$name = preg_replace('#[^0-9a-zA-Z-_]#', '', $parts['filename']);
		$clean = ($name == '' ? 'image' : $name) . ($extension == '' ? '' : ('.' . $extension));

		$shard = substr(md5($name), 0, 2);
		$path = trim($this->config->get('path'), '/');
		if(strlen($path) > 0) {
			$path = $path . '/';
		}
		return $path . '/' . $shard . '/' . $clean;
	}


	protected function deleteEntity($entity)
	{
		$this->image->delete($entity->path . $entity->file);
		$this->data->delete($entity);
	}


	protected function item(Entity $entity, $zone, $module)
	{
		$item = parent::item($entity, $zone, $module);

		$item['data']['src'] = $this->url->url($this->config->get('route'), [
			'preset' => $this->config->get('embed'),
			'path' => trim($item['data']['path'], '/'),
			'file' => $item['data']['file']
		]);

		$item['crop'] = $this->croppable($entity, $zone, $module) ? $this->url->action($zone, $module, 'crop', $entity->id, 'callback={{callback}}') : false;
		return $item;
	}


	protected function hydrate($entity, & $relations, $data, $zone, $module)
	{
		// hydrate
		parent::hydrate($entity, $relations, $data, $zone, $module);

		// Only do this when creating
		if(! $entity->id) {
			// create image to do additional checks and changes
			$image = $this->image->image($data['path'] . $data['file']);

			// check and resize max dimensions
			$dimensions = $this->config->get('dimensions.downsize');

			if($image->width() > $dimensions['width'] || $image->height() > $dimensions['height']) {
				// calculate required memory
				// bytes per pixel
				$bpp = 3;
				// safety margin
				$margin = 3;
				$memory = (
					 ($image->width() * $image->height() * $bpp)
					 + ($dimensions['width'] * $dimensions['height'] * $bpp)
				) * $margin / 1024 / 1024;

				ini_set('memory_limit', $memory . 'M' );
				$image->resize((int) $dimensions['width'], (int) $dimensions['height'], function ($constraint) {
					$constraint->upsize();
					$constraint->aspectRatio();
				});
				$this->image->delete($data['path'] . $data['file']);
				$this->image->save($image, $data['path'] . $data['file']);
			}

			// add width and height
			$entity->width = $image->width();
			$entity->height = $image->height();
		}
	}


	protected function croppable($entity, $zone, $module)
	{
		return $this->identity->allowed($module, 'crop', $entity->id)
		|| $this->identity->allowed($zone . '/' . $module, 'crop', $entity->id);
	}
}