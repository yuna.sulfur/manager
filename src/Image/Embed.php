<?php

namespace Sulfur\Manager\Image;

use Sulfur\Form\Builder;

class Embed extends Builder
{

	public function elements()
	{
		return [
			['image', 'image', 'multiple' => false,  'module' => 'image', 'max' => 1],
		];
	}

	public function layout()
	{
		return [
			'image'
		];
	}
}