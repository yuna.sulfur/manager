<?php

namespace Sulfur\Manager\Image;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function elements()
	{
		return [
			['title', 'text'],
		];
	}


	public function rules()
	{
		return [
			['title', 'required']
		];
	}

	public function layout()
	{
		return [
			'title'
		];
	}
}