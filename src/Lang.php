<?php

namespace Sulfur\Manager;

use Sulfur\Config;

class Lang extends Config
{
	protected $lang;

	protected $module;

	protected $replacements = [];


	public function lang($lang = null)
	{
		if($lang === null) {
			return $this->lang;
		} else {
			$this->lang = $lang;
			return $this;
		}
	}




	public function module($module = null)
	{
		if($module === null) {
			return $this->module;
		} else {
			$this->module = $module;
			return $this;
		}
	}


	public function get($key, $module = null)
	{
		if($module === null) {
			if ($this->module === null) {
				throw new Exception('Trying to get language value with key ' . $key . ' but no module is provided');
			} else {
				$module = $this->module;
			}
		}

		if($result = $this->resource($module, $key, false)) {
			// result found in app module
		} elseif($result = $this->resource('manager', is_array($key) ? array_merge([$module], $key) : $module . '.' . $key, false)) {
			// result found vendor module section of manager text
		} elseif($result = $this->resource('manager', $key, false)) {
			// result found in vendor manager base section
		} else {
			// no result
			return '';
		}

		if(! is_string($result)) {
			// bad result
			return '';
		}

		// get replacements
		$replacements = $this->replacements($module);

		// replace values
		$result = str_replace($replacements['find'], $replacements['replace'], $result);

		// uppercase
		return ucfirst($result);
	}


	protected function replacements($module)
	{
		if(isset($this->replacements[$module])) {
			return $this->replacements[$module];
		} else {
			// keys to find
			$keys = ['name', 'article', 'single', 'plural', 'this', 'new'];

			$resources = [
				// first load the default manager module
				$this->resource('manager', $module),
				// then load the app module
				$this->resource($module)
			];

			// go through results and collect the keys
			$replacements = [];
			foreach($resources as $resource) {
				if(is_array($resource)){
					foreach($keys as $key){
						if(isset($resource[$key])) {
							$replacements[':' . $key] = lcfirst($resource[$key]);
						}
					}
				}
			}

			// store ready for use in str_replace
			$this->replacements[$module] = [
				'find' => array_keys($replacements),
				'replace' => array_values($replacements)
			];
			return $this->replacements[$module];
		}
	}


	protected function data($resource)
	{
		$resource = ($this->lang ? $this->lang . '/' : '') . $resource;
		return parent::data($resource);
	}
}