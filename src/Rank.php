<?php

namespace Sulfur\Manager;

use Sulfur\Data;
use Sulfur\Data\Entity;

class Rank
{

	protected $data;


	public function __construct(Data $data)
	{
		$this->data = $data;
	}



	public function update(Entity $entity, $after, $parent = null)
	{
		// check if entity is loaded
		if(! $entity) {
			return false;
		}

		// get the correct database
		$database = $this->data->database($entity::database());

		// get parent
		$parent = is_numeric($parent) ? (int) $parent : false;

		// update the parent id
		if($parent !== false) {
			$database->update($entity::table())
			->set([
				'parent_id' => $parent
			])
			->where('id', $entity->id)
			->execute();
		}

		// select the item set
		$query = $database->select('id')
		->from($entity::table())
		->where('id', '<>', $entity->id)
		->where('zone', '=', $entity->zone)
		->order('rank', 'ASC');
		if($parent !== false) {
			$query->where('parent_id', $parent);
		}
		$items = $query->result();

		// init values
		$placed = false;
		$first = ! $after || $after === '0' || $after === 0;
		$current = 1;
		foreach($items as $item){
			// place as first
			if($first && ! $placed){
				$entity->rank = $current;
				$current++;
				$placed = true;
			}

			// update the sibling
			$database->update($entity::table())
			->set(['rank' => $current])
			->where('id', $item['id'])
			->execute();

			// place item?
			if((int) $item['id'] == $after && ! $placed){
				$current++;
				$entity->rank = $current;
				$placed = true;
			}

			// increment rank
			$current++;
		}

		// not yet placed
		if( ! $placed){
			$entity->rank = $current;
		}

		// save the entity
		$this->data->save($entity);

	}
}