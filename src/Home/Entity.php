<?php
namespace Sulfur\Manager\Home;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'home';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'title' => 'string',
		'zone' => 'string',
	];

	protected static $relations =  [];
}