<?php

namespace Sulfur\Manager\Home;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function elements()
	{
		return [
			['title', 'text']
		];
	}


	public function rules()
	{
		return [

		];
	}

	public function layout()
	{
		return [
			'title'
		];
	}
}