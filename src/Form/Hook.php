<?php

namespace Sulfur\Manager\Form;

use Sulfur\Email;

class Hook
{

	protected $email;

	public function __construct(Email $email)
	{
		$this->email = $email;
	}


	public function execute($values, $hooks = [])
	{
		if(! is_array($hooks)) {
			$hooks = [];
		}

		foreach($hooks as $hook) {
			if(isset($hook['type'])) {
				switch($hook['type']) {
					case 'email' :
						$this->email($values, $hook);
						break;
					case 'webhook' :
						$this->webhook($values, $hook);
						break;
				}
			}
		}
	}


	protected function email($values, $hook)
	{
		if(
			isset($hook['from'])
			&& filter_var($hook['from'], FILTER_VALIDATE_EMAIL)
			&& isset($hook['to'])
		) {
			$tos = [];

			foreach(preg_split( "#[\s\;\,]+#", $hook['to'] ) as $to) {
				$to = trim($to);
				if(preg_match('#^\[([a-zA-Z0-9\-\_]+)\]$#', $to, $matches)) {
					$to = isset($values[$matches[1]]) ? $values[$matches[1]] : '';
				}
				if(filter_var($to, FILTER_VALIDATE_EMAIL)) {
					$tos[] = $to;
				}
			}

			if(count($tos) == 0) {
				return;
			}

			$body = isset($hook['message']) ? $hook['message'] : '';
			$body = preg_replace_callback('#\[([a-zA-Z0-9\-\_]+)\]#', function($matches) use ($values){
				if(isset($values[$matches[1]])) {
					$value = $values[$matches[1]];
					if(is_array($value)) {
						$value = implode(', ', $value);
					}
					return $value;
				} else {
					return $matches[0];
				}
			}, $body);


			$message = $this->email->message(
				isset($hook['subject']) &&  $hook['subject'] ? $hook['subject'] : 'No Subject',
				$tos,
				isset($hook['sender']) &&  $hook['sender'] ? [$hook['from'] => $hook['sender']] : [$hook['from'] => $hook['from']],
				$body
			)->send();
		}
	}


	protected function webhook($values, $hook)
	{
		if(isset($hook['url'])) {
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $hook['url']);
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($values));
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($curl);
			$error = curl_error($curl);
			curl_close ($curl);
		}
	}
}