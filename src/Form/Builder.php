<?php

namespace Sulfur\Manager\Form;

use Sulfur\Form\Builder as BaseBuilder;

class Builder extends BaseBuilder
{

	protected $parsed = null;

	protected function parsed($resource) {

		if($this->parsed === null) {

			$keys = [];

			$elements = [];
			$rules = [];
			$layout = [];

			foreach($this->elements as $row)  {
				$line = [];
				foreach($row as $element) {
					// get key
					$key = $element['name'];

					// get correct type
					if($element['type'] == 'email') {
						$type = 'text';
					} else {
						$type = $element['type'];
					}

					$options = [];
					if($element['type'] == 'radio' || $element['type'] == 'checkbox') {
						foreach($element['options'] as $option) {
							$options[$option] = $option;
						}
					}

					if($element['required'] === 1 || $element['required'] === '1' || $element['required'] === 'true' || $element['required'] === true) {
						$required = true;
						$rules[] = [$key, 'required'];
						if($element['type'] == 'email') {
							$rules[] = [$key, 'email'];
						}
					} else {
						$required = false;
					}

					$elements[] = [$key, $type, 'label' => $element['label'], 'options' => $options, 'required' => $required, 'message' => $element['error'], 'info' => $element['info']];
					$line[] = $key;

				}
				$layout[] = ['line', $line];
			}

			$layout[] = ['line', [
				['submit', 'label' => $this->submit]
			]];

			$this->parsed = [
				'elements' => $elements,
				'rules' => $rules,
				'layout' => $layout,
			];
		}

		if(isset($this->parsed[$resource])) {
			return $this->parsed[$resource];
		}
	}


	public function elements()
	{
		return $this->parsed('elements');
	}

	public function rules()
	{
		return $this->parsed('rules');
	}

	public function layout()
	{
		return $this->parsed('layout');
	}

	public function attributes()
	{
		return [
			'action' => $this->action ?: ''
		];
	}
}