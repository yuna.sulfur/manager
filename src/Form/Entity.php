<?php

namespace Sulfur\Manager\Form;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'form';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'status' => ['enum', 'values' => ['edit', 'review' ,'live']],
		'title' => 'string',
		'submit' => 'string',
		'success' => 'text',
		'elements' => 'json',
		'hooks' => 'json',
		'user_id' => 'int',
		'zone' => ['string', 'index' => true],
	];
}