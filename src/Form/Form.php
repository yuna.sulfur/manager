<?php

namespace Sulfur\Manager\Form;

use Sulfur\Form\Builder;

class Form extends Builder
{
	public function elements()
	{
		return [
			['title', 'text'],
			['submit', 'text'],
			['success', 'textarea'],
			['form', 'form', 'keys' => ['elements', 'hooks']],
		];
	}


	public function rules()
	{
		return [

		];
	}


	public function layout()
	{
		return [
			['column', ['title', 'submit']],
			['column', ['success']],
			'form'
		];
	}
}