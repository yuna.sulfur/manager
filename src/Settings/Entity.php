<?php
namespace Sulfur\Manager\Settings;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'settings';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'head' => 'text',
		'body_before' => 'text',
		'body_after' => 'text',
		'redirect' => 'json',
		'zone' => 'string',
	];

	protected static $relations =  [];
}