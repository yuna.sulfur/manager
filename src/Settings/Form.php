<?php

namespace Sulfur\Manager\Settings;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function elements()
	{
		return [
			['head', 'textarea'],
			['body_before', 'textarea'],
			['body_after', 'textarea'],
			['redirect', 'redirect']
		];
	}


	public function rules()
	{
		return [

		];
	}

	public function layout()
	{
		return [
			'head',
			'body_before',
			'body_after',
			'redirect',
		];
	}
}