<?php

namespace Sulfur\Manager\History;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'history';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'entity' => 'string',
		'entity_id' => 'int',
		'user_id' => 'int',
		'revision' => 'int',
		'data' => 'json',
	];

	protected static $relations = [
		'user' => ['Sulfur\Manager\User\Entity', 'belongs']
	];
}