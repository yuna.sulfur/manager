<?php

namespace Sulfur\Manager;

use Sulfur\Request;
use Sulfur\Response;
use Sulfur\Manager\Bootstrap;
use Sulfur\Manager\Auth\Access;
use Sulfur\Url;
use Sulfur\Manager\Config as Config;
use Sulfur\Container;
use Sulfur\View;
use Sulfur\Manager\State;
use Sulfur\Data;

class Controller
{
	public function action(
		Request $request,
		Response $response,
		Bootstrap $bootstrap,
		Access $access,
		Url $url,
		Config $config,
		Container $container,
		View $view,
		State $state,
		Data $data
	)
	{
		// Get vars from request
		$zone = $request->get('zone');
		$module = $request->get('module');
		$action = $request->get('action', 'index');
		$id = $request->get('id');

		// get vars from qs
		$filter = $this->argument($request->query('filter', ''));
		$sort = $this->argument($request->query('sort', ''));
		$skip = $request->query('skip', null);
		$amount = $request->query('amount', null);
		$search = $request->query('search', null);
		$count = $request->query('count', null) === '1';
		$status = $request->query('status', null);
		$parent = $request->query('parent', null);
		$after = $request->query('after', null);
		$viewport = $request->query('viewport', null);
		$task = $request->query('task', null);
		$callback = $request->query('callback', null);

		// Bootstrap the code
		$bootstrap($module, $zone);

		// Get entity for ownership
		if($id && $class = $config->get('entity')) {
			$entity = $data->finder($class)->one($id);
		} else {
			$entity = null;
		}

		// Check access to the request
		if($access($request, $entity) === false) {
			if($access->status() === Access::NOT_LOGGED_IN) {
				// redirect to login page
				$response->redirect($url->route('login') . '?redirect=' . urlencode($url->current(true)) );
			} else {
				// just stop
				$response->status(401);
				return $access->message();
			}
		}

		// Get the manager class
		$manager = $container->get($config->get('manager'));

		// Provision default dependencies
		// dont use the constructor, so extensions can use the constructor
		if (is_callable([$manager, 'provision'])) {
			$container->call( [$manager, 'provision'], [], $module . '@provision');
		}

		// Call the action function on the manager class
		if (is_callable([$manager, $action])) {
			$payload = $container->call( [$manager, $action], [
				':zone' => $zone,
				':module' => $module,
				':action' => $action,
				':id' => $id,
				':filter' => $filter,
				':sort' => $sort,
				':skip' => $skip,
				':amount' => $amount,
				':search' => $search,
				':count' => $count,
				':status' => $status,
				':parent' => $parent,
				':after' => $after,
				':viewport' => $viewport,
				':task' => $task,
				':callback' => $callback,
			], $module . '@' . $action);

			// Render given view with given data
			return $view->render($payload->view(), $payload->data());
		} else {
			// just stop
			$response->status(404);
			return 'Invalid action: ' . $action;
		}
	}


	protected function argument($string)
	{
		$vars = [];
		$arguments = explode(';', $string);
		foreach($arguments as $argument){
			$pair = explode('=', $argument);
			if(isset($pair[1])){
				$var = $pair[0];
				$val = $pair[1];
				if(substr($var, -2) === '[]'){
					$var = substr($var, 0, -2);
					$val = explode(',', $val);
				}
				$vars[$var] = $val;
			}
		}
		return $vars;
	}
}