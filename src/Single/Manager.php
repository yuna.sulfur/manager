<?php

namespace Sulfur\Manager\Single;

use Sulfur\Manager\Manager as BaseManager;


class Manager extends BaseManager
{


	public function index($zone, $module)
	{
		return $this->update($zone, $module);
	}


	public function update($zone, $module, $id = null)
	{
		$entity = $this->finder()
		->only('id')
		->where('zone', $zone)
		->one();

		if($entity) {
			return parent::update($zone, $module, $entity->id);
		} else {
			return parent::update($zone, $module);
		}
	}
}