<?php
namespace Sulfur\Manager\User;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'user';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'username' => 'string',
		'password' => 'string',
		'screenname' => 'string',
		'email' => 'string',
		'roles' => 'json',
		'ips' => 'json',
		'lang' => 'string',
		'failed' => 'json',
	];

	protected static $relations =  [];
}