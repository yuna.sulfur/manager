<?php

namespace Sulfur\Manager\User;

use Sulfur\Manager\Manager as BaseManager;
use Sulfur\Data\Finder;
use Sulfur\Manager\Payload;

class Manager extends BaseManager
{



	protected function zone(Finder $finder, $zone)
	{
		// dont filter zone
	}



	protected function form($name, $zone, $module, $id = null)
	{
		// add dependensies to the form
		return $this->form->get($name, [
			'id' => $id,
			'finder' => $this->finder(),
			'roles' => $this->identity->roles()
		]);
	}


	protected function populate($form, $entity = null)
	{
		parent::populate($form, $entity);
		// unset password in form
		$form->value('password', '');
	}


	protected function hydrate($entity, & $relations, $data, $zone, $module)
	{
		if($data['password'] === '') {
			// dont set an empty password, just leave it as-is
			unset($data['password']);
		} else {
			// has a new password
			$data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
		}
		parent::hydrate($entity, $relations, $data, $zone, $module);
	}
}