<?php

namespace Sulfur\Manager\User;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function elements()
	{
		return [
			['username', 'text'],
			['password', 'password'],
			['screenname', 'text'],
			['email', 'text'],
			['roles', 'checkbox', 'options' => $this->roles]
		];
	}


	public function rules()
	{
		return [
			['username', 'required'],
			['username', 'unique', function($username){
				$user = $this->finder
				->clear()
				->where('username', $username)
				->where('id', '<>', $this->id)
				->one();

				if($user) {
					return false;
				} else {
					return true;
				}
			}],
			['email', 'required'],
			['email', 'email'],
			['email', 'unique', function($email){
				$user = $this->finder
				->clear()
				->where('email', $email)
				->where('id', '<>', $this->id)
				->one();
				if($user) {
					return false;
				} else {
					return true;
				}
			}],
			['password','password', function($password){
				if($this->id && $password == '') {
					return true;
				} elseif(strlen($password) < 6) {
					return false;
				} else {
					return true;
				}
			}]
		];
	}


	public function layout()
	{
		return [
			['column', ['username', 'password', 'email', 'screenname']],
			['column', ['roles']]
		];
	}
}