<?php

namespace Sulfur\Manager\Tag;

use Sulfur\Manager\Manager as BaseManager;
use Sulfur\Data\Finder;

class Manager extends BaseManager
{


	public function save($zone, $module, $id = null)
	{
		$success = false;
		$message = $this->lang->get('message.invalid');
		$errors = ['title' => $this->lang->get('error.title.required')];
		if($title = $this->request->post('title')) {
			// get entity
			$entity = $this->entity($id);
			// hydrate title
			$entity->title = trim($title);
			// hydrate slug
			$entity->slug = $this->slug->clean($entity->title);
			if(! $id) {
				// new tag: try to find existing tag with the same slug
				$tag = $this->finder()
				->where('slug', '=', $entity->slug)
				->where('zone', $zone)
				->one();

				if($tag) {
					// found one: use that
					$entity = $tag;
				} else {
					// new tag: save
					$entity->created = date('Y-m-d H:i:s');
					$entity->zone = $zone;
					$entity->owner_id = $this->identity->id;
					$this->data->save($entity);
				}
				// use this id in the response
				$id = $entity->id;
			} else {
				// NB: in this scenario double titles can exist
				$this->data->save($entity);
			}
			$success = true;
			$message =  $this->lang->get('message.' . ($id ? 'updated' : 'created'));
			$errors = [];
		}


		return $this->payload('json', [
			'data' => [
				'id' => $id,
				'success' => $success,
				'message' => $message,
				'errors' => []
			]
		]);
	}


	protected function sort(Finder $finder, $sort)
	{
		if(is_array($sort) && isset($sort['popular'])) {
			$finder->order('created', 'desc');
			// todo: get popular tags from relations
		} else {
			parent::sort($finder, $sort);
		}
	}
}