<?php

namespace Sulfur\Manager\Tag;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'tag';

	protected static $columns = [
		'id' => 'int',
		'slug' => 'string',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'title' => 'string',
		'user_id' => 'int',
		'zone' => ['string', 'index' => true],
	];
}