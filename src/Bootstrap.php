<?php

namespace Sulfur\Manager;

use Sulfur\Container;
use Sulfur\Manager\State;
use Sulfur\Manager\Config;
use Sulfur\Manager\Lang;
use Sulfur\Identity;
use Sulfur\Manager\Helper;

class Bootstrap
{
	protected $container;
	protected $state;
	protected $config;
	protected $lang;
	protected $identity;
	protected $helper;


	public function __construct(
		Container $container,
		State $state,
		Config $config,
		Lang $lang,
		Identity $identity,
		Helper $helper
	)
	{
		$this->container = $container;
		$this->state = $state;
		$this->config = $config;
		$this->lang = $lang;
		$this->identity = $identity;
		$this->helper = $helper;
	}


	public function __invoke($module, $zone = null)
	{
		// Set state zone / module
		if($zone) {
			$this->state->zone($zone);
		}
		$this->state->module($module);


		// Set config zone / module
		if($zone) {
			$this->config->zone($zone);
		}
		$this->config->module($module);


		// Set lang module
		$this->lang->module($module);

		// get language from identity user
		$lang = null;
		if($this->identity->confirmed()) {
			$lang = $this->identity->user->lang;
		}

		// set language, fallback to manager config
		$this->lang->lang($lang ? $lang : $this->config->lang('default'));


		// Register helpers
		$this->helper->register($module, $zone);
	}
}