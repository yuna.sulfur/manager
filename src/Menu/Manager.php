<?php

namespace Sulfur\Manager\Menu;

use Sulfur\Manager\Manager as BaseManager;

use Sulfur\Manager\Tree;

class Manager extends BaseManager
{
	protected function form($name, $zone, $module, $id = null)
	{
		$tree = new Tree(
			$this->finder()
			->where('zone', $zone)
			->where('id', '<>', $id)
			->order('rank', 'ASC')
			->all()
		);

		$parents = ['00' => $this->lang->get('option.parent_id.root')];
		foreach($tree->index() as $entity) {
			$prefix = str_repeat('__nbsp____nbsp____nbsp__', $entity->depth) . '__nbsp____nbsp__Ⱶ';
			$parents[$entity->id] = $prefix . $entity->title;
		}

		$form = $this->form->get($name, [
			'parents' => $parents,
			'presets' => $this->config->get('presets', [])
		]);

		if($parentId = $this->request->query('parent_id', false)) {
			$form->value('parent_id', $parentId);
		}
		return $form;
	}


	protected function deleteEntity($entity)
	{
		$this->deleteChildren($entity);
		$this->data->delete($entity);
	}

}