<?php

namespace Sulfur\Manager\Menu;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function attributes()
	{
		return ['y-use' => 'manager.form.Menu'];
	}

	public function elements()
	{
		return [
			['title', 'text'],
			['parent_id', 'select', 'options' => $this->parents],
			['type', 'menu', 'presets' => $this->presets],
			['link', 'link'],
			['preset', 'select'],
			['html', 'textarea'],
		];
	}

	public function layout()
	{
		return [
			['row', [
				['column', ['type', 'parent_id']],
				['column', ['title', 'link', 'html']]
			]]
		];
	}
}