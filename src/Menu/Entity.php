<?php

namespace Sulfur\Manager\Menu;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'menu';

	protected static $columns = [
		'id' => 'int',
		'tag' => ['string', 'index' => true],
		'rank' => 'int',
		'parent_id' => 'int',
		'created' => 'datetime',
		'status' => ['enum', 'values' => ['edit', 'review' ,'live','deleted'], 'default' => 'live', 'index' => true],
		'timestamp' => 'timestamp',
		'deletable' => ['boolean', 'default' => 1],
		'movable' =>  ['boolean', 'default' => 1],
		'updatable' => ['boolean', 'default' => 1],
		'title' => 'string',
		'type' => 'string',
		'link' => 'json',
		'html' => 'text',
		'user_id' => 'int',
		'zone' => ['string', 'index' => true],
	];
}