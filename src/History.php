<?php

namespace Sulfur\Manager;

use Sulfur\Data;

class History
{

	protected $data;

	protected $config = [
		'entity' => 'Sulfur\Manager\History\Entity',
		'keep' => 5
	];


	public function __construct(Data $data, $config = [])
	{
		$this->data = $data;
		$this->config = array_merge($this->config, $config);
	}


	public function revisions($entity)
	{
		$revisions = [];
		foreach(
			$this->data->finder($this->config['entity'])
			->with('user')
			->where('entity', '=', get_class($entity))
			->where('entity_id', '=', $entity->id)
			->order('revision', 'DESC')
			->all() as $revision
		) {

			$revisions[] = [
				'revision' => $revision->revision,
				'username' => $revision->user ? $revision->user->username : '',
				'time' => strtotime($revision->created)
			];
		}
		return $revisions;
	}



	public function revision($entity, $revision, $with = [])
	{
		$revision = $this->data->finder($this->config['entity'])
		->where('entity', '=', get_class($entity))
		->where('entity_id', '=', $entity->id)
		->where('revision', '=', $revision)
		->one();

		if($revision) {
			return $this->data->hydrate(get_class($entity), $revision->data, $with);
		} else {
			return false;
		}
	}


	public function commit($entity, $values, $userId = 0)
	{
		$revisions = $this->data->finder($this->config['entity'])
		->where('entity', '=', get_class($entity))
		->where('entity_id', '=', $entity->id)
		->order('revision', 'ASC')
		->all(true);

		$revision = count($revisions) > 0 ? $revisions[count($revisions) - 1]['revision'] + 1 : 1;

		$data = array_merge($entity->data(), $values);

		$this->data->create($this->config['entity'], [
			'created' => date('Y-m-d H:i:s'),
			'revision' => $revision,
			'entity' => get_class($entity),
			'entity_id' => $entity->id,
			'user_id' => $userId,
			'data' => $data
		]);

		$delete = count($revisions) + 1 - $this->config['keep'];

		if($delete > 0) {
			for($i = 0; $i < $delete; $i++) {
				$this->data->delete($revisions[$i]);
			}
		}
	}
}