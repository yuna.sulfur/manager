<?php

namespace Sulfur\Manager;

class Payload
{

	protected $view;
	protected $data = [];

	public function __construct($view = null, array $data = [])
	{
		$this->view = $view;
		$this->data = $data;
	}


	public function data($dataOrKey = null, $value = null)
	{
		if($dataOrKey === null) {
			return $this->data;
		} elseif(is_array($dataOrKey)) {
			$this->data = array_merge($this->data, $dataOrKey);
		} elseif(is_string($dataOrKey) && $value === null ) {
			if(isset($this->data[$dataOrKey])) {
				return $this->data[$dataOrKey];
			} else {
				return null;
			}
		} elseif(is_string($dataOrKey)) {
			$this->data[$dataOrKey] = $value;
		}
		return $this;
	}


	public function view($view = null)
	{
		if($view === null) {
			return $this->view;
		} else {
			$this->view = $view;
			return $this;
		}
	}
}