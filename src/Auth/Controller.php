<?php

namespace Sulfur\Manager\Auth;

use Sulfur\Manager\Bootstrap;
use Sulfur\Request;
use Sulfur\Response;
use Sulfur\Form;
use Sulfur\Identity;
use Sulfur\View;
use Sulfur\Url;
use Sulfur\Manager\Config;
use Sulfur\Csrf;

class Controller
{
	public function login(
		Bootstrap $bootstrap,
		Request $request,
		Response $response,
		Form $form,
		Identity $identity,
		View $view,
		Url $url,
		Config $config,
		Csrf $csrf
	)
	{
		$bootstrap('auth');

		// create form
		$form = $form->get($config->get('form'));

		// not yet failed
		$error = false;


		if ($form->valid()) {

			if ($identity->confirm($form->value('username'), $form->value('password'))) {

				// regenerate csrf
				$csrf->token(true);

				// redirect url given
				if ($redirect = $request->query('redirect', false)) {
					$response->redirect($redirect, 303);
				} else {
					$response->redirect($url->route('start'), 303);
				}
			} else {
				$error = $identity->error();
				$response->status(401);
			}
		}

		return $view->render('auth/login', [
			'form' => $form,
			'error' => $error
		]);
	}


	public function keepalive(Identity $identity, Csrf $csrf)
	{
		$identity->keepalive();
		$csrf->keepalive();
	}


	public function logout(Identity $identity, Csrf $csrf, Response $response, Url $url)
	{
		$identity->destroy();
		$csrf->destroy();
		$response->redirect($url->route('login'), 303);
	}
}