<?php

namespace Sulfur\Manager\Auth;

use Sulfur\Request;
use Sulfur\Identity;
use Sulfur\Csrf;
use Sulfur\Manager\Config;
use Sulfur\Data\Entity;

class Access
{
	const NOT_LOGGED_IN = 1;
	const NOT_ALLOWED = 2;
	const CSRF_FAILED = 3;

	protected $identity = null;

	protected $csrf = null;

	protected $config = null;

	protected $status = 200;

	protected $message = '';

	public function __construct(Identity $identity, Csrf $csrf, Config $config)
	{
		$this->identity = $identity;
		$this->csrf = $csrf;
		$this->config = $config;
	}

	public function __invoke(Request $request, Entity $entity = null)
	{
		return $this->check($request, $entity);
	}

	public function check(Request $request, Entity $entity = null)
	{
		// get zone
		$zone = $request->get('zone');
		// get module
		$module = $request->get('module');
		// get action
		$action = $request->get('action');

		// check if logged in
		if ($this->identity->confirmed() == false){
			$this->status = self::NOT_LOGGED_IN;
			$this->message = 'User not logged in';
			return false;
		}

		// check if ip whitelisted
		if ( ! $this->identity->whitelisted($request->ip())){
			$this->status = self::NOT_ALLOWED;
			$this->message = 'Manager not accessible';
			return false;
		}

		// check if access to manager
		if ( ! $this->identity->allowed('manager', 'access')){
			$this->status = self::NOT_ALLOWED;
			$this->message = 'Manager not accessible';
			return false;
		}

		// check if access to zone
		if ( ! $this->identity->allowed($zone . '/zone', 'access')){
			$this->status = self::NOT_ALLOWED;
			$this->message = 'Zone "' . $zone . '" not accessible';
			return false;
		}

		// check if access to module
		if (
			! $this->identity->allowed($module, 'access')
			&& ! $this->identity->allowed($zone . '/' . $module, 'access')
		) {
			$this->status = self::NOT_ALLOWED;
			$this->message = 'Module "' . $module . '" not accessible in zone "' . $zone . '"';
			return false;
		}

		$ownerId = is_object($entity) && isset($entity->user_id) ? $entity->user_id : null;


		if($action == 'save' && ! is_object($entity) ) {
			// saving a new entity. Check for 'create' instead
			$action = 'create';
		}

		if (
			! $this->identity->allowed($module, $action, $ownerId)
			&& ! $this->identity->allowed($zone . '/' . $module, $action, $ownerId)
		){
			$this->status = $this::NOT_ALLOWED;
			$this->message = 'Action "' . $action . '" not allowed on module "' . $module . '" in zone "' . $zone . '"';
			return false;
		}
		
		// check csrf
		if(in_array(strtolower($action), $this->config->csrf())) {
			if (! $this->csrf->validate($request->post('csrf', false))) {
				$this->status = $this::CSRF_FAILED;
				$this->message = 'CSRF failed';
				return false;
			}
		}

		// we're good
		return true;
	}


	public function status()
	{
		return $this->status;
	}

	public function message()
	{
		return $this->message;
	}


	public function keepalive()
	{
		// keep sessions alive
		$this->identity->keepalive();
		$this->csrf->keepalive();
	}
}

