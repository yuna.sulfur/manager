<?php

namespace Sulfur\Manager\Auth;

use Sulfur\Form\Builder;

class Form extends Builder
{
	public function attributes()
	{
		return ['submit' => true];
	}


	public function elements()
	{
		return [
			['username', 'text'],
			['password', 'password'],
			['login', 'submit'],
		];
	}

	public function rules()
	{
		return [
			['username', 'required'],
			['password', 'required']
		];
	}
}