<?php

namespace Sulfur\Manager;

use Sulfur\Router;
use Sulfur\Url as BaseUrl;
use Sulfur\Request;
use Sulfur\Config;
use Sulfur\Container;


class Url
{

	protected $url;

	protected $config;

	protected $request;

	protected $builder = null;


	public function __construct(BaseUrl $url, Config $config, Request $request, Container $container)
	{
		$this->url = $url;
		$this->config = $config;
		$this->request = $request;
		$this->container = $container;
	}


	public function base()
	{
		return $this->url->base();
	}


	public function action($zone, $module, $action, $id = null, $query = null)
	{
		$url = $this->url->route('action', ['zone' => $zone, 'module' => $module, 'action' => $action, 'id' => $id]);

		$separator = strpos($url, '?') === false ? '?' : '&';

		if(is_string($query) ) {
			$url .= $separator . $query;
			$separator = '&';
		} elseif (is_array($query)) {
			$qs = [];
			foreach($query as $key => $val) {
				$qs[] = $key. '=' . $val;
			}
			$url .= $separator . implode('&', $qs);
			$separator = '&';
		}

		if(strpos($url, 'viewport=') === false && $viewport = $this->request->query('viewport', false)) {
			$url .= $separator . 'viewport=' . $viewport;
			$separator = '&';
		}

		if(strpos($url, 'task=') === false && $task = $this->request->query('task', false)) {
			$url .= $separator . 'task=' . $task;
			$separator = '&';
		}

		if(strpos($url, 'callback=') === false && $callback = $this->request->query('callback', false)) {
			$url .= $separator . 'callback=' . $callback;
			$separator = '&';
		}

		return $url;
	}


	public function route($route, $params = [])
	{
		return $this->url->route($route, $params);
	}

	public function current($qs = true)
	{
		return $this->url->current($qs);
	}


	public function url($route, $params = [], $parts = [])
	{
		// make sure there is a zone set, take it from the request if not
		$params['zone'] = isset($params['zone']) ? $params['zone'] : $this->request->get('zone');

		// create builder for the frontend url
		$url = $this->container->make('Sulfur\Url',[
			':router' => $this->container->make('Sulfur\Router', [':routes' => $this->config->__invoke('app:routes')] ),
			':request' => $this->container->make('Sulfur\Request'),
		]);
	
		// if there is a configured builder: use that and provision url with this url
		if($builder = $this->config->url('builder')) {
			$url = $this->container->get($builder, [':url' => $url]);
		}

		return $url->route($route, $params, $parts);
	}
}