<?php

namespace Sulfur\Manager;

use Sulfur\Session;

class State
{

	protected $session;
	protected $zone = '';
	protected $module = '';

	public function __construct(Session $session)
	{
		$this->session = $session;
	}


	public function zone($zone)
	{
		$this->zone = $zone;
	}


	public function module($module)
	{
		$this->module = $module;
	}


	public function get($name, $default = null)
	{
		$prefix = $this->zone . '_' . $this->module . '_';
		return $this->session->get($prefix . $name, $default);
	}


	public function set($name, $value)
	{
		$prefix = $this->zone . '_' . $this->module . '_';
		return $this->session->set($prefix . $name, $value);
	}
}
