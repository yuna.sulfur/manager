<?php

namespace Sulfur\Manager\Link;

use Sulfur\Form\Builder;

class Form extends Builder
{

	public function elements()
	{
		return [
			['url', 'url', 'presets' => $this->presets, 'modules' => $this->modules],
			['title', 'text'],
			['blank', 'toggle', 'val' => '1', 'default' => '0'],
			$this->nofollow ? ['nofollow', 'toggle', 'val' => '1', 'default' => '0'] : null,
		];
	}


	public function rules()
	{
		return [];
	}


	public function layout()
	{
		return [
			['row', [
				['column', ['url']],
				['column', ['title', 'blank', $this->nofollow ? 'nofollow' : null]]
			]]
		];
	}
}