<?php

namespace Sulfur\Manager\Link;


use Sulfur\Manager\Manager as BaseManager;
use Sulfur\Manager\Payload;

class Manager extends BaseManager
{

	public function update($zone, $module, $id = null)
	{
		$form = $this->form->get($this->config->get('form'), [
			'presets' => $this->config->get('presets'),
			'modules' => $this->config->get('modules'),
			'nofollow' => $this->config->get('nofollow'),
		]);

		$form->values($this->request->post());

		return new Payload('link/update', [
			'form' => $form,
		]);
	}


	public function build()
	{
		if($route = $this->request->post('route')) {
			// theres is a route, use that
		} elseif($module = $this->request->post('module')) {
			// try to get a route from the module config
			$route = $this->config->resource('module/' . $module, 'route', false);
		} else {
			// no route found
			$route = false;
		}


		if($route) {
			$info = json_decode($info = $this->request->post('info'), true);
			$info = is_array($info) ? $info : [];

			$params = isset($info['params']) && is_array($info['params']) ? $info['params'] : [];
			$parts = isset($info['parts']) && is_array($info['parts']) ? $info['parts'] : [];

			if(! isset($params['zone'])) {
				$params['zone'] = $this->request->get('zone');
			}

			if(! isset($params['module'])) {
				$params['module'] = $this->request->post('module', '');
			}

			$url = $this->url->url($route, $params, $parts);
		} else {
			$url = '';
		}

		return new Payload('json', ['data' => ['url' => $url]]);
	}
}