<?php

namespace Sulfur\Manager;

use Sulfur\Manager\Payload;
use Sulfur\Data\Entity;
use Sulfur\Data\Finder;

use Sulfur\Request;
use Sulfur\Identity;
use Sulfur\Manager\State;
use Sulfur\Data;
use Sulfur\Manager\Config;
use Sulfur\Manager\Url;
use Sulfur\Manager\Rank;
use Sulfur\Form;
use Sulfur\Manager\Lang;
use Sulfur\Manager\Slug;
use Sulfur\Manager\History;

class Manager
{

	/**
	 * Incoming request
	 * @var Sulfur\Request
	 */
	protected $request;

	/**
	 * Current Identity
	 * @var Sulfur\Identity
	 */
	protected $identity;

	/**
	 * Current State
	 * @var Sulfur\Manager\State
	 */
	protected $state;

	/**
	 * Data layer
	 * @var Sulfur\Data
	 */
	protected $data;

	/**
	 * Loaded config
	 * @var Sulfur\Manager\Config
	 */
	protected $config;

	/**
	 * Url helper
	 * @var Sulfur\Manager\Url
	 */
	protected $url;

	/**
	 * Rank helper
	 * @var Sulfur\Manager\Rank
	 */
	protected $rank;

	/**
	 * Form factory
	 * @var Sulfur\Form
	 */
	protected $form;

	/**
	 * Lang
	 * @var Sulfur\Manager\Lang
	 */
	protected $text;

	/**
	 * Slug maker
	 * @var Sulfur\Manager\Slug
	 */
	protected $slug;


	/**
	 * History
	 * @var Sulfur\Manager\History
	 */
	protected $history;



	public function provision(
		Request $request,
		Identity $identity,
		State $state,
		Data $data,
		Config $config,
		Url $url,
		Rank $rank,
		Form $form,
		Lang $lang,
		Slug $slug,
		History $history
	)
	{
		$this->request = $request;
		$this->identity = $identity;
		$this->state = $state;
		$this->data = $data;
		$this->config = $config;
		$this->url = $url;
		$this->rank = $rank;
		$this->form = $form;
		$this->lang = $lang;
		$this->slug = $slug;
		$this->history = $history;
	}


	public function index($zone, $module)
	{
		return $this->payload('index', [
			'create' => $this->identity->allowed($module, 'create') || $this->identity->allowed($zone . '/' . $module, 'create') ? $this->url->action($zone, $module, 'create') : false,
			'batch' => $this->batchActions($zone, $module)
		]);
	}


	public function items($zone, $module, $filter, $sort, $search, $skip, $amount, $count)
	{
		return $this->payload('json', [
			'data' => [
				'items' => $this->parse($this->prepare(
					$zone,
					$module,
					$filter,
					$sort ? $sort : ($this->config->get('sortable') ? ['rank' => 'ASC']: $this->config->get('sort')),
					$search,
					$skip,
					$amount
				), $zone, $module),
				'total' => $count ? $this->count($zone, $filter, $search) : false
			]
		]);
	}


	public function status($id, $status)
	{
		$entity = $this->entity($id);

		if($status) {
			$entity->status = $status;
			$this->data->save($entity);
			$success = true;
		} else {
			$success = false;
		}

		return $this->payload('json', [
			'data' => $success
		]);
	}


	public function move($id, $after, $parent)
	{
		$entity = $this->entity($id);
		$success = $this->rank->update($entity, $after, $parent);
		return $this->payload('json', [
			'data' => $success
		]);
	}


	public function create($zone, $module)
	{
		return $this->update($zone, $module);
	}


	public function update($zone, $module, $id = null)
	{
		$form = $this->form($this->config->get('form'), $zone, $module, $id)->request([]);
		$revision = $this->request->query('revision', 0);
		$post = $this->request->post();
		$revisions = [];
		$delete = false;
		$status = 'edit';

		if($id !== null) {
			// get entity
			$entity = $this->entity($id);
			// get revision
			if($revision > 0) {
				$entity = $this->revision($entity, $revision);
			}
			// insert posted data (probably from restore)
			if(! empty($post)) {
				$entity = $this->restore($entity, $post);
			}
			// get avaiable revisions
			$revisions = $this->revisions($entity);
			// populate the form
			$this->populate($form, $entity);
			// check if deletable
			$delete = $this->deletable($entity, $zone, $module);
			// get the status
			$status = $entity->status ?: 'edit';
			// get statusable
			$statusable = $this->statusable($entity, $zone, $module);
		} else {
			// get statusable for new object
			$statusable = $this->statusable((object) ['user_id' => $this->identity->user->id], $zone, $module);
		}

		return $this->payload('update', [
			'id' => $id,
			'form' => $form,
			'revision' => $revision,
			'revisions' => $revisions,
			'status' => $status,
			'delete' => $delete,
			'preview' => $this->config->get('preview') ? $this->url->url($this->config->get('preview')) : false,
			'publish' => $statusable && $this->config->get('publish')
		]);
	}


	public function save($zone, $module, $id = null)
	{
		$form = $this->form($this->config->get('form'), $zone, $module, $id);
		if($form->valid()) {
			$entity = $this->entity($id);
			$relations = [];
			$values = $form->values();

			if(! $id) {
				// Statusable for new object
				$statusable = $this->statusable((object) ['user_id' => $this->identity->user->id], $zone, $module);
			} else {
				// Statsable for exising entity
				$statusable = $this->statusable($entity, $zone, $module);
			}


			if($statusable) {
				// entity is statusable
				if($status = $this->request->post('status')) {
					// add status if posted
					$values['status'] = $status;
				}
			} else {
				// entity is not stusable
				unset($values['status']);
			}

			$this->hydrate($entity, $relations, $values, $zone, $module);

			$this->data->save($entity, $relations);
			$this->commit($entity, $values);
			$id = $entity->id;
			$revisions = $this->revisions($entity);
			$success = true;
			$message = $this->lang->get('message.' . ($id ? 'updated' : 'created'));
			$errors = [];
		} else {
			$id = null;
			$revisions = [];
			$success = false;
			$message =  $this->lang->get('message.invalid');
			$errors = $this->errors($form);
		}

		return $this->payload('json', [
			'data' => [
				'id' => $id,
				'revisions' => $revisions,
				'success' => $success,
				'message' => $message,
				'errors' => $errors
			]
		]);
	}


	public function locked() {
		$data = [];
		foreach(
			$this->finder()
			->only('lock')
			->where('id', 'in', explode(',', $this->request->post('id')))
			->all() as $entity
		) {
			list($time, $userId, $username) = explode(':', $entity->lock, 3) + [0, 0, ''];
			if((int) $time  > time() - 20 && $userId && $userId != $this->identity->id) {
				// active lock
				$data[$entity->id] = htmlspecialchars($username);
			} elseif($userId != $this->identity->id && $entity->lock !== '') {
				// cleanup old locks
				$entity->lock = '';
				$this->data->save($entity);
			}
		};
		return $this->payload('json', [
			'data' => $data
		]);
	}


	public function lock($id) {
		$entity = $this->entity($id);
		list($time, $userId, $username) = explode(':', ($entity ? $entity->lock : ''), 3) + [0, 0, ''];
		if(
			// force lock
			$this->request->post('force', false) == '1'
			// lock is timed out
			|| (int) $time < time() - 20
			// lock belongs to current user
			|| $userId == $this->identity->id
			// lock belongs to no user
			|| ! $userId
		) {
			$entity->lock = time() . ':' . $this->identity->id . ':' . $this->identity->username;
			$this->data->save($entity);
			$data = [
				'success' => true,
				'username' => htmlspecialchars($this->identity->username),
			];
		} else {
			$data = [
				'success' => false,
				'username' => htmlspecialchars($username),
			];
		}
		return $this->payload('json', [
			'data' => $data
		]);
	}



	protected function errors($form)
	{
		$errors = [];
		foreach($form->errors() as $key => $ers){
			if(count($ers) > 0){
				$errors[$key] = $this->lang->get(['error', $key, $ers[0]]);
			}
		}
		return $errors;
	}


	protected function revision($entity, $revision)
	{
		if($this->config->get('history')) {
			if($revision = $this->history->revision($entity, $revision, $this->config->get('with'))) {
				$entity = $revision;
			}
		}
		return $entity;
	}


	protected function commit($entity, $values)
	{
		if($this->config->get('history')) {
			$this->history->commit($entity, $values, $this->identity->id);
		}
	}


	protected function revisions($entity)
	{
		if($this->config->get('history') && $entity) {
			return $this->history->revisions($entity);
		} else {
			return [];
		}
	}


	protected function restore($entity, array $values = [])
	{
		$restored = $this->data->hydrate($this->config->get('entity'), $values, $this->config->get('with'));
		foreach(array_keys($values) as $key) {
			$entity->{$key} = $restored->{$key};
		}
		return $entity;
	}


	protected function form($name, $zone, $module, $id = null)
	{
		return $this->form->get($name, []);
	}


	protected function populate($form, $entity = null)
	{
		if($entity) {
			$form->values($entity->data());
		}
	}


	protected function excerpt($name, $entity)
	{
		return $entity->data();
	}


	protected function hydrate($entity, & $relations, $data, $zone, $module)
	{
		if (! $entity->id) {
			// Extra data for new entities
			$data['created'] = $data['created'] ?? date('Y-m-d H:i:s');
			$data['zone'] = $data['zone'] ?? $zone;
			$data['user_id'] = $data['user_id'] ?? $this->identity->id;
		}
		foreach($data as $name => $value) {
			if ($name === 'slug') {
				$entity->slug = $this->slug($value, $entity);
			} elseif ($entity::relation($name)) {
				// update relation
				$relations[$name] = $value;
			} else {
				// update enitity
				$entity->{$name} = $value;
			}
		}
	}


	protected function slug($value, $entity)
	{
		return  $this->slug->get($value, $entity, 'slug');
	}


	public function delete($id)
	{
		$entity = $this->entity($id);
		$success = false;
		if($entity) {
			$this->deleteEntity($entity);
			$success = true;
		}

		return new Payload('json', [
			'data' => [
				'success' => $success,
				'message' => ''
			]
		]);
	}


	protected function deleteEntity($entity)
	{
		$this->data->delete($entity);
	}


	protected function deleteChildren($entity)
	{
		$children = [];
		foreach($this->finder()->where('parent_id', $entity->id)->all() as $child) {
			$children = $children + $this->deleteChildren($child);
			$children[] = $child;
			$this->deleteEntity($child);
		}
		return $children;
	}


	/**
	 * Get a finder for the configured entity
	 * @return Sulfur\Data\Finder
	 */
	protected function finder()
	{
		$entity = $this->config->get('entity');
		$with = $this->config->get('with', []);
		$finder = $this->data->finder($entity);
		// preload all the relations
		foreach(array_keys($entity::relations()) as $name){
			$finder->with($name);
		}
		// preload deeper withs
		foreach($this->config->get('with', []) as $with) {
			$finder->with($with);
		}
		return $finder;
	}


	/**
	 * Get a loaded entity
	 * @param int $id
	 * @return Sulfur\Data\Entity
	 */
	protected function entity($id = null)
	{
		if($id !== null && $id > 0) {
			return $this->finder()->one($id);
		} else {
			$class = $this->config->get('entity');
			return new $class;
		}
	}


	/**
	 * Get the database for an entity
	 * @param Sulfur\Data\Entity $entity
	 * @return Sulfur\Database
	 */
	protected function database($entity)
	{
		return $this->data->database($entity::database());
	}


	protected function count($zone, $filter, $search)
	{
		$finder = $this->finder();
		$this->zone($finder, $zone);
		$this->filter($finder, $filter);
		$this->search($finder, $search);
		return $finder->count();
	}


	protected function prepare($zone, $module, $filter, $sort, $search, $skip, $amount)
	{
		$finder = $this->finder();

		$this->state->set('filter', $filter);
		$this->state->set('sort', $sort);
		$this->state->set('search', $search);
		$this->state->set('skip', $skip);
		$this->state->set('amount', $amount);

		$this->zone($finder, $zone);
		$this->filter($finder, $filter);
		$this->sort($finder, $sort);
		$this->search($finder, $search);
		$this->skip($finder, $skip);
		$this->amount($finder, $amount);

		return $finder;
	}


	protected function parse(Finder $finder, $zone, $module)
	{
		$parsed = [];
		foreach($finder->all() as $entity) {
			$parsed[] = $this->item($entity, $zone, $module);
		}
		return $parsed;
	}


	protected function item(Entity $entity, $zone, $module)
	{
		$data = $entity->data();
		return [
			'data' => $data,
			'create' => $this->creatable($entity, $zone, $module) ? $this->url->action($zone, $module, 'create', null, ['parent_id' => $data['id']]) : false,
			'update' => $this->updatable($entity, $zone, $module) ? $this->url->action($zone, $module, 'update', $data['id']) : false,
			'move' => $this->movable($entity, $zone, $module) ? $this->url->action($zone, $module, 'move', $data['id'], 'after={{after}}&parent={{parent}}') : false,
			'delete' => $this->deletable($entity, $zone, $module) ? $this->url->action($zone, $module, 'delete', $data['id']) : false,
			'status' => $this->statusable($entity, $zone, $module) ? $this->url->action($zone, $module, 'status', $data['id'], 'status={{status}}') : false,
			'select' => $this->selectable($entity, $zone, $module),
			'preview' => $this->previewable($entity, $zone, $module) ? $this->url->action($zone, $module, 'preview', $data['id']) : false,
			'actions' => $this->actions($entity, $zone, $module),

		];
	}


	protected function creatable($entity, $zone, $module)
	{
		return $this->config->get('tree' , false)
		&& (
			$this->identity->allowed($module, 'create')
			|| $this->identity->allowed($zone . '/' . $module, 'create')
		);
	}


	protected function updatable($entity, $zone, $module)
	{
		return ! ($entity->updatable === 0 || $entity->updatable === '0' || $entity->updatable === false)
		&& (
			$this->identity->allowed($module, 'update', $entity->user_id)
			|| $this->identity->allowed($zone . '/' . $module, 'update', $entity->user_id)
		);
	}


	protected function movable($entity, $zone, $module)
	{
		return ! ($entity->movable === 0 || $entity->movable === '0' || $entity->movable === false)
		&& (
			$this->identity->allowed($module, 'move', $entity->user_id)
			|| $this->identity->allowed($zone . '/' . $module, 'move', $entity->user_id)
		);
	}


	protected function deletable($entity, $zone, $module)
	{
		return ! ($entity->deletable === 0 || $entity->deletable === '0' || $entity->deletable === false)
		&& (
			$this->identity->allowed($module, 'delete', $entity->user_id)
			|| $this->identity->allowed($zone . '/' . $module, 'delete', $entity->user_id)
		);
	}


	protected function statusable($entity, $zone, $module)
	{
		return $this->identity->allowed($module, 'status', $entity->user_id)
		|| $this->identity->allowed($zone . '/' . $module, 'status', $entity->user_id);
	}


	protected function selectable($entity, $zone, $module)
	{
		return $this->request->query('task', false) === 'select';
	}


	protected function previewable($entity, $zone, $module)
	{

		return $this->identity->allowed($module, 'preview', $entity->user_id)
		|| $this->identity->allowed($zone . '/' . $module, 'preview', $entity->user_id);
	}


	protected function actions($entity, $zone, $module)
	{
		$actions = [];
		foreach($this->config->get('actions') as $action => $info) {
			if(is_int($action)) {
				$action = $info;
				$info = [];
			}
			if(
				(is_array($info) && isset($info['allowed']) && $info['allowed'])
				|| $this->identity->allowed($module, $action, $entity->user_id)
				|| $this->identity->allowed($zone . '/' . $module, $action, $entity->user_id)
			) {
				$actions[$action] = $this->url->action($zone, $module, $action, $entity->id);
			}
		}
		return count($actions) == 0 ? false : $actions;
	}


	protected function batchActions($zone, $module)
	{
		$actions = [];
		foreach($this->config->get('batch') as $action => $info) {
			if(is_int($action)) {
				$action = $info;
				$info = [];
			}
			if(
				(is_array($info) && isset($info['allowed']) && $info['allowed'])
				|| $this->identity->allowed($module, $action)
				|| $this->identity->allowed($zone . '/' . $module, $action)
			) {
				$actions[$action] = $info;
			}
		}
		return count($actions) == 0 ? false : $actions;
	}


	protected function zone(Finder $finder, $zone)
	{
		$finder->where('zone', $zone);
	}


	protected function filter(Finder $finder, $filter)
	{
		if(is_array($filter)) {
			foreach($filter as $name => $value) {
				$finder->where($name, $value);
			}
		}
	}


	protected function sort(Finder $finder, $sort)
	{
		if(is_array($sort)) {
			foreach($sort as $name => $direction) {
				$finder->order($name, $direction);
			}
		}
	}


	protected function search(Finder $finder, $search)
	{
		if($search) {
			$columns = $this->config->get('search');
			$finder->where(function($query) use ($columns, $search){
				foreach($columns as $column){
					$query->orWhere($column, 'LIKE', '%' . trim($search) . '%');
				}
			});
		}
	}


	protected function skip(Finder $finder, $skip)
	{
		if($skip > 0) {
			$finder->offset($skip);
		}
	}


	protected function amount(Finder $finder, $amount)
	{
		if($amount > 0) {
			$finder->limit($amount);
		}
	}

	protected function payload($template, $data)
	{
		return new Payload($template, $data);
	}
}