<?php

namespace Sulfur\Manager;

use Sulfur\View;
use Sulfur\Request;
use Sulfur\Identity;
use Sulfur\Manager\Config;
use Sulfur\Manager\Lang;
use Sulfur\Manager\Navigation;
use Sulfur\Manager\State;
use Sulfur\Manager\Url;
use Sulfur\Csrf;

class Helper
{
	protected $view;
	protected $request;
	protected $identity;
	protected $config;
	protected $lang;
	protected $navigation;
	protected $state;
	protected $url;
	protected $csrf;

	public function __construct(
		View $view,
		Request $request,
		Identity $identity,
		Config $config,
		Lang $lang,
		Navigation $navigation,
		State $state,
		Url $url,
		Csrf $csrf
	)
	{
		$this->view = $view;
		$this->request = $request;
		$this->identity = $identity;
		$this->config = $config;
		$this->lang = $lang;
		$this->navigation = $navigation;
		$this->state = $state;
		$this->url = $url;
		$this->csrf = $csrf;
	}


	public function register($module, $zone) {
		// set view helpers
		$this->view->helper([
			'lang'=> function($key = null, $module = null) {
				if($key === null) {
					return $this->lang->lang();
				} elseif($text = $this->lang->get($key, $module)) {
					return $text;
				} else {
					return $key;
				}
			},
			'base'=> function(){
				return $this->url->base();
			},
			'action' => function($module = null, $action = null, $id = null, $query = null, $zn = null) use ($zone) {
				if($module === null) {
					return $this->request->get('action', 'index');
				}
				if($zn === null) {
					$zn = $zone;
				}
				return $this->url->action($zn, $module, $action, $id, $query);
			},
			'route' => function($route, $params = []) use ($zone) {
				if(! isset($params['zone'])) {
					$params['zone'] = $zone;
				}
				return $this->url->route($route, $params);
			},
			'url' => function($route, $params = [], $parts = []) {
				return $this->url->url($route, $params, $parts);
			},
			'navigation' => function($type) {
				return $this->navigation->{$type}();
			},
			'identity' => function() {
				return $this->identity;
			},
			'allowed' => function($resource, $privilege, $ownerId = null) use ($zone) {
				return $this->identity->allowed($resource, $privilege, $ownerId)
				|| $this->identity->allowed($zone . '/' . $resource, $privilege, $ownerId);
			},
			'config' => function($key = null, $default = null) {
				if($key === null) {
					return $this->config;
				} else {
					return $this->config->get($key, $default);
				}
			},
			'state' => function($key, $default = null) {
				return $this->state->get($key, $default);
			},
			'env' => function($name, $default = null) {
				return $this->config->env($name, $default);
			},
			'csrf' => function() {
				return $this->csrf->token();
			},
			'viewport' => function() {
				return $this->request->query('viewport', 'main');
			},
			'task' => function() {
				return $this->request->query('task', 'none');
			},
			'callback' => function() {
				return $this->request->query('callback', '');
			},
			'module' => function() use ($module) {
				return $module;
			},
			'zone' => function() use ($zone) {
				return $zone;
			},
		]);
	}
}