<?php

namespace Sulfur\Manager;

class Provider
{
	public static function register($container)
	{
		$config = $container->get('Sulfur\Config');

		$container->set([
			'Sulfur\Manager\Config' => [':paths' => $config->config(), ':replace' => $config->env()],
			'Sulfur\Manager\History' => [':config' => $config->history()],
			'Sulfur\Manager\File\Manager' => [
				'upload' => ['filesystem' => [':name' => 'files']],
				'filesystem' => [':name' => 'files']
			],
			'Sulfur\Manager\Image\Manager' => [
				'upload' => ['filesystem' => [':name' => 'images']],
			],
			'Sulfur\Manager\Lang' => ['paths' => function() use ($config) {
				return $config->lang('paths');
			}],
		]);


		$container->share([
			'Sulfur\Manager\Config',
			'Sulfur\Manager\State',
			'Sulfur\Manager\Url',
			'Sulfur\Manager\Lang',
		]);
	}
}