<?php

namespace Sulfur\Manager;

class Tree
{
	protected $map = [];

	protected $tree = [];

	public function __construct($entities)
	{
		foreach($entities as $entity) {
			$entity->children = [];
			$this->map[$entity->id] = $entity;
		}

		foreach($entities as $entity) {
			if($entity->parent_id == 0) {
				$this->tree[] = $entity;
			} elseif(isset($this->map[$entity->parent_id])) {
				$children = $this->map[$entity->parent_id]->children;
				$children[] = $entity;
				$this->map[$entity->parent_id]->children = $children;
			}
		}
	}


	public function tree()
	{
		return $this->tree;
	}


	public function index()
	{
		$index = [];
		return $this->_index($this->tree, $index, 0);
	}


	public function _index($children, & $index, $depth)
	{
		foreach($children as $child) {
			$child->depth = $depth;
			$index[] = $child;
			$this->_index($child->children, $index, $depth + 1);
		}
		return $index;
	}
}