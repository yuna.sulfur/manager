<?php

namespace Sulfur\Manager\File;

use Sulfur\Manager\Manager as ModuleManager;

use Sulfur\Upload;
use Sulfur\Filesystem;

class Manager extends ModuleManager
{

	protected $upload;

	protected $filesystem;

	public function __construct(Upload $upload, Filesystem $filesystem)
	{
		$this->upload = $upload;
		$this->filesystem = $filesystem;
	}


	public function index($zone, $module)
	{
		$payload = parent::index($zone, $module);
		$payload->data([
			'create' => false,
			'upload' => $payload->data('create')
		]);
		$payload->view('file/index');
		return $payload;
	}


	public function create($zone, $module)
	{
		$this->upload
		->size($this->config->get('size'))
		->filter([$this, 'valid'])
		->path([$this, 'path']);

		// parse the files
		$files = $this->upload->store('file');

		if(count($files) === 0) {
			// no files found: get errors
			$success = false;
			$errors = [];
			foreach($this->upload->errors() as $error){
				$errors[] = str_replace(':file', $error['file'], $this->lang->get(['error', $error['error']]));
			}
			$success = false;
			$message = $this->lang->get('message.not_uploaded');
			$items = [];
		} else {
			// success
			$items = [];
			$success = true;
			$message = $this->lang->get('message.uploaded');
			$errors = [];
			foreach($files as $info) {
				$data = [
					'file' => pathinfo($info['file'])['basename'],
					'path' => pathinfo($info['file'])['dirname'] . '/',
					'title' => $info['name']
				];
				$entity = $this->entity();
				$relations = [];
				$this->hydrate($entity, $relations, $data, $zone, $module);
				$this->data->save($entity);

				// resulting items
				$items[] = $this->item($entity, $zone, $module);
			}
		}

		return $this->payload(
			'json',
			['data' => [
				'success' => $success,
				'errors' => $errors,
				'items' => $items,
				'message' => $message
			]]
		);
	}



	public function serve($id)
	{
		$entity = $this->entity($id);

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename="' . $entity->file . '"');
		header('Expires: 0');
		header('Cache-Control: must-revalidate');
		header('Pragma: public');
		header('Content-Length: ' . $this->filesystem->size($entity->path . $entity->file));

		echo $this->filesystem->read($entity->path . $entity->file);
		exit;
	}


	protected function deleteEntity($entity)
	{
		$this->filesystem->delete($entity->path . $entity->file);
		$this->data->delete($entity);
	}

	public function valid($file)
	{
		return true;
	}

	public function path($name, Filesystem $filesystem)
	{
		$path = trim($this->config->get('path'), '/');
		if(strlen($path) > 0) {
			$path = $path . '/';
		}

		$parts = pathinfo($name);
		$extension = preg_replace('#[^0-9a-zA-Z]#', '',  (isset($parts['extension']) ? $parts['extension'] : ''));
		$name = preg_replace('#[^0-9a-zA-Z-_]#', '', $parts['filename']);
		$file = ($name == '' ? 'file' : $name) . ($extension == '' ? '' : ('.' . $extension));

		return $path . $file;
	}


	protected function form($name, $zone, $module, $id = null)
	{
		return $this->form->get($name, [
			'url' => $this->url->action($zone, $module, 'upload')
		]);
	}
}