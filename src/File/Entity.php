<?php

namespace Sulfur\Manager\File;

use Sulfur\Data\Entity as BaseEntity;

class Entity extends BaseEntity
{
	protected static $database = 'default';

	protected static $table = 'file';

	protected static $columns = [
		'id' => 'int',
		'created' => 'datetime',
		'timestamp' => 'timestamp',
		'status' => ['enum', 'values' => ['edit', 'review' ,'live']],
		'title' => 'string',
		'file' => 'string',
		'path' => 'string',
		'user_id' => 'int',
		'zone' => ['string', 'index' => true],
	];
}