<?php

namespace Sulfur\Manager;

class Detect
{
	public static function manager($keyword = 'manager', $domain = null)
	{
		$uri = !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : (isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '');

		$parts = explode('/',$uri);

		if(in_array($keyword, $parts)){
			return true;
		}

		return false;
	}
}
