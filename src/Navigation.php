<?php
namespace Sulfur\Manager;

use Sulfur\Identity;
use Sulfur\Request;
use Sulfur\Manager\Config;
use Sulfur\View;
use Sulfur\Manager\Lang;
use Sulfur\Manager\Url;

class Navigation
{
	protected $identity;

	protected $request;

	protected $config;

	protected $view;

	protected $lang;

	protected $url;

	public function __construct(Identity $identity, Request $request, Config $config, View $view, Lang $lang, Url $url)
	{
		$this->identity = $identity;
		$this->request = $request;
		$this->config = $config;
		$this->view = $view;
		$this->lang = $lang;
		$this->url = $url;
	}



	protected function normalize($zone, $module, $info)
	{
		$module = isset($info['module']) ? $info['module'] : $module;
		$action = isset($info['action']) ? $info['action'] : 'index';

		$items = false;
		$button = false;
		$href = false;
		$label = '';

		if(isset($info['items']) && is_array($info['items'])) {
			$label = $this->lang->get($module, 'section');

			// item with subitems: normalize the subitems
			$items = [];
			foreach($info['items'] as $subModule => $subInfo) {
				if($subItem = $this->normalize($zone, $subModule, $subInfo)) {
					$items[] = $subItem;
				}
			}

			// if none of them is allowed: dont user this
			if(count($items) == 0) {
				return false;
			}
		} else {
			// this is a regular clickable item
			if(
				(
					$this->identity->allowed($module, 'access')
					|| $this->identity->allowed($zone . '/' . $module, 'access')
				) && (
					$this->identity->allowed($module, $action)
					|| $this->identity->allowed($zone . '/' . $module, $action)
				)
			) {

				$field = $action == 'index' ? 'name' : 'label.' . $action;
				if($label = $this->lang->get($field, $module)) {

				} else {
					$label = $field;
				}

				// action allowed, set href
				$href  = $this->url->action($zone, $module, $action);

				// process button
				if(isset($info['button']) && isset($info['button']['action'])) {
					$buttonModule = isset($info['button']['module']) ? $info['button']['module'] : $module;
					if(
						$this->identity->allowed($buttonModule, $info['button']['action'])
						|| $this->identity->allowed($zone . '/' . $buttonModule, $info['button']['action'])
					) {
						$button = [
							'icon' => $info['button']['icon'] ? $info['button']['icon'] : 'arrow_right_alt',
							'href' => $this->url->action($zone, $buttonModule, $info['button']['action'])
						];
					}
				}
			} else {
				// dont use item if main action not allowed
				return false;
			}
		}

		return [
			'icon' => isset($info['icon']) ? $info['icon'] : 'play_arrow',
			'href' => $href,
			'button' => $button,
			'items' => $items,
			'label' => $label,
		];
	}



	public function main()
	{
		// get zones
		$zones = [];
		foreach ($this->config->zones() as $zone => $data) {
			if ($this->identity->allowed($zone . '/zone', 'access')) {
				$zones[$zone] = $data;
			}
		}

		$zone = $this->request->get('zone');

		// create a nested array with nav items
		$items = [];
		foreach ($this->config->navigation('menu') as $module => $info) {
			if($item = $this->normalize($zone, $module, $info)) {
				$items[] = $item;
			}
		}

		// check if frontendlink should be shown
		if ($route = $this->config->navigation('frontend')) {
			$frontend = $this->url->url($route, ['zone' => $zone]);
		} else {
			$frontend = false;
		}

		// result view
		return $this->view->render('navigation/main', [
			'zones' => $zones,
			'zone' => $zone,
			'frontend' => $frontend,
			'items' => $items,
		]);
	}
}