<?php

namespace Sulfur\Manager;

use Sulfur\Data;
use Sulfur\Data\Entity;

class Slug
{
	/**
	 * Sulfur data
	 * @var Sulfur\Data
	 */
	protected $data;

	/**
	 * List of slug that can not be used
	 * @var array
	 */
	protected $blacklist = [];


	public function __construct(Data $data)
	{
		$this->data = $data;
	}


	public function blacklist(array $blacklist)
	{
		$this->blacklist = $blacklist;
		return $this;
	}


	public function get($uri, Entity $entity, $column = 'slug')
	{
		$uri = $this->clean($uri);
		$uri = $this->unique($uri, $entity, $column);
		return $uri;
	}


	public function clean($uri)
	{
		// no emprty uri's
		if($uri === ''){
			$uri = '-';
		}

		// replace spaces with dash
		$uri = str_replace(' ', '-', $uri);

		// replace all funny chars
		try{
			$uri = iconv('UTF-8', 'ASCII//TRANSLIT', $uri);
		} catch(Exception $e){
			$uri = str_replace(
				['À','Á','Â','Ã','Ä','Å','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','ß','à','á','â','ã','ä','å','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ'],
				['a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','ss','a','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y'],
				$uri
			);
		}
		// remove all other chars
		$uri = preg_replace('/[^A-Za-z0-9-]/', '', $uri);
		// remove mulptiple dashes
		$uri = preg_replace('/-+/', '-', $uri);
		// trim dashes
		$uri = trim($uri, '-');
		// to lower
		$uri = strtolower($uri);
		// done
		return $uri;
	}


	public function unique($uri, Entity $entity, $column)
	{
		$database = $this->data->database($entity::database());
		$table = $entity::table();

		$query = $database->select($column)
		->from($table)
		->where($column, 'LIKE', $uri . '%');

		if ($entity->id) {
			$query->where('id', '<>', $entity->id);
		}

		if ($entity->zone) {
			$query->where('zone', $entity->zone);
		}

		// create lookup array
		$lookup = [];
		foreach($query->result() as $result){
			$lookup[strtolower($result[$column])] = true;
		}


		if( ! isset($lookup[strtolower($uri)]) && ! in_array($uri, $this->blacklist)){
			// no exact matches: uri is ok
			return $uri;
		} else {
			// try to add a number to it and see if it exists
			$number = 2;
			while( isset($lookup[strtolower($uri) . '-' . $number]) || in_array(strtolower($uri) . '-' . $number, $this->blacklist)){
				$number++;
			}
			return $uri . '-' . $number;
		}
	}
}