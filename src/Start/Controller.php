<?php

namespace Sulfur\Manager\Start;

use Sulfur\Manager\Bootstrap;
use Sulfur\Request;
use Sulfur\Response;
use Sulfur\Identity;
use Sulfur\View;
use Sulfur\Manager\Url;
use Sulfur\Manager\Config;
use Sulfur\Manager\Lang;

class Controller
{

	protected $lang;

	protected $identity;

	protected $url;

	public function index(
		Bootstrap $bootstrap,
		Request $request,
		Response $response,
		Identity $identity,
		View $view,
		Url $url,
		Config $config,
		Lang $lang
	) {

		$this->lang = $lang;
		$this->identity = $identity;
		$this->url = $url;


		if ($identity->confirmed() == false){
			$response->redirect($url->route('login') . '?redirect=' . urlencode($url->current(true)) );
		}

		if ( ! $identity->whitelisted($request->ip())){
			$response->redirect($url->route('login') . '?redirect=' . urlencode($url->current(true)) );
		}

		if($zone = $request->get('zone')) {
			// show the request zone
			if ($identity->allowed($zone . '/zone', 'access')) {

				// start up a page
				$bootstrap('start', $zone);

				// fill up sections
				$items = [];
				foreach ($config->navigation('menu') as $module => $info) {
					if($item = $this->normalize($zone, $module, $info)) {

						$items[] = $item;
					}
				}

				// render it
				return $view->render('start/index', [
					'items' => $items,
					'identity' => $identity,
					'zone' => $zone
				]);

			} else {
				$response->status(401);
				return 'No access to zone ' . $zone;
			}
		} else {
			// Get zones from config
			$zones = $config->zones();

			// Get domain
			$domain = $request->domain();

			// First allowed zone
			$first = null;

			foreach($zones as $name => $zone) {
				if(
					isset($zone['base'])
					&& strpos($zone['base'], 'http') === 0
					&& strpos($zone['base'], $domain) !== false
					&& $identity->allowed($name . '/zone', 'access')
				) {
					// Zone is dictated by the domain and is allowed
					$response->redirect($url->route('start', ['zone' => $name]), 303);
				}

				// Remember first allowed zone
				if ($first === null && $identity->allowed($name . '/zone', 'access')) {
					$first = $name;
				}
			}

			if ($first === null) {
				// No allowed zone found
				$identity->destroy();
				$response->status(401);
				return 'No access to any zones';
			} else {
				//Redirect to the start page of the first zone
				$response->redirect($url->route('start', ['zone' => $first]), 303);
			}
		}
	}


	protected function normalize($zone, $module, $info)
	{
		$module = isset($info['module']) ? $info['module'] : $module;
		$action = isset($info['action']) ? $info['action'] : 'index';

		$items = false;
		$button = false;
		$href = false;
		$label = '';
		$description = '';

		if(isset($info['items']) && is_array($info['items'])) {
			$label = $this->lang->get($module, 'section');

			// item with subitems: normalize the subitems
			$items = [];
			foreach($info['items'] as $subModule => $subInfo) {
				if($subItem = $this->normalize($zone, $subModule, $subInfo)) {
					$items[] = $subItem;
				}
			}

			// if none of them is allowed: dont user this
			if(count($items) == 0) {
				return false;
			}
		} else {
			// this is a regular clickable item
			if(
				(
					$this->identity->allowed($module, 'access')
					|| $this->identity->allowed($zone . '/' . $module, 'access')
				) && (
					$this->identity->allowed($module, $action)
					|| $this->identity->allowed($zone . '/' . $module, $action)
				)
			) {

				$field = $action == 'index' ? 'name' : 'label.' . $action;
				if($label = $this->lang->get($field, $module)) {

				} else {
					$label = $field;
				}

				$description = $this->lang->get('description', $module);

				// action allowed, set href
				$href  = $this->url->action($zone, $module, $action);

				// process button
				if(isset($info['button']) && isset($info['button']['action'])) {
					$buttonModule = isset($info['button']['module']) ? $info['button']['module'] : $module;
					if(
						$this->identity->allowed($buttonModule, $info['button']['action'])
						|| $this->identity->allowed($zone . '/' . $buttonModule, $info['button']['action'])
					) {
						$button = [
							'icon' => $info['button']['icon'] ? $info['button']['icon'] : 'arrow_right_alt',
							'href' => $this->url->action($zone, $buttonModule, $info['button']['action'])
						];
					}
				}
			} else {
				// dont use item if main action not allowed
				return false;
			}
		}

		return [
			'icon' => isset($info['icon']) ? $info['icon'] : 'play_arrow',
			'href' => $href,
			'button' => $button,
			'items' => $items,
			'label' => $label,
			'description' => $description
		];
	}
}