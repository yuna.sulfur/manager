<?php

namespace Sulfur\Manager;

use Sulfur\Config as BaseConfig;

class Config extends BaseConfig
{

	protected $zone = null;

	protected $module = null;


	public function zone($zone)
	{
		$this->zone = $zone;
	}

	public function module($module)
	{
		$this->module = $module;
	}


	/**
	 * Get a value from the loaded resources
	 * @param string|array $key
	 * @param mixed $default
	 * @return mixed
	 */
	public function get($key, $default = null, $module = null)
	{
		if($module === null) {
			if ($this->module === null) {
				throw new Exception('Trying to get config value with key ' . $key . ' but no module is provided');
			} else {
				$module = $this->module;
			}
		}

		// get cascading set of resources with key
		$resources = [];
		if($this->zone) {
			// load resource from app zone config first
			$resources[] = [$this->zone . '/module/' . $module, $key];
		}
		// load resource from  app config
		$resources[] = 	['module/' . $module, $key];
		// load resource from manager config
		$resources[] = ['manager', is_array($key) ? array_merge([$module], $key) : $module . '.' . $key];
		// load resource from default setting
		$resources[] = 	['manager', is_array($key) ? array_merge(['default'], $key) :  'default.' . $key];

		// go through resources to get value
		foreach($resources as $resource) {
			$found = $this->resource($resource[0], $resource[1], '__notfound__');
			if($found !== '__notfound__') {
				return $found;
			}
		}
		return $default;
	}
}